# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.sites.models import Site
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.decorators.cache import cache_page
from django.views.generic import ListView, TemplateView

from market.models import MySite


# from template_selection.managers import custom_get_current_site


class VerifyYandex(TemplateView):
    template_name = 'yandex.html'

    def get_current_site(self, request):
        if settings.SITE_BY_REQUEST:
            return Site.objects.get(domain=request.get_host())
        else:
            return Site.objects.get_current()

    def get_context_data(self, **kwargs):
        kwargs = super(VerifyYandex, self).get_context_data(**kwargs)
        kwargs['mysite'] = get_object_or_404(MySite, verify_yandex=self.kwargs['slug'],
                                             site=self.get_current_site(self.request))
        return kwargs

    def dispatch(self, request, *args, **kwargs):
        self.current_site = self.get_current_site(request)
        super_dispatch = super(VerifyYandex, self).dispatch
        return super_dispatch(request, *args, **kwargs)


class BingVerify(TemplateView):
    template_name = 'BingSiteAuth.xml'

    def get_current_site(self, request):
        if settings.SITE_BY_REQUEST:
            return Site.objects.get(domain=request.get_host())
        else:
            return Site.objects.get_current()

    def get_context_data(self, **kwargs):
        kwargs = super(BingVerify, self).get_context_data(**kwargs)
        mysite = get_object_or_404(MySite, site=self.get_current_site(self.request))
        if mysite.verify_bing is None:
            raise Http404
        kwargs['bing_code'] = mysite.verify_bing
        return kwargs

    def dispatch(self, request, *args, **kwargs):
        self.current_site = self.get_current_site(request)
        super_dispatch = super(BingVerify, self).dispatch
        return super_dispatch(request, *args, **kwargs)


class VerifyAdmitad(ListView):
    """
    Returns a generated verify_cpa.txt file with correct mimetype (text/plain),
    status code (200 or 404),
    """
    model = MySite
    context_object_name = 'object_mysite'
    cache_timeout = False
    template_name = 'verify-admitad.html'

    def get_current_site(self, request):
        if settings.SITE_BY_REQUEST:
            return Site.objects.get(domain=request.get_host())
        else:
            return Site.objects.get_current()

    def get_queryset(self):
        return MySite.objects.filter(site=self.current_site)

    def get_context_data(self, **kwargs):
        context = super(VerifyAdmitad, self).get_context_data(**kwargs)
        return context

    def render_to_response(self, context, **kwargs):
        return super(VerifyAdmitad, self).render_to_response(
            context, content_type='text/plain', **kwargs
        )

    def get_cache_timeout(self):
        return self.cache_timeout

    def dispatch(self, request, *args, **kwargs):
        cache_timeout = self.get_cache_timeout()
        self.current_site = self.get_current_site(request)
        super_dispatch = super(VerifyAdmitad, self).dispatch
        if not cache_timeout:
            return super_dispatch(request, *args, **kwargs)
        key_prefix = self.current_site.domain
        cache_decorator = cache_page(cache_timeout, key_prefix=key_prefix)
        return cache_decorator(super_dispatch)(request, *args, **kwargs)


class VerifyAdmitadCustom(ListView):
    """
    Returns a generated verify_cpa.txt file with correct mimetype (text/plain),
    status code (200 or 404),
    """
    model = MySite
    context_object_name = 'object_mysite'
    cache_timeout = False
    template_name = 'verify-admitad.html'

    def get_current_site(self, request):
        if settings.SITE_BY_REQUEST:
            return Site.objects.get(domain=request.get_host())
        else:
            return Site.objects.get_current()

    def get_queryset(self):
        return MySite.objects.filter(site=self.current_site)

    def get_context_data(self, **kwargs):
        context = super(VerifyAdmitadCustom, self).get_context_data(**kwargs)
        return context

    def render_to_response(self, context, **kwargs):
        return super(VerifyAdmitadCustom, self).render_to_response(
            context, content_type='text/plain', **kwargs
        )

    def get_cache_timeout(self):
        return self.cache_timeout

    def dispatch(self, request, *args, **kwargs):
        cache_timeout = self.get_cache_timeout()
        self.current_site = self.get_current_site(request)
        super_dispatch = super(VerifyAdmitadCustom, self).dispatch
        if not cache_timeout:
            return super_dispatch(request, *args, **kwargs)
        key_prefix = self.current_site.domain
        cache_decorator = cache_page(cache_timeout, key_prefix=key_prefix)
        return cache_decorator(super_dispatch)(request, *args, **kwargs)
