# -*- coding: utf-8 -*-
from django.conf.urls import url

from verify_cpa import views

urlpatterns = [
    url(r'^verify-admitad\.txt$', views.VerifyAdmitad.as_view(), name='verify_admitad'),
    url(r'^BingSiteAuth\.xml$', views.BingVerify.as_view(), name='verify_bing'),
    url(r'^admitad\.txt$', views.TemplateView.as_view(template_name="verify_admitad_txt.html")),
    url(r'^yandex_(?P<slug>[-\w\d]+)\.html$', views.VerifyYandex.as_view(), name="verify_yandex"),
]
