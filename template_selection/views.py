# -*- coding: utf-8 -*-

from django.views.generic import ListView

from template_selection.managers import get_custom_template, get_custom_template_name


class GustomListView(ListView):
    def get_paginate_by(self, queryset):
        template_obj = get_custom_template(self.request)
        if template_obj.paginate_by:
            return template_obj.paginate_by
        return self.paginate_by

    def get_template_names(self):

        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """

        if self.template is None:
            names = super(GustomListView, self).get_template_names()
        else:
            self.template_name = get_custom_template_name(self.request, self.template)
            names = []
            names.append(self.template_name)
        return names
