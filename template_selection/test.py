# -*- coding: utf-8 -*-
import unittest

from django.contrib.sites.models import Site
from django.test import TestCase

from django.urls import reverse


# from mixer.backend.django import mixer

# from base.base_test_lib import clear_search_index
# from market.models import Lot, ENABLED, Seller

@unittest.skip
class MarketClientTest(TestCase):
    def setUp(self):
        self.url = reverse('main_view')
        self.r = self.client.get(self.url)

    def tearDown(self):
        # clear_search_index()
        pass

    def test_create_site(self):
        # Сайта нет в базе, создаем его
        response = self.client.get(self.url)
        print(response.context['SITE'].domain)
        self.assertEqual(response.status_code, 200)
        for site in Site.objects.all():
            print(site, site.pk)
        self.assertEqual(site, 200)

        # self.lots = mixer.cycle(5).blend(Lot, status=ENABLED)
        # for seller in Seller.objects.all():
        #     for site in Site.objects.all():
        #         seller.site.add(site)
        # # print(Site.objects.get(pk=2))
        # template = response.templates[0]
        # self.assertEqual(response.status_code, 200)
        # self.assertEqual(template.name, 'default/index.html')
        # self.assertEqual(response.context['new_lots'], [])
        # self.assertEqual(response.context['sale'], [])
        # self.assertEqual(response.context['special'], [])
        # clear_search_index()
