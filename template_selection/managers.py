# -*- coding: utf-8 -*-
import logging
import os
import shutil

from django.conf import settings
from django.contrib.sites.models import Site
from django.db import models
from django.http.request import split_domain_port
from django.template import exceptions
from django.template.loader import get_template

from market.models import Template, MySite

logger = logging.getLogger(__name__)

SITE_CACHE = {}


# DEFOLT_FOLDER = 'default'
# ADMIN_FOLDER = 'dashboard'

# DEFOLT_FOLDER = 'trendsetter'


def get_custom_template_name(request, file_name):
    try:
        template = get_custom_template(request)
        if template is None:
            folder = settings.DEFOLT_FOLDER
        else:
            folder = template.folder
        template_name_full = '%s/%s' % (folder, file_name)
        logger.debug('template_name_full: %s' % template_name_full)
        try:
            # Шаблон есть в данной папке?
            get_template(template_name=template_name_full)
        except exceptions.TemplateDoesNotExist:
            if template.is_crm_template:
                template_name_full = '%s/%s' % (settings.ADMIN_FOLDER, file_name)
            else:
                template_name_full = '%s/%s' % (settings.DEFOLT_FOLDER, file_name)
            logger.info(('Пользовательского шаблона нет, выводим дефолтный %s' % template_name_full))

            try:
                # Пользовательского шаблона нет, выводим дефолтный
                get_template(template_name=template_name_full)
                return template_name_full
            except exceptions.TemplateDoesNotExist:
                logger.error('И дефолтного нет!!!! Это наглость!!! Админу письмо, срочно!!!!')
                if template.is_crm_template:
                    create_default_template(file_name, settings.ADMIN_FOLDER)
                else:
                    create_default_template(file_name)
                # И дефолтного нет!!!! Это наглость!!! Админу письмо, срочно!!!!
                return '%s/%s' % (settings.DEFOLT_FOLDER, 'error.html')
    except AttributeError:
        # У пользользовательского шаблона нет папки? дефолтный используем.
        folder = settings.DEFOLT_FOLDER
    return '%s/%s' % (folder, file_name)


def get_custom_template(request):
    try:
        site = custom_get_current_site(request)
        # print(site)
        return Template.objects.get(mysite__site=site)
    except (Template.DoesNotExist, Site.DoesNotExist) as e:
        try:
            return Template.objects.get(name=settings.DEFOLT_FOLDER)
        except Template.DoesNotExist:
            return Template.objects.create(name=settings.DEFOLT_FOLDER,
                                           folder=settings.DEFOLT_FOLDER,
                                           hidden=True)


def create_default_template(name, folder=settings.DEFOLT_FOLDER):
    tem_dir = os.path.join(settings.BASE_DIR, 'templates', folder)
    if not os.path.exists(tem_dir):
        os.makedirs(tem_dir)
    file = os.path.join(tem_dir, name)
    # От куда копируем образец
    file_temp = os.path.join(settings.BASE_DIR, 'templates', settings.DEFOLT_FOLDER, 'form.html')
    shutil.copyfile(file_temp, file)
    logger.info(('Файл создали %s, обновите страницу)' % file))


def custom_get_current_site(request):
    # TODO: Кеширование Запросов ОБЯЗАТЕЛЬНО!!!!
    site, create = Site.objects.update_or_create(domain=request.get_host(),
                                                 defaults={'name': request.get_host()})

    if create:
        template, create = Template.objects.get_or_create(folder=settings.DEFOLT_FOLDER,
                                                          defaults={'name': settings.DEFOLT_FOLDER})
        MySite.objects.update_or_create(site=site,
                                        defaults={
                                            'template': template,
                                            'is_https': request.is_secure()
                                        })
    logger.debug('custom_get_current_site %s' % site.id)
    if site.mysite.is_https != request.is_secure():
        MySite.objects.update_or_create(site=site,
                                        defaults={
                                            'is_https': request.is_secure()
                                        })
    return site
