# -*- coding: utf-8 -*-

import logging

from django.db import models

from market.models import MySite, CommonAbstractModel, Lot

logger = logging.getLogger(__name__)


class StatUrl(CommonAbstractModel):
    site = models.ForeignKey(MySite)
    url = models.TextField(default='', verbose_name='Целевая ссылка')
    lot = models.ForeignKey(Lot, null=True, blank=True)
