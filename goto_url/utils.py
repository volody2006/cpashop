# -*- coding: utf-8 -*-
import base64
import re


def goto_url(url):
    """Build link to internal view"""
    url = base64.encodestring(url.encode()).decode().replace("\n", "")
    return "/{name}/{url}".format(name="goto", url=url)


def decode_goto_url(string):
    try:
        redirect_url = base64.decodestring(string.encode()).decode()
    except (Exception,) as e:
        redirect_url = string
        if not re.search('^http(s)?://', redirect_url):
            redirect_url = 'http://{0}'.format(redirect_url)
    return redirect_url
