# -*- coding: utf-8 -*-
import base64
import logging
import re

# from base.models import MyError
from django.http import HttpResponseRedirect
# from django.utils.encoding import DjangoUnicodeDecodeError
from django.views.generic import View

logger = logging.getLogger(__name__)


class GotoView(View):
    def dispatch(self, request, url, **kwargs):
        try:
            redirect_url = base64.decodestring(url.encode()).decode()
        except (Exception,) as e:
            # if e.message == 'Incorrect padding':
            redirect_url = url
            if not re.search(r'^http(s)?://', redirect_url):
                redirect_url = 'http://{0}'.format(redirect_url)
        return HttpResponseRedirect(redirect_url)
