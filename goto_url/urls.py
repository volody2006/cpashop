from django.conf.urls import url

#
# from views import GotoView
from goto_url.views import GotoView

urlpatterns = [
    url(r'^goto/(.+)$', GotoView.as_view(), name='goto'),
]
