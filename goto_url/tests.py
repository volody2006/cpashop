# coding: utf-8
# import unittest
from django.test import TestCase
from django.urls import reverse

from goto_url import utils
from goto_url.templatetags.goto_url import goto_url


class GoToTestCase(TestCase):
    def setUp(self):
        self.url1 = "http://google.com/"
        self.url2 = "http://яндекс.рф/"

    def test_url_to_goto(self):
        self.assertTrue(utils.goto_url(self.url1), '/goto/aHR0cDovL2dvb2dsZS5jb20v')
        self.assertTrue(utils.goto_url(self.url2), '/goto/aHR0cDovL9GP0L3QtNC10LrRgS7RgNGELw==')
        # self.assertEqual(lion.speak(), 'The lion says "roar"')
        # self.assertEqual(cat.speak(), 'The cat says "meow"')

    def test_goto_to_url(self):
        self.assertTrue(utils.decode_goto_url('aHR0cDovL2dvb2dsZS5jb20v'), self.url1)
        self.assertTrue(utils.decode_goto_url('aHR0cDovL9GP0L3QtNC10LrRgS7RgNGELw=='), self.url2)

    def test_url_decode(self):
        self.assertTrue(utils.decode_goto_url(self.url1), self.url1)
        self.assertTrue(utils.decode_goto_url(self.url1), self.url1)
        self.assertTrue(utils.decode_goto_url('https://google.com/'), 'https://google.com/')
        self.assertTrue(utils.decode_goto_url('google.com/'), 'http://google.com/')

    def test_templatetags_goto_url(self):
        self.assertTrue(goto_url(self.url1), '/goto/aHR0cDovL2dvb2dsZS5jb20v')
