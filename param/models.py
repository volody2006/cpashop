# -*- coding: utf-8 -*-
from django.db import models
from six import python_2_unicode_compatible

from base.models import CommonAbstractModel


@python_2_unicode_compatible
class Param(CommonAbstractModel):
    name = models.CharField(max_length=255, verbose_name='название параметра', null=True, blank=True)
    value = models.CharField(max_length=255, verbose_name='значение параметра', null=True, blank=True)
    unit = models.CharField(max_length=255, verbose_name='единица измерения (для числовых параметров)',
                            null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta(object):
        unique_together = ('name', 'value', 'unit')


class ParamName(CommonAbstractModel):
    name = models.CharField(max_length=255, verbose_name='название параметра')
    name_ru = models.CharField(max_length=255, verbose_name='название параметра на русском', default='')
    synonyms = models.TextField(help_text='Синонимы (размер, размеры)', default='')
