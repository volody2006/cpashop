$(document).on('click', 'a.del_btn', function () {
    var $btn = $(this);
    var url = $btn.attr('href');

    if (confirm('Вы уверены, что хотите удалить этот seller с сайта?')) {
        $.ajax({
            url: url,
            data: {seller: $(this).data('seller-id')},
            type: 'POST',
            cache: false,
            success: function (data) {
                if (data.status) {
                    $btn.closest('tr').remove()
                } else {
                    alert(data['error']);
                }
            }
        });
    }
    return false;
});

