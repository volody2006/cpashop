# -*- coding: utf-8 -*-
# import urlparse

import redis
from django.conf import settings
from future.backports.urllib.parse import urlparse
from requests.packages import chardet


def to_unicode(string):
    """
    Принудительно конвертирует string в unicode-строку, вне зависимости от
    исходного типа и кодировки
    """
    if not isinstance(string, str):
        raise TypeError("Argument must be string or unicode")
    if isinstance(string, str):
        return string

    # новый chardet 2.2.1 ругается при передачи в него уникода - он понимает только байтовые строки
    # if isinstance(string, str):
    #    string = string.decode('utf-8')
    try:
        info = chardet.detect(string)
        try:
            if info.get('encoding'):
                return string.decode(info['encoding'])
            else:
                return ''
        except Exception:
            return ''
    except LookupError:
        return ''


def get_redis():
    """
    Унифицированный метод, возвращающий объект соединения с Редисом
    """
    if getattr(settings, 'REDIS_POOL'):
        pool = settings.REDIS_POOL
    else:
        pool = redis.ConnectionPool(host=settings.REDIS_HOST, port=int(settings.REDIS_PORT), db=int(settings.REDIS_DB))
    return redis.StrictRedis(connection_pool=pool)


def absolute_url(path, based_on):
    """
    Приводит абсолютный путь без указания домена вида /foo/bar/ к
    виду http://example.com/foo/bar при указании базы вида http://example.com/my/index.php

    Если абсолютный путь является URL с указанием схемы, преобразования не происходит
    """
    if not path.lower().startswith(('http://', 'https://')):
        parts = urlparse(based_on)
        return '{scheme}://{hostname}/{path}'.format(scheme=parts.scheme,
                                                     hostname=parts.netloc,
                                                     path=path.lstrip('/'))
    return path
