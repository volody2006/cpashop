# -*- coding: utf-8 -*-
import logging
from datetime import datetime
from urllib.parse import urlparse

import pytz
from admitad import items
from admitad.api import get_oauth_client_client
from django.conf import settings

from market.models import Deeplink, Seller
from my_admitad_api.exception import ApiError
from my_admitad_api.models import *

logger = logging.getLogger(__name__)


def get_utc_from_local(date_time, local_tz=None):
    assert date_time.__class__.__name__ == 'datetime'
    if local_tz is None:
        local_tz = pytz.timezone(settings.TIME_ZONE)  # Django eg, "Europe/London"
    local_time = local_tz.normalize(local_tz.localize(date_time))
    return local_time.astimezone(pytz.utc)


class BaseClass:
    def get_site(self, url):
        '''
        Возвращает мойсайт по урлу.
        :param url:
        :return:
        '''
        return None

    def read_datetime(self, value):
        try:
            dt_str, delta = value.split("+")
        except ValueError:
            dt_str = value
        except AttributeError:
            return None
        # '2014-03-27T09:50:23'
        # dt = datetime.strptime('2014-03-27T09:50:23', '%Y-%m-%dT%H:%M:%S')
        try:
            dt = datetime.strptime(dt_str, '%Y-%m-%dT%H:%M:%S')
            return get_utc_from_local(date_time=dt)
        except ValueError:
            return None

    def read_string(self, value):
        """
        Интерфейс для считывания текстового содержимого текущего узла
        """
        try:
            return str(value)
        except UnicodeEncodeError:
            return value

    def read_is_none(self, value):
        if self.read_string(value).lower() == 'none':
            return None
        return value

    def read_string_or_none(self, value):
        if self.read_string(value).lower() == 'none':
            return None
        if self.read_string(value).replace(' ', '') == '':
            return None
        return self.read_string(value)

    def read_boolean_or_none(self, value):
        """
        Приводит строку 'true' или 'false', полученную из
        содержимого узла, к соответствующему булевому значению.
        """
        if self.read_string(value).lower() == 'none':
            return None
        return self.read_string(value).upper() == 'TRUE'

    def read_number_integer(self, value):
        """
        Приводит строку, полученную из содержимого узла, к
        соответствующему целочисленному значению.
        """
        try:
            raw = self.read_string(value)
            return int(float((raw or '0').replace(',', '.')))
        except(TypeError, UnicodeDecodeError, ValueError):
            return 0

    def read_number_integer_or_none(self, value):
        """
        Приводит строку, полученную из содержимого узла, к
        соответствующему целочисленному значению.
        """
        if self.read_is_none(value) is None:
            return None
        try:
            raw = self.read_string(value)
            return int(float((raw or '0').replace(',', '.')))
        except(TypeError, UnicodeDecodeError, ValueError):
            return 0

    def read_number_float(self, value):
        """
        Приводит строку, полученную из содержимого узла, к
        соответствующему значению.
        """
        try:
            raw = self.read_string(value)
            return float((raw or '0').replace(',', '.'))
        except(TypeError, UnicodeDecodeError, ValueError):
            return float(0)

    def read_number_float_or_none(self, value):
        """
        Приводит строку, полученную из содержимого узла, к
        соответствующему значению.
        """
        if self.read_is_none(value) is None:
            return None
        try:
            raw = self.read_string(value)
            return float((raw or '0').replace(',', '.'))
        except(TypeError, UnicodeDecodeError, ValueError):
            return float(0)


class AdmitadApi(BaseClass):
    def __init__(self, client_id=None, client_secret=None, user=None,
                 mysite=None):
        self.user = user
        self.mysite = mysite
        if self.user is None:
            if client_id is None:
                self.client_id = settings.ADMITAD_CLIENT_ID
            else:
                self.client_id = client_id

            if client_secret is None:
                self.client_secret = settings.ADMITAD_CLIENT_SECRET
            else:
                self.client_secret = client_secret
        else:
            self.client_id = self.user.userprofile.admitad_identifier
            self.client_secret = self.user.userprofile.admitad_secret_key
        self.adm_website = getattr(self.mysite, 'adm_website', None)

    def get_client(self, scopes):
        logger.debug('Admitad Api: get_client')
        return get_oauth_client_client(
            client_id=self.client_id,
            client_secret=self.client_secret,
            scopes=scopes)

    def _get_referrals(self, **kwargs):
        logger.debug('Admitad Api: _get_referrals')
        scopes = ' '.join(set([items.Referrals.SCOPE]))
        client = self.get_client(scopes=scopes)
        res = client.Referrals.get(**kwargs)
        logger.debug(res)
        return res

    def referrals(self):
        res = self._get_referrals()
        count = res['_meta']['count']
        limit = res['_meta']['limit']
        offset = res['_meta']['offset']
        if not AdmUserProfile.objects.filter(username=settings.ADMITAD_LOGIN).exists():
            res_me = self.get_me()
            AdmUserProfile.objects.update_or_create(id=self.read_number_integer(res_me['id']),
                                                    defaults={
                                                        'username': self.read_string(res_me['username']),
                                                    })

        while True:
            if res:
                for ref_js in res['results']:
                    logger.debug(ref_js)
                    AdmUserProfile.objects.update_or_create(
                        id=self.read_number_integer(ref_js['id']),
                        defaults={
                            'username': self.read_string(ref_js['username']),
                            'payment': self.read_number_integer_or_none(ref_js['payment']),
                            'referral': AdmUserProfile.objects.get(username=settings.ADMITAD_LOGIN)
                        })

            offset = offset + limit
            if offset > count:
                break
            res = self._get_referrals(offset=offset)

    def get_me(self):
        scope = ' '.join(set([items.Me.SCOPE]))
        return self.get_client(scopes=scope).Me.get()

    def user_info(self):
        info = self.get_me()
        AdmUserProfile.objects.update_or_create(
                        id=self.read_number_integer(info['id']),
                        defaults={
                            'username': self.read_string(info['username']),
                        })

    def get_balance(self):
        # [{u'currency': u'USD', u'balance': u'0.00'}, {u'currency': u'RUB',
        # #u'balance': u'15202.92'}]
        logger.debug('Admitad Api: get_balance')
        scopes = 'private_data_balance'
        client = self.get_client(scopes=scopes)
        return client.Balance.get()

    def get_websites(self, offset=0):
        logger.debug('Admitad Api: get_websites')
        scopes = 'websites'
        client = self.get_client(scopes=scopes)
        return client.Websites.get(limit=20, offset=offset)

    def advcampaigns_one_for_website(self, c_id, w_id=None):
        if not w_id:
            # id Маркет в Москве (больше всех программ подключено)
            w_id = 180756
        logger.debug('Admitad Api: advcampaigns_for_website')
        scopes = ' '.join(set([items.CampaignsForWebsite.SCOPE]))
        client = self.get_client(scopes=scopes)
        from admitad.exceptions import HttpException
        try:
            res = client.CampaignsForWebsite.getOne(_id=w_id, c_id=c_id)
        except HttpException:
            return {'error': 'HttpException'}
        return res

    def advcampaigns_all_for_website(self, w_id=None, limit=500, offset=0):
        if not w_id:
            # id Маркет в Москве (больше всех программ подключено)
            w_id = 180756
        logger.debug('Admitad Api: advcampaigns_for_website')
        scopes = ' '.join(set([items.CampaignsForWebsite.SCOPE]))
        client = self.get_client(scopes=scopes)
        kwargs = {}
        kwargs['limit'] = limit
        kwargs['offset'] = offset
        res = client.CampaignsForWebsite.get(w_id, **kwargs)
        logger.debug(res)
        return res

    def _campaigns_for_websites(self):
        if self.mysite.adm_website is None:
            # сайт не привязан, нечего делать
            return False
        w_id = self.mysite.adm_website.pk
        return self.advcampaigns_all_for_website(w_id)

    def campaigns_for_websites(self):
        res = self._campaigns_for_websites()
        if res:
            for cam_js in res['results']:
                logger.debug(cam_js)
                try:
                    adm_cam = AdmCampaign.objects.get(pk=cam_js['id'])
                    seller = Seller.objects.get(adm_campaign=adm_cam)
                except AdmCampaign.DoesNotExist:

                    logger.info('Незнаем компанию %s %s' % (cam_js['id'], cam_js['name']))
                    continue
                except Seller.DoesNotExist:
                    logger.info('Незнаем seller, adm_cam %s %s' % (adm_cam.pk, adm_cam.name))
                    seller, create = Seller.objects.update_or_create(
                        url_home=cam_js['site_url'],
                        defaults={
                            'name': cam_js['name'],
                            'adm_campaign': adm_cam,
                            'xml_feed': cam_js['products_xml_link']
                        })
                gotolink = self.read_string_or_none(cam_js['gotolink'])
                adm_cam_user, create = AdmCampaignUser.objects.update_or_create(
                    user=self.user,
                    adm_campaign=adm_cam,
                    website=self.mysite.adm_website,
                    defaults={
                        'connection_status': self.read_string(cam_js['connection_status']),
                        'gotolink': gotolink
                    }
                )
                if self.read_string(cam_js['connection_status']) == 'active' and gotolink is not None:
                    active = True
                    Deeplink.objects.update_or_create(site=self.mysite,
                                                      seller=seller,
                                                      defaults={
                                                          'active': active,
                                                          'url': gotolink,
                                                      })

    def _verify_website(self):
        logger.debug('Admitad Api: _verify_website')
        scope = ' '.join(set([items.WebsitesManage.SCOPE]))
        return self.get_client(scopes=scope).WebsitesManage.verify(self.adm_website.id)

    def verify_website(self):
        if self.mysite.adm_website != self.adm_website:
            logger.info('Ошибка. Мы пытаемся верифицировать сайт не того пользователя')
            raise ApiError('Ошибка. Мы пытаемся верифицировать сайт не того пользователя')
        if self.mysite.verify_admitad is None:
            logger.info('Ошибка. Нет верификационного кода')
            raise ApiError('Ошибка. Нет верификационного кода')
        if not self.mysite.adm_website:
            logger.info('Ошибка. Нет привязки к сайту')
            raise ApiError('Ошибка. Нет привязки к сайту')

        if self.mysite.adm_website.status == 'active':
            logger.info('Верификации сайта не требуется')
            self.mysite.manual_verification = True
            self.mysite.save()
            return True

        res = self._verify_website()
        if res['success'] == "Accepted":
            logger.info('Успех верификации сайта')
            self.mysite.manual_verification = True
            self.mysite.save()
            return True
        logger.error('Error verifite code, website %s, user %s' % (self.mysite, self.user))
        return False

    def _connecting_site_to_program(self, c_id, w_id=None):
        """
        Connect an advertising campaign for a website
        Here w_id is a website id and c_id is a campaign id

        Args:
            c_id (int)
            w_id (int)

        """
        if w_id is None:
            w_id = self.adm_website.id

        logger.debug('Admitad Api: _connecting_site_to_program')
        scope = ' '.join(set([items.CampaignsManage.SCOPE]))
        return self.get_client(scopes=scope).CampaignsManage.connect(c_id, w_id, )

    def connecting_site_to_program(self, c_id):
        adm_campaign = AdmCampaign.objects.get(id=c_id)
        if AdmCampaignUser.objects.filter(user=self.user, adm_campaign=adm_campaign).exists():
            # Программа уже есть, зачем ее подключать?
            return False
        res = self._connecting_site_to_program(c_id)
        logger.debug(res)
        if str(res.get('status')).lower() == 'ok':
            return True
        if str(res.get('success')).lower() == 'ok':
            return True
        return False

    def _disconnect_site_to_program(self, c_id, w_id=None):
        """
        Connect an advertising campaign for a website
        Here w_id is a website id and c_id is a campaign id

        Args:
            c_id (int)
            w_id (int)

        """
        if w_id is None:
            w_id = self.adm_website.id

        logger.debug('Admitad Api: _connecting_site_to_program')
        scope = ' '.join(set([items.CampaignsManage.SCOPE]))
        return self.get_client(scopes=scope).CampaignsManage.disconnect(c_id, w_id, )

    def disconnect_site_to_program(self, c_id):
        adm_campaign = AdmCampaign.objects.get(id=c_id)
        # if adm_campaign in self.mysite.adm_campaigns.all():
        if AdmCampaignUser.objects.filter(user=self.user, adm_campaign=adm_campaign).exists():
            res = self._disconnect_site_to_program(c_id)
            logger.info(res)
            # print(res)
            if str(res.get('status')).lower() == 'ok':
                return True
            if str(res.get('success')).lower() == 'deleted':
                return True
        return False

    def add_program_site(self, c_id):
        # Подключаем программу Адмитад к сайту
        if self.connecting_site_to_program(c_id):
            self.campaigns_for_websites()
            # AdmCampaignUser.objects.get(user=self.user, adm_campaign_id=c_id).delete()
            # self.mysite.adm_campaigns.add(AdmCampaign.objects.get(id=c_id))

    def remove_program_site(self, c_id):
        # Отключаем программу Адмитад от сайта
        if self.disconnect_site_to_program(c_id):
            AdmCampaignUser.objects.get(user=self.user, adm_campaign_id=c_id).delete()
            deeplink = Deeplink.objects.filter(site=self.mysite,
                                               seller__adm_campaign_id=c_id)
            deeplink.delete()
            # self.mysite.adm_campaigns.remove(AdmCampaign.objects.get(id=c_id))

    def _create_website_adm(self):
        if not self.mysite:
            # Нет сайта! Нечего делать.
            raise ApiError

        if self.mysite.adm_website:
            # Данный сайт уже создан.
            raise ApiError

        scope = ' '.join(set([items.WebsitesManage.SCOPE]))

        res = self.get_client(scope).WebsitesManage.create(
            name=self.mysite.name,
            kind='website',
            language='ru',
            site_url=self.mysite.http_domain,
            description=self.mysite.description,
            categories=62,
            regions='RU',
        )
        logger.info('res')
        return res

    def create_website_adm(self):
        res = self._create_website_adm()
        # Почему-то json вызывает ошибку при записи. Поле не важное, так что пропускаем его.
        website, create_site = Website.objects.update_or_create(
            id=self.read_number_integer(res['id']),
            defaults={
                'name': self.read_string(res['name']),
                'description': self.read_string(res['description']),
                'status': self.read_string(res['status']),
                'is_old': self.read_boolean_or_none(res['is_old']),
                'creation_date': self.read_datetime(res['creation_date']),
                'site_url': self.read_string(res['site_url']),
                'verification_code': self.read_string(res['verification_code']),
                'results': res,
            }
        )
        self.mysite.adm_website = website
        self.mysite.verify_admitad = self.read_string(res['verification_code'])
        self.mysite.save()
        if self.user:
            website.user = self.user
            website.save()
        return website

    def update_websites(self):
        logger.debug('Admitad Api: update_websites')
        websites = self.get_websites()
        count = websites['_meta']['count']
        limit = websites['_meta']['limit']
        offset = websites['_meta']['offset']
        while True:
            for website in websites['results']:
                logger.debug(website)
                site_adm, create_site = Website.objects.update_or_create(
                    id=self.read_number_integer(website['id']),
                    defaults={
                        'name': self.read_string(website['name']),
                        'description': self.read_string(website['description']),
                        'status': self.read_string(website['status']),
                        'is_old': self.read_boolean_or_none(website['is_old']),
                        'atnd_visits': self.read_number_integer(
                            website['atnd_visits'] if 'atnd_visits' in website else 0),
                        'atnd_hits': self.read_number_integer(website['atnd_hits'] if 'atnd_hits' in website else 0),
                        'creation_date': self.read_datetime(website['creation_date']),
                        'site_url': self.read_string(website['site_url']),
                        'verification_code': self.read_string(website['verification_code']),
                        'results': website,
                        # 'language': self.read_string(website['language']),
                    }
                )
                if self.user:
                    site_adm.user = self.user

                if 'regions' in website:
                    for region in website.pop('regions'):
                        region_obj, create_region = Region.objects.update_or_create(
                            region=region['region'],
                        )
                        site_adm.regions.add(region_obj)
                if 'categories' in website:
                    for category in website.pop('categories'):
                        category_obj, create_cat = AdmCategory.objects.get_or_create(id=category['id'])
                        site_adm.categories.add(category_obj)
                if 'kind' in website:
                    kind, create_kind = AdmWebsitesKinds.objects.get_or_create(name=website.pop('kind'))
                    site_adm.kind = kind

                if 'adservice' in website:
                    if website['adservice'] is not None:
                        site_adm.adservice, create_kind = AdmAdservices.objects.get_or_create(
                            id=website['adservice']['id'],
                            defaults={
                                'name': website['adservice']['name'],
                                'results': website['adservice'],
                            })

                if 'language' in website:
                    site_adm.language = website.pop('language')
                site_adm.save()

            offset = offset + limit
            if offset > count:
                break

            websites = self.get_websites(offset=offset)

    def get_campaigns(self, offset=0):
        # Список рекламных программ
        logger.debug('Admitad Api: get_campaigns')
        scopes = 'advcampaigns'
        client = self.get_client(scopes=scopes)
        return client.Campaigns.get(limit=20, offset=offset)
        # return client.Campaigns.get(limit=2)

    def update_campaigns(self):
        logger.debug('Admitad Api: update_campaigns')
        campaigns = self.get_campaigns(offset=0)
        count = campaigns['_meta']['count']
        limit = campaigns['_meta']['limit']
        offset = campaigns['_meta']['offset']
        while offset < count:
            for campaign in campaigns['results']:
                try:
                    campaign['modified_date'] = self.read_datetime(campaign['modified_date']).isoformat()
                except AttributeError:
                    # campaign['modified_date'] = None
                    campaign.pop('modified_date')
                try:
                    campaign['activation_date'] = self.read_datetime(campaign['activation_date']).isoformat()
                except AttributeError:
                    campaign.pop('activation_date')
                logger.debug(campaign)
                self.update_one_campaign(campaign)
            offset = offset + limit
            campaigns = self.get_campaigns(offset=offset)

    def update_one_campaign(self, campaign):
        logger.debug('Admitad Api: update_one_campaign')
        campaign = self.purge_dict_campaign(campaign)
        results = campaign.copy()
        campaign_object, create = AdmCampaign.objects.update_or_create(
            id=campaign.pop('id'),
            defaults={'results': results}
        )
        # TODO: Обработка todo значений, в дальнейшем здесь будет правильная обработка
        if 'actions' in campaign:
            campaign_object.actions_todo = campaign.pop('actions')
        if 'currency' in campaign:
            campaign_object.currency_todo = campaign.pop('currency')
        if 'traffics' in campaign:
            campaign_object.traffics_todo = campaign.pop('traffics')
        campaign_object.save()

        if 'name_aliases' in campaign:
            for name in campaign.pop('name_aliases'):
                campaign_object.save_name_aliases(name)

        for region in campaign.pop('regions'):
            # print (region)
            region_obj, create_region = Region.objects.get_or_create(
                region=region['region'],
            )

            campaign_object.regions.add(region_obj)

        for category in campaign.pop('categories'):
            category_obj, create_cat = AdmCategory.objects.get_or_create(id=category['id'])
            if create_cat:
                # Пора обновить категории?
                self.update_categories()
            campaign_object.categories.add(category_obj)

        # field_names = AdmCampaign._meta.get_all_field_names()
        field_names = [f.name for f in AdmCampaign._meta.get_fields()]
        for key in list(campaign.keys()):
            if key not in field_names:
                no_key, create_key = NoKeyAdmCampaign.objects.get_or_create(
                    name=key, values=campaign[key])
                logger.debug((u'update_one_campaign: нет ключа: %s - %s') % (key, campaign.pop(key)))
        AdmCampaign.objects.filter(pk=campaign_object.pk).update(**campaign)

    def purge_dict_campaign(self, campaign):
        '''
        приводим значения в словаре к правильному типу
        Args:
            campaign: словарь
        Returns: словарь

        '''
        base = BaseClass()

        if 'modified_date' in campaign:
            campaign['modified_date'] = base.read_datetime(campaign['modified_date']).isoformat()
        if 'activation_date' in campaign:
            campaign['activation_date'] = base.read_datetime(campaign['activation_date']).isoformat()
        if 'rating' in campaign:
            campaign['rating'] = base.read_number_float_or_none(campaign['rating'])
        if 'cr' in campaign:
            campaign['cr'] = base.read_number_float_or_none(campaign['cr'])
        if 'cr_trend' in campaign:
            campaign['cr_trend'] = base.read_number_float_or_none(campaign['cr_trend'])
        if 'ecpc' in campaign:
            campaign['ecpc'] = base.read_number_float_or_none(campaign['ecpc'])
        if 'ecpc_trend' in campaign:
            campaign['ecpc_trend'] = base.read_number_float_or_none(campaign['ecpc_trend'])
        if 'rate_of_approve' in campaign:
            campaign['rate_of_approve'] = base.read_number_float_or_none(campaign['rate_of_approve'])
        if 'avg_hold_time' in campaign:
            campaign['avg_hold_time'] = base.read_number_integer_or_none(campaign['avg_hold_time'])
        if 'name_aliases' in campaign:
            name_aliases = []
            name_aliases_old = campaign['name_aliases'].split(',')
            for name in name_aliases_old:
                if name.strip() == '':
                    continue
                name_aliases.append(name)
            campaign['name_aliases'] = name_aliases
        return campaign

    def get_categories(self, offset=0):
        logger.debug('Admitad Api: get_categories')
        scopes = 'public_data'
        client = self.get_client(scopes=scopes)
        return client.CampaignCategories.get(offset=offset)

    def update_categories(self):
        '''
        Обновляем категории адмитада
        Returns:
        '''
        logger.debug('Admitad Api: update_categories')
        categories = self.get_categories(offset=0)
        count = categories['_meta']['count']
        limit = categories['_meta']['limit']
        offset = categories['_meta']['offset']
        while offset < count:
            for category in categories['results']:
                # print(category)
                if category['parent'] is None:
                    parent = None
                else:
                    parent, create_parent = AdmCategory.objects.update_or_create(id=int(category['parent']['id']))
                category_obj, create_category = AdmCategory.objects.update_or_create(
                    id=category['id'],
                    defaults={
                        'name': category['name'],
                        'language': category['language'],
                        'parent': parent,
                    }
                )
            offset = offset + limit
            categories = self.get_categories(offset=offset)


def search_adm_website(site_id):
    """
    Ищем сайты в адмитад и возвращаем первый попавший
    :param site_id:
    :return:
    """
    # from my_admitad_api.models import Website

    from market.models import MySite
    mysite = MySite.objects.get(site_id=site_id)
    logger.info('search_adm_website %s' % mysite.domain)
    if mysite.adm_website:
        return mysite.adm_website
    if not mysite.user.userprofile.admitad_identifier or not mysite.user.userprofile.admitad_secret_key:
        logger.info('Not admitad_identifier or admitad_secret_key user %s' % mysite.user)
        return None
    client_id = mysite.user.userprofile.admitad_identifier
    client_secret = mysite.user.userprofile.admitad_secret_key
    adm = AdmitadApi(client_id=client_id,
                     client_secret=client_secret,
                     user=mysite.user,
                     mysite=mysite)
    adm.update_websites()
    site_list = []
    for site_adm in Website.objects.filter(site_url__icontains=mysite.domain, user=mysite.user):
        url_parse = urlparse(site_adm.site_url)
        if url_parse.netloc:
            if url_parse.netloc == mysite.domain:
                site_list.append(site_adm)
        elif url_parse.path:
            if url_parse.path == mysite.domain:
                site_list.append(site_adm)
    for site_adm in site_list:
        if site_adm.status == 'active':
            return site_adm
    for site_adm in site_list:
        if site_adm.status == 'new':
            return site_adm
    logger.info('Admitad site None (( %s' % mysite.domain)
    return None
