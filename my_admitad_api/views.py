# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, ListView

from market.mixpanel.utils import GetTemplateNamesMixin
from my_admitad_api.models import AdmCampaign, AdmCategory


class AdmCampaignDetailView(LoginRequiredMixin, GetTemplateNamesMixin, DetailView):
    template = 'my_admitad_api/admcampaign_detail.html'
    model = AdmCampaign


class AdmCampaignListView(LoginRequiredMixin, GetTemplateNamesMixin, ListView):
    template = 'my_admitad_api/admcampaign_list_new.html'
    model = AdmCampaign
    paginate_by = None

    def get_paginate_by(self, queryset):
        return self.paginate_by

    def get_queryset(self):
        # TODO: Кеширование обязательно!!!
        return AdmCampaign.objects.filter(allow_deeplink=True, status='active',
                                          categories__in=AdmCategory.objects.filter(pk=62)
                                          )
