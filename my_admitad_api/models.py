# -*- coding: utf-8 -*-
import logging
from urllib.parse import urlparse

from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models, utils
from django.urls import reverse
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from base.models import CommonAbstractModel

logger = logging.getLogger(__name__)


# class AdmLog(CommonAbstractModel):
#     task = models.CharField(max_length=254, null=True, blank=True, help_text='Название таски')
#     modified_date = models.DateTimeField(help_text='Время записи', null=True, blank=True)
#     error = models.CharField(max_length=254, null=True, blank=True, help_text='Ошибка, если произошла')
#     user = models.ForeignKey(User, null=True, blank=True)
#     message = models.CharField(max_length=254, help_text='Сообщение')
#     def __str__(self):
#         return self.message

class AdmCampaign(CommonAbstractModel):
    # id	Идентификатор рекламной программы
    id = models.IntegerField(primary_key=True, help_text='Идентификатор рекламной программы')
    name = models.CharField(max_length=254, help_text='Название рекламной программы', null=True, blank=True)
    image = models.URLField(max_length=1024, help_text='Логотип рекламной программы', null=True, blank=True)
    status = models.CharField(verbose_name='статус', max_length=254, help_text='Статус рекламной программы', null=True,
                              blank=True)
    rating = models.FloatField(help_text='Рейтинг рекламной программы', null=True, blank=True)
    description = models.TextField(help_text='Описание рекламной программы', null=True, blank=True)
    raw_description = models.TextField(help_text='Описание рекламной программы содержащее html теги', null=True,
                                       blank=True)
    site_url = models.URLField(max_length=1024, help_text='Сайт рекламной программы', null=True, blank=True)
    exclusive = models.NullBooleanField(verbose_name='Экслюзив', help_text='Экслюзивная рекламная программа', null=True,
                                        blank=True)
    # TODO: Валюты в отдельную таблицу. Удалить '_todo'
    # currency = models.ManyToManyField(help_text='Системные валюты', null=True, blank=True)
    currency_todo = models.TextField(help_text='Системные валюты', null=True, blank=True)
    regions = models.ManyToManyField('Region', help_text='Регионы площадок')
    categories = models.ManyToManyField('AdmCategory', help_text='Категории рекламных программ')
    actions_todo = models.TextField(help_text='Список тарифов по оплачиваемым действиям', null=True, blank=True)
    cr = models.FloatField(help_text='Показатель средней конверсии', null=True, blank=True)
    cr_trend = models.FloatField(help_text='Изменение показателя средней конверсии', null=True, blank=True)
    ecpc = models.FloatField(help_text='Показатель среднего eCPC', null=True, blank=True)
    ecpc_trend = models.FloatField(help_text='Изменение показателя среднего eCPC', null=True, blank=True)
    rate_of_approve = models.FloatField(help_text='Процент подтверждения', null=True, blank=True)
    more_rules = models.TextField(help_text='Дополнительные правила', null=True, blank=True)
    geotargeting = models.NullBooleanField(help_text='Разделение оплаты в зависимости от страны клиента', null=True,
                                           blank=True)
    coupon_iframe_denied = models.NullBooleanField(help_text='Запрет купонного трафика через iframe', null=True,
                                                   blank=True)
    activation_date = models.DateTimeField(help_text='Дата запуска программы', null=True, blank=True)
    modified_date = models.DateTimeField(help_text='Дата последнего изменения программы', null=True, blank=True)
    connected = models.NullBooleanField(verbose_name='Подключена?',
                                        help_text='Программа подключена к одной или нескольким площадкам вебмастера',
                                        null=True, blank=True)
    avg_hold_time = models.IntegerField(help_text='Фактическое среднее время холда', null=True, blank=True)
    max_hold_time = models.IntegerField(help_text='Максимальное время холда', null=True, blank=True)
    denynewwms = models.NullBooleanField(help_text='Набор новых веб-мастеров приостановлен', null=True, blank=True)
    goto_cookie_lifetime = models.IntegerField(help_text='Время жизни postclick cookie', null=True, blank=True)
    retag = models.CharField(max_length=254, help_text='ReTag', null=True, blank=True)
    show_products_links = models.NullBooleanField(
        help_text='Разрешено показывать ссылки для выгрузки товаров (true, false)',
        null=True, blank=True)
    # TODO
    traffics_todo = models.TextField(help_text='Разрешенные виды трафика', null=True, blank=True)
    landing_code = models.TextField(help_text='Код связанного лендинга', null=True, blank=True)
    landing_title = models.TextField(help_text='Заголовок связанного лендинга', null=True, blank=True)
    action_type = models.CharField(max_length=254, help_text='Тип действия (lead/sale)', null=True, blank=True)
    individual_terms = models.NullBooleanField(help_text='Индивидуальные условия', null=True, blank=True)
    # TODO: В библиотеке нет ответа!? moderation
    # moderation = models.TextField(help_text='Необходимость модерации', null=True, blank=True)
    allow_deeplink = models.NullBooleanField(help_text='Возможность использовать “Deeplink”', null=True, blank=True)
    results = JSONField(null=True, blank=True)

    def __str__(self):
        return self.name

    def save_name_aliases(self, name):
        if not AdmCampaignNameAliases.objects.filter(name=name, campaign=self).exists():
            try:
                AdmCampaignNameAliases.objects.create(name=name, campaign=self)
            except utils.DataError as e:
                logger.info('%s name:%s  campaign:%s' % (e, name, self.name))
                print('%s name:%s  campaign:%s' % (e, name, self.name))
                AdmCampaignNameAliases.objects.create(name=name[:254], campaign=self)

    def get_categories(self):
        return self.categories.all()

    def get_absolute_url(self):
        return reverse('admcampaign_detail', args=[self.pk, ])

    def get_xml_feed(self):
        if self.allow_deeplink:
            from my_admitad_api.managers import AdmitadApi
            adm = AdmitadApi()
            # try:
            resuld = adm.advcampaigns_one_for_website(c_id=self.id)
            print(resuld)
            if 'error' in resuld:
                self.status = 'delete'
                self.save()
            return resuld.get('products_xml_link')
            # except:
            #     return None
        return None

    class Meta:
        ordering = ['name']


class AdmCampaignUser(CommonAbstractModel):
    # Прокси модель для хранения юзерских программ
    user = models.ForeignKey(User)
    adm_campaign = models.ForeignKey(AdmCampaign)
    website = models.ForeignKey('Website')
    connection_status = models.CharField(verbose_name='Статус компании', max_length=50,
                                         null=True, blank=True)
    gotolink = models.URLField(max_length=1024, null=True, blank=True)

    def __str__(self):
        try:
            return 'AdmCampaignUser %s - %s' % (self.user.username, self.adm_campaign.name)
        except TypeError:
            return 'AdmCampaignUser %s - %s' % (self.user.id, self.adm_campaign.id)

    class Meta:
        unique_together = ("user", "adm_campaign", "website")


class AdmCampaignNameAliases(CommonAbstractModel):
    name = models.CharField(verbose_name='Синоним', max_length=255)
    campaign = models.ForeignKey(AdmCampaign)

    def __str__(self):
        return self.name

    class Meta(object):
        unique_together = ("name", "campaign")


class AdmCategory(MPTTModel, CommonAbstractModel):
    id = models.IntegerField(primary_key=True, help_text='Идентификатор категории')
    name = models.CharField(max_length=255, help_text='Название категории', null=True)
    language = models.CharField(max_length=255, help_text='Язык ответа', null=True)
    parent = TreeForeignKey('self', related_name='children',
                            null=True,
                            blank=True)

    def __str__(self):
        if self.name is None:
            return str(self.id)
        return self.name


class AdmWebsitesKinds(CommonAbstractModel):
    name = models.CharField(max_length=255, help_text='Типы площадок', unique=True)

    # class Meta(object):
    #     unique_together = ("feed", "source_id")
    def __str__(self):
        return self.name


class Region(CommonAbstractModel):
    # id = models.IntegerField(primary_key=True)
    region = models.CharField(max_length=255, help_text='Регион площадок', unique=True)

    def __str__(self):
        return self.region


class NoKeyAdmCampaign(CommonAbstractModel):
    name = models.CharField(max_length=255, help_text='Имя отсутсвующего поля')
    values = models.TextField(help_text='значение', null=True, blank=True)

    def __str__(self):
        return self.name


class AdmAdservices(CommonAbstractModel):
    id = models.IntegerField(primary_key=True, help_text='Идентификатор рекламного сервиса')
    name = models.CharField(max_length=254, help_text='Название рекламного сервиса', null=True, blank=True)
    logo = models.TextField(null=True, blank=True, help_text='Логотип рекламного сервиса')
    allowed_referrers = models.TextField(null=True, blank=True, help_text='Разрешённые рефереры')
    url = models.URLField(max_length=1024, help_text='Ссылка на сайт рекламного сервиса', null=True, blank=True)
    results = JSONField(null=True, blank=True)

    def __str__(self):
        return self.name


class Website(CommonAbstractModel):
    # Наши сайты у адмитада
    id = models.IntegerField(primary_key=True, help_text='Идентификатор площадки')
    name = models.CharField(max_length=254, help_text='Название площадки', null=True, blank=True)
    description = models.TextField(help_text='Описание площадки', null=True, blank=True)
    status = models.CharField(verbose_name='Статус', max_length=254, help_text='Статус площадки', null=True, blank=True)
    is_old = models.NullBooleanField(null=True, blank=True)
    atnd_visits = models.IntegerField(null=True, blank=True)
    atnd_hits = models.IntegerField(null=True, blank=True)
    creation_date = models.DateTimeField(null=True, blank=True, help_text='Дата создания площадки')
    site_url = models.URLField(max_length=1024, help_text='Сайт площадки', null=True, blank=True)
    verification_code = models.CharField(max_length=255, verbose_name='Код верификации', null=True, blank=True)
    kind = models.ForeignKey(AdmWebsitesKinds, null=True, blank=True)
    language = models.TextField(null=True, blank=True, help_text='Системные языки')
    regions = models.ManyToManyField(Region, help_text='Регионы площадок')
    adservice = models.ForeignKey(AdmAdservices, null=True, blank=True)
    categories = models.ManyToManyField('AdmCategory', help_text='Категории рекламных программ')
    results = JSONField(null=True, blank=True)
    user = models.ForeignKey(User, null=True)

    def __str__(self):
        if self.name:
            return self.name
        return 'id %s' % self.id

    def get_site_url(self):
        if not self.site_url:
            return None
        url = urlparse(self.site_url)
        return url.netloc


# class AdmReferrals(models.Model):
#     id = models.IntegerField(primary_key=True, help_text='Идентификатор реферала')
#     user = models.ForeignKey(User)
#     username = models.CharField(max_length=200, help_text='Логин реферала')
#     payment = models.IntegerField(null=True, blank=True)
#
#     def __str__(self):
#         return '%s %s' % (self.id, self.username)


class AdmUserProfile(models.Model):
    id = models.IntegerField(primary_key=True, help_text='Идентификатор веб-мастера')
    user = models.OneToOneField(User)

    username = models.CharField(max_length=200, null=True, blank=True,
                                help_text='Логин веб-мастера')
    email = models.CharField(max_length=252, null=True, blank=True, help_text='Адрес электронной почты веб-мастера')
    payment = models.IntegerField(null=True, blank=True)
    referral = models.ForeignKey('self', null=True, blank=True, help_text='Чей реферал')


    def __str__(self):
        return self.user.username
