# -*- coding: utf-8 -*-
import logging
from datetime import timedelta

from celery.schedules import crontab
from celery.task import task, periodic_task
from django.conf import settings

from market.models import MySite
from my_admitad_api.managers import AdmitadApi, search_adm_website
# import os
# from cpashop import settings
from my_admitad_api.models import Website

logger = logging.getLogger(__name__)


@periodic_task(run_every=crontab(hour=23, minute=53))
def update_adm_api():
    """
    Обновляем адмитад
    """
    adm = AdmitadApi()
    adm.update_categories()
    adm.update_websites()
    adm.update_campaigns()
    from custom_profile.models import UserProfile
    user_profiles = UserProfile.objects.filter(admitad_identifier__isnull=False,
                                               admitad_secret_key__isnull=False)
    for user_profile in user_profiles:

        if settings.CELERY_START:
            update_adm_api_user.delay(user_profile.user_id)
        else:
            update_adm_api_user(user_profile.user_id)


@task
def update_adm_api_user(user_id):
    """
    Обновляем адмитад
    """
    from custom_profile.models import UserProfile
    user_profile = UserProfile.objects.get(user_id=user_id)
    logger.info('Обновляем Admitad API юзера %s', user_profile.user)
    client_id = user_profile.admitad_identifier
    client_secret = user_profile.admitad_secret_key
    adm = AdmitadApi(client_id=client_id, client_secret=client_secret,
                     user=user_profile.user,
                     )
    # adm.update_categories()
    adm.update_websites()
    for website in Website.objects.filter(user_id=user_id,
                                          mysite__isnull=False):
        adm.mysite = website.mysite
        adm.campaigns_for_websites()
        if settings.CELERY_START:
            task_adm_add_seller.delay(website.mysite.pk)
        else:
            task_adm_add_seller(website.mysite.pk)
    # adm.update_campaigns()


@task
def task_create_website_adm(site_id, validate=True):
    mysite = MySite.objects.get(site_id=site_id)
    client_id = mysite.user.userprofile.admitad_identifier
    client_secret = mysite.user.userprofile.admitad_secret_key
    adm = AdmitadApi(client_id=client_id, client_secret=client_secret, user=mysite.user, mysite=mysite)
    if mysite.adm_website is None:
        logger.info('Отправляем площадку %s на создание в систему Adm.', mysite)
        adm.create_website_adm()
        if validate:
            task_verify_website_adm(site_id)
        else:
            adm.update_websites()


@task
def task_verify_website_adm(site_id):
    mysite = MySite.objects.get(site_id=site_id)
    if mysite.adm_website:
        client_id = mysite.user.userprofile.admitad_identifier
        client_secret = mysite.user.userprofile.admitad_secret_key
        adm = AdmitadApi(client_id=client_id,
                         client_secret=client_secret,
                         user=mysite.user,
                         mysite=mysite)
        adm.verify_website()
        adm.update_websites()


def task_del_site_adm(site_id=59):
    print(site_id)


@task
def adm_validate_site(site_id):
    mysite = MySite.objects.get(site_id=site_id)
    adm = AdmitadApi(client_id=mysite.user.userprofile.admitad_identifier,
                     client_secret=mysite.user.userprofile.admitad_secret_key,
                     user=mysite.user,
                     mysite=mysite)
    adm.update_websites()
    if mysite.adm_website and not mysite.verify_admitad:
        # Есть сайт, но почему-то нет кода. Сохраняем код, верифицируем.
        logger.info('Есть сайт, но почему-то нет кода. Сохраняем код, верифицируем. %s' % mysite.domain)
        mysite.verify_admitad = mysite.adm_website.verification_code
        mysite.save()
        task_verify_website_adm(site_id)
        return
    if not mysite.adm_website:
        # Нет сайт, пытаемся найти сайт.
        adm_site = search_adm_website(site_id)
        if adm_site:
            # Сайт нашли, делаем верификацию
            mysite.adm_website = adm_site
            mysite.verify_admitad = adm_site.verification_code
            task_verify_website_adm(site_id)
        else:
            # Все грусно, сайт не найден. Создаем сайт и сразу верифицируем
            task_create_website_adm(site_id)
    else:
        adm.verify_website()



@periodic_task(run_every=timedelta(hours=1, minutes=5))
def task_adm_validate_site():
    mysites = MySite.objects.filter(user__isnull=False, manual_verification=False)
    mysites = mysites.filter(user__userprofile__admitad_identifier__isnull=False,
                             user__userprofile__admitad_secret_key__isnull=False)
    for mysite in mysites:
        logger.info('Validate site %s' % mysite.domain)
        adm_validate_site(mysite.site_id)


@task
def task_adm_add_seller(site_id):
    if settings.DEBUG:
        print('Типа начали task_adm_add_seller')
        return
    mysite = MySite.objects.get(site_id=site_id)
    logger.info('task_adm_add_seller(%s), %s' % (site_id, mysite))
    adm = AdmitadApi(client_id=mysite.user.userprofile.admitad_identifier,
                     client_secret=mysite.user.userprofile.admitad_secret_key,
                     user=mysite.user,
                     mysite=mysite)
    sellers = mysite.sellers.all()
    for seller in sellers:
        adm_campaign = seller.adm_campaign
        adm.connecting_site_to_program(adm_campaign.pk)
    adm.campaigns_for_websites()
