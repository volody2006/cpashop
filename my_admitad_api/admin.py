# -*- coding: utf-8 -*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from my_admitad_api.models import *
from my_admitad_api.models import AdmCampaignNameAliases, Website


class AdmCampaignAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'exclusive', 'status')
    list_filter = ['connected', 'status', 'exclusive', 'allow_deeplink', ]
    search_fields = ['name', 'site_url', ]


admin.site.register(AdmCampaign, AdmCampaignAdmin)


class AdmCategoryAdmin(MPTTModelAdmin):
    list_display = ('id', 'name')


admin.site.register(AdmCategory, AdmCategoryAdmin)


class AdmWebsitesKindsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(AdmWebsitesKinds, AdmWebsitesKindsAdmin)


class AdmWebsitesRegionsAdmin(admin.ModelAdmin):
    list_display = ('id', 'region')


admin.site.register(Region, AdmWebsitesRegionsAdmin)


class NoKeyAdmCampaignAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'values')


admin.site.register(NoKeyAdmCampaign, NoKeyAdmCampaignAdmin)


class AdmCampaignNameAliasesAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'campaign')


admin.site.register(AdmCampaignNameAliases, AdmCampaignNameAliasesAdmin)


class WebsiteAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'status')
    list_filter = ['status', ]


admin.site.register(Website, WebsiteAdmin)


class AdmCampaignUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'adm_campaign', 'website', 'connection_status', 'gotolink')
    list_filter = ['connection_status', ]


admin.site.register(AdmCampaignUser, AdmCampaignUserAdmin)


class AdmUserProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'username')
    search_fields = ['username', 'user__username', 'email' ]
admin.site.register(AdmUserProfile, AdmUserProfileAdmin)