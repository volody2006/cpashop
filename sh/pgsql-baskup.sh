#!/usr/bin/env bash

#pg_dump [ параметр-подключения ...] [ параметр ...] [ база_данных ]
pg_dump --dbname=cpashop --host=192.168.11.5 --username=cpashop --no-password --format=tar |bzip2 -9 > /data/backup/cpashop/backup-$(date +%Y-%m-%d).tar.bz2
hostname:port:database:username:password

#pg_restore [параметр-подключения...] [параметр...] [имя_файла]
pg_restore --dbname=cpashop_prod --host=localhost --username=postgres --no-password /home/user/program/backup-2017-04-17.tar
psql --dbname=cpashop_prod --host=localhost --username=postgres --no-password --file=/home/user/program/backup-2017-04-17