# -*- coding: utf-8 -*-

import os

import pytest

# from dj_database_url import parse as db_parse

pytest_plugins = ['base_fuxtures', ]


@pytest.fixture(autouse=True)
def autouse_db(db):
    pass


def pytest_configure():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cpashop.settings')

    from django.conf import settings

    # settings.DATABASES = {'default': db_parse('sqlite://:memory:')}
    settings.DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'cpashop_test',
            'USER': 'postgres',
            'PASSWORD': 'postgres',
            'HOST': 'localhost',
            'PORT': '5432',
        },
    }

    settings.PASSWORD_HASHERS = [
        'django.contrib.auth.hashers.MD5PasswordHasher',
    ]
    settings.CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': ''
        }
    }
    settings.HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'search.elasticsearch2_backend.ElasticsearchSearchEngine',
            'URL': 'http://localhost:9200/',
            'INDEX_NAME': 'test_cpashop',
        },
    }
    # settings.HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
    settings.EMAIL_HOST = 'localhost'
    settings.EMAIL_PORT = 1025
    settings.EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
    settings.WEBMASTER_VERIFICATION = {}
    settings.CELERY_START = False
    settings.IS_TEST = True
    settings.RAVEN_CONFIG = {}
    settings.LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            },
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue'
            }
        },
        'formatters': {
            'main_formatter': {
                'format': '%(levelname)s:%(name)s: %(message)s '
                          '(%(asctime)s; %(filename)s:%(lineno)d)',
                'datefmt': "%Y-%m-%d %H:%M:%S",
            },
            'new_formatter': {
                'format': '%(asctime)s %(levelname)s: %(name)s:%(filename)s:%(lineno)d:  %(message)s ',
                'datefmt': "%Y-%m-%d %H:%M:%S",
            }
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'filters': ['require_debug_true'],
                'class': 'logging.StreamHandler',
                'formatter': 'new_formatter',
            },
            'test_file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': 'logs/test.log',
                'maxBytes': 1024 * 1024 * 5,  # 5 MB
                'backupCount': 7,
                'formatter': 'new_formatter',
                'filters': ['require_debug_false'],
            },
        },
        'loggers': {
            'django.request': {
                'handlers': ['test_file', 'console'],
                'level': 'ERROR',
                'propagate': True,
            },
            'elasticsearch': {
                'handlers': ['console', ],
                'level': "ERROR",
            },
            'urllib3': {
                'handlers': ['console', ],
                'level': "ERROR",
            },
            '': {
                'handlers': ['console', 'test_file', ],
                'level': "DEBUG",
            },
        }
    }


def pytest_ignore_collect(path, config):
    return '_test_cases' not in str(path)
