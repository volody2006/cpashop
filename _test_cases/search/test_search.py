# -*- coding: utf-8 -*-
import pytest
from haystack.query import SearchQuerySet

from market.models import Lot


@pytest.fixture(params=['объектив', 'Ушастик', 'Yshast'], )
def url_search(request):
    return f'/search/?q={request.param}'


@pytest.fixture
def context_search(client, url_search):
    response = client.get(url_search)
    return response.context


@pytest.fixture
def lots(mixer, my_site, clear_index):
    from market import models
    # clear_search_index()
    mixer.cycle(5).blend('market.Lot',
                         status=models.ENABLED,
                         name="объектив",
                         seller__name='Ушастик',
                         seller__mysite=my_site)
    mixer.cycle(5).blend('market.Lot',
                         status=models.ENABLED,
                         name="фотоаппарат",
                         seller__name='Yshast',
                         seller__mysite=my_site)
    mixer.cycle(5).blend('market.Lot',
                         status=models.ENABLED,
                         name="фотоаппарат",
                         seller__name='Not_Publ',
                         seller__mysite=my_site)


def test_index(lots):
    assert Lot.objects.all().count() == 15
    assert SearchQuerySet().all().count() == 15


# def test_view_site(lots, context_search):
#     # Проблема в фикстуре <lots> , неправильно генерируются лоты
#     print(context_search['object_list'])
#     assert context_search['object_list'].__len__() == 5
