# -*- coding: utf-8 -*-

import pytest

from my_admitad_api.managers import BaseClass


@pytest.fixture
def base_class():
    return BaseClass()


@pytest.mark.parametrize('value', ['', ' ', False, True, None])
def test_read_datetime_bad_value(base_class, value):
    assert base_class.read_datetime(value) is None

#
# @pytest.mark.parametrize('value_in, value_out', [
#     ('2014-03-27T09:50:23', datetime.datetime(2014, 3, 27, 9, 50, 23, tzinfo='UTC')),
# ])
# def test_read_datetime_god_value(base_class, value_in, value_out):
#     assert base_class.read_datetime(value_in) == value_out
