# -*- coding: utf-8 -*-
JSON_CREARE_WEBSITE = {
    'id': 42,
    'status': 'new',
    'kind': 'website',
    'name': 'FooBar',
    'categories': [{
        'id': 1,
        'language': 'en',
        'name': 'Cat1',
        'parent': None
    }, {
        'id': 2,
        'language': 'en',
        'name': 'Cat2',
        'parent': None
    }],
    'adservice': None,
    'creation_date': '2016-10-10T11:54:45',
    'description': 'Lord not only five centuries, but also the leap into electronic typesetting, '
                   'remaining essentially unchanged. It was popularised in the 1960s with the release of '
                   'Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing '
                   'software like Aldus PageMaker including versions of Lorem Ipsum.',
    'is_old': False,
    'mailing_targeting': True,
    'regions': ['RU'],
    'site_url': 'https://foobar.bar/',
    'validation_passed': False,
    'verification_code': '244a5d4a14',
    'atnd_hits': 500,
    'atnd_visits': 100,
}
