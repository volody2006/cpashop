# -*- coding: utf-8 -*-

import pytest
from django.contrib.auth.models import User

from market.models import MySite
from my_admitad_api import managers
from my_admitad_api import models
from my_admitad_api.models import AdmCampaignUser


@pytest.fixture
def user(mixer):
    user = mixer.blend(User)
    user.userprofile.admitad_identifier = 'Test admitad_identifier'
    user.userprofile.admitad_secret_key = 'Test admitad_secret_key'
    return user


@pytest.fixture
def adm_website(mixer):
    return mixer.blend('my_admitad_api.Website')


@pytest.fixture
def mysite(mixer):
    return mixer.blend('market.MySite')


@pytest.fixture
def mysite_adm_website(mixer, adm_website, user):
    return mixer.blend('market.MySite', adm_website=adm_website, user=user, verify_admitad='Test verify_admitad')


@pytest.fixture
def adm_campaign(mixer):
    return mixer.blend('my_admitad_api.AdmCampaign', id=6)


@pytest.fixture
def create_website_response():
    return {
        'id': 42,
        'status': 'new',
        'kind': 'website',
        'name': 'FooBar',
        'categories': [{
            'id': 1,
            'language': 'en',
            'name': 'Cat1',
            'parent': None
        }, {
            'id': 2,
            'language': 'en',
            'name': 'Cat2',
            'parent': None
        }],
        'adservice': None,
        'creation_date': '2016-10-10T11:54:45',
        'description': 'Lord not only five centuries, but also the leap into electronic typesetting, '
                       'remaining essentially unchanged. It was popularised in the 1960s with the release of '
                       'Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing '
                       'software like Aldus PageMaker including versions of Lorem Ipsum.',
        'is_old': False,
        'mailing_targeting': True,
        'regions': ['RU'],
        'site_url': 'https://foobar.bar/',
        'validation_passed': False,
        'verification_code': '244a5d4a14',
        'atnd_hits': 500,
        'atnd_visits': 100,
    }


@pytest.fixture
def update_website_response():
    return {
        "results": [
            {
                "status": "new",
                "kind": "social_app",
                "is_old": False,
                "name": "vkontakte.ru",
                'creation_date': '2016-10-10T11:54:45',
                "verification_code": "d2b3d05c3d",
                "site_url": "http://vkontakte.ru/app123",
                "regions": [
                    {
                        "region": "RU",
                        "id": 8
                    }
                ],
                "adservice": {
                    "id": 5,
                    "name": "VKontakte"
                },
                "id": 40,
                "categories": [
                    {
                        "name": "Shop",
                        "parent": None,
                        "id": 1
                    },
                    {
                        "name": "Online Games",
                        "parent": None,
                        "id": 2
                    },
                    {
                        "name": "Other",
                        "parent": None,
                        "id": 5
                    },
                    {
                        "name": "Test",
                        "parent": None,
                        "id": 16
                    }
                ],
                "description": "description"
            }
        ],
        "_meta": {
            "count": 1,
            "limit": 1,
            "offset": 1
        }
    }


@pytest.fixture
def verify_website_response():
    return {
        "message": "Ad space validated successfully.",
        "success": "Accepted"
    }


def test_admitad_api_create_website_adm(user, mysite, monkeypatch, create_website_response):
    def my_json(*args, **kwargs):
        return create_website_response

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._create_website_adm', my_json)

    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite)

    web_site1 = adm.create_website_adm()
    web_site2 = models.Website.objects.get(id=42)
    assert web_site1 == web_site2
    assert web_site2.verification_code == '244a5d4a14'
    assert web_site2.mysite == mysite
    assert web_site2.mysite.verify_admitad == '244a5d4a14'


def test_admitad_api_create_website_adm_bad_json(user, mysite, monkeypatch):
    def my_json(*args, **kwargs):
        return {
            'id': 42,
            'status': '',
            'kind': '',
            'name': '',
            'categories': [],
            'adservice': None,
            'creation_date': '',
            'description': '',
            'is_old': False,
            'mailing_targeting': True,
            'regions': [],
            'site_url': 'https://foobar.bar/',
            'validation_passed': False,
            'verification_code': '244a5d4a14',
            'atnd_hits': 0,
            'atnd_visits': 0,
        }

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._create_website_adm', my_json)

    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite)

    web_site1 = adm.create_website_adm()
    web_site2 = models.Website.objects.get(id=42)
    assert web_site1 == web_site2
    assert web_site2.verification_code == '244a5d4a14'
    assert web_site2.mysite == mysite
    assert web_site2.mysite.verify_admitad == '244a5d4a14'


def test_admitad_api_create_website_adm_not_mysite(user):
    with pytest.raises(managers.ApiError):
        adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user)
        adm.create_website_adm()


def test_admitad_api_create_website_mysite_is_adm_website(user, mysite_adm_website):
    with pytest.raises(managers.ApiError):
        adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)
        adm.create_website_adm()


def test_update_website(user, monkeypatch, update_website_response):
    def my_json(*args, **kwargs):
        return update_website_response

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi.get_websites', my_json)

    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user)
    adm.update_websites()
    websites = models.Website.objects.all()

    assert websites.count() == 1
    assert websites[0].verification_code == 'd2b3d05c3d'


def test_verify_website_good(user, monkeypatch, mysite_adm_website):
    def my_json(*args, **kwargs):
        return {
            "message": "Ad space validated successfully.",
            "success": "Accepted"
        }

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._verify_website', my_json)

    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)

    assert adm.verify_website() is True
    assert MySite.objects.get(site_id=mysite_adm_website.site_id).manual_verification is True


def test_verify_website_bad(user, monkeypatch, mysite_adm_website):
    def my_json(*args, **kwargs):
        return {
            "message": "Ad space validated successfully.",
            "success": "Error"
        }

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._verify_website', my_json)

    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)

    assert adm.verify_website() is False
    assert MySite.objects.get(site_id=mysite_adm_website.site_id).manual_verification is False


def test_connecting_site_to_program(user, monkeypatch, mysite_adm_website, adm_campaign):
    def my_json(*args, **kwargs):
        return {
            "message": "Request for adding the campaign CampaignName was successfully created.",
            "success": "OK"
        }

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._connecting_site_to_program', my_json)
    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)
    assert adm.connecting_site_to_program(adm_campaign.id) is True


def test_not_connecting_site_to_program(user, monkeypatch, mysite_adm_website, adm_campaign):
    def my_json(*args, **kwargs):
        return {
            "message": "Request for adding the campaign CampaignName was successfully created.",
            "success": "ERROR"
        }

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._connecting_site_to_program', my_json)
    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)
    assert adm.connecting_site_to_program(adm_campaign.id) is False


def test_add_program_site(user, monkeypatch, mysite_adm_website, adm_campaign):
    def _connecting_site_to_program(*args, **kwargs):
        return {
            "message": "Request for adding the campaign CampaignName was successfully created.",
            "success": "OK"
        }

    def _campaigns_for_websites(*args, **kwargs):
        return {"results": [
            {"status": "active",
             "connection_status": "active",
             "rating": "5.00",
             "image": "//cdn.admitad.com/media/campaign/images/2012/06/13/4b9182dae3a6758eb12f76a76a5b3f26.jpg",
             "show_products_links": False,
             "traffics": [
                 {"enabled": False,
                  "name": "Type 1",
                  "id": 1},
             ],
             "description": "Gmail is a mail service by google",
             "avg_hold_time": None,
             "actions": [
                 {"payment_size": "5USD",
                  "type": "lead",
                  "name": "Example action",
                  "id": 42}
             ],
             "site_url": "http://www.gmail.com/",
             "regions": [
                 {
                     "region": "RU"
                 },
                 {
                     "region": "US"
                 }
             ],
             "actions_detail": [
                 {
                     "tariffs": [
                         {
                             "action_id": 42,
                             "rates": [
                                 {
                                     "price_s": "0.00",
                                     "tariff_id": 42,
                                     "country": None,
                                     "date_s": "2014-11-06",
                                     "is_percentage": False,
                                     "id": 78,
                                     "size": "5.00"
                                 }
                             ],
                             "id": 42,
                             "name": "Tariff name"
                         }
                     ],
                     "type": "lead",
                     "name": "Example action",
                     "id": 42
                 }
             ],
             "currency": "USD",
             "goto_cookie_lifetime": 45,
             "geotargeting": True,
             "coupon_iframe_denied": False,
             "allow_deeplink": True,
             "cr": 84.38,
             "activation_date": "2010-03-31 19:05:39",
             "modified_date": "2010-04-12 18:05:32",
             "moderation": False,
             "gotolink": "http://ad.admitad.com/goto/some_link/",
             "ecpc": 23.74,
             "id": 6,
             "products_csv_link": "http://export.admitad.com/ru/webmaster/websites/xxx/products/export_adv_products/?user=webmaster&code=db403bd6d2&advcampaign_id=xxx&format=csv",
             "products_xml_link": "http://export.admitad.com/ru/webmaster/websites/xxx/products/export_adv_products/?user=webmaster&code=db403bd6d2&advcampaign_id=xxx&format=xml",
             "feeds_info": [
                 {
                     "name": "Campaign1",
                     "admitad_last_update": "2015-10-27 15:35:58",
                     "advertiser_last_update": "2015-10-27 08:38:00",
                     "csv_link": "http://export.admitad.com/ru/webmaster/websites/22/products/export_adv_products/?user=webmaster1&code=db403bd6d2&feed_id=xxx&format=csv",
                     "xml_link": "http://export.admitad.com/ru/webmaster/websites/22/products/export_adv_products/?user=webmaster1&code=db403bd6d2&feed_id=xxx&format=xml"
                 }
             ],
             "categories": [
                 {
                     "language": "en",
                     "name": "Shop",
                     "parent": None,
                     "id": 1
                 },
             ],
             "name": "Campaign1"
             }
        ],
            "_meta": {
                "count": 4,
                "limit": 1,
                "offset": 0
            }}

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._connecting_site_to_program',
                        _connecting_site_to_program)
    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._campaigns_for_websites', _campaigns_for_websites)
    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)
    adm.add_program_site(adm_campaign.id)
    assert AdmCampaignUser.objects.all().count() == 1
    assert AdmCampaignUser.objects.filter(user=adm.user, adm_campaign=adm_campaign,
                                          website=adm.mysite.adm_website).exists() is True


def test_remove_program_site(user, monkeypatch, mysite_adm_website, adm_campaign):
    def my_json(*args, **kwargs):
        return {
            "message": "Request for adding the campaign CampaignName was successfully created.",
            "success": "ERROR"
        }

    monkeypatch.setattr('my_admitad_api.managers.AdmitadApi._connecting_site_to_program', my_json)
    adm = managers.AdmitadApi(client_id='qwe', client_secret='sekret', user=user, mysite=mysite_adm_website)
    adm.remove_program_site(adm_campaign.id)
    assert AdmCampaignUser.objects.all().count() == 0
    assert AdmCampaignUser.objects.filter(user=adm.user, adm_campaign=adm_campaign,
                                          website=adm.mysite.adm_website).exists() is False

# {'message': 'Заявка на добавление кампании Связной успешно создана.', 'success': 'OK'}
# >>> adm.add_program_site(9896)
# {'message': 'Заявка на добавление кампании Media Markt успешно создана.', 'success': 'OK'}
#
