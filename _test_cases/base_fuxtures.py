# -*- coding: utf-8 -*-
import pytest

from base.base_test_lib import clear_search_index


@pytest.fixture(scope='session')
def mixer():
    "Returns a Mixer instance"
    from mixer.backend.django import mixer
    return mixer


@pytest.fixture
def site(mixer):
    return mixer.blend('sites.Site', domain='testserver')


@pytest.fixture
def url():
    from django.core.urlresolvers import reverse
    return reverse('main_view')


@pytest.fixture
def my_site(mixer, site):
    return mixer.blend('market.MySite', site=site)


@pytest.fixture
def clear_index():
    print('Начало теста, очищаем индекс')
    clear_search_index()
    yield
    print("Тест окончен, очищаем индекс")
    clear_search_index()


@pytest.fixture
def context(client, url):
    response = client.get(url)
    return response.context
