# -*- coding: utf-8 -*-

import pytest

from menu.models import Menu


@pytest.mark.parametrize('key, expected_value', [
    ('Test', 'test'),
    ('Проверка', 'proverka'),
    ('Tom Kryes', 'tom-kryes')
])
def test_slug_gen(key, expected_value):
    Menu.objects.create(name=key)
    menu = Menu.objects.get(name=key)
    assert menu.slug == expected_value


@pytest.mark.parametrize('value', ['/about/', 'http://test.ru', 'https://test.com'])
def test_menu_is_url(mixer, value):
    menu = mixer.blend('menu.Menu', link=value)
    assert menu.get_absolute_url() == value

# def test_menu_not_url(mixer):
#     menu = mixer.blend('menu.Menu')
#     assert menu.get_absolute_url() == '/catalog/%s/' % slugify(menu.name)
