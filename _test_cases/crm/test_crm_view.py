# -*- coding: utf-8 -*-
import pytest
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.urls import reverse

from crm import views
from custom_profile.forms import RegistrationForm
from custom_profile.models import UserProfile
from custom_profile.views import RegisterFormView
from market.models import MySite

USER_NAME = 'TestStalin'
USER_PASSWORD = 'IamStalin'
USER_EMAIL = 'stalin@stalingrad.us'


@pytest.fixture
def crm_site(mixer):
    site, create = Site.objects.get_or_create(domain='testserver', name='testserser')
    MySite.objects.update_or_create(site=site,
                                    defaults={'template': mixer.blend('market.Template',
                                                                      name='dashboard',
                                                                      folder='dashboard',
                                                                      hidden=True,
                                                                      crm_template=True)})
    return site


@pytest.fixture
def user():
    return User.objects.create_user(username=USER_NAME, email=USER_EMAIL, password=USER_PASSWORD)


@pytest.fixture
def client_login(user, client, crm_site):
    client.login(username=USER_NAME, password=USER_PASSWORD)
    return client


@pytest.fixture
def user_template(mixer, user):
    return mixer.blend('market.Template',
                       name='User_1',
                       folder='User_1',
                       hidden=False,
                       crm_template=False,
                       user=user)


@pytest.fixture
def not_user_template(mixer, user):
    return mixer.blend('market.Template',
                       name='User_2',
                       folder='User_2',
                       hidden=False,
                       crm_template=False)


@pytest.fixture
def user_template_hidden(user_template):
    user_template.hidden = True
    user_template.save()
    return user_template


@pytest.fixture
def user_template_crm_template(user_template):
    user_template.crm_template = True
    user_template.save()
    return user_template


@pytest.fixture
def user_template_crm_template_and_hidden(user_template):
    user_template.crm_template = True
    user_template.hidden = True
    user_template.save()
    return user_template


def test_form_register():
    form_data = {'username': USER_NAME,
                 'password': USER_PASSWORD,
                 'password2': USER_PASSWORD,
                 'email': USER_EMAIL}
    form = RegistrationForm(data=form_data)
    assert form.is_valid() is True
    form.save()
    user = User.objects.get(username=USER_NAME)
    assert user.email == USER_EMAIL
    assert user.is_staff is False
    assert user.is_active is True


def test_registration(client):
    url = reverse('register')
    data = {'username': USER_NAME,
            'password': USER_PASSWORD,
            'password2': USER_PASSWORD,
            'email': USER_EMAIL,
            }
    client.get(url)
    response = client.post(url, data)
    assert response.resolver_match.func.__name__ == RegisterFormView.as_view().__name__
    assert response.status_code == 302
    assert response.url, reverse('crm_dashboard_view')
    user = User.objects.get(username=USER_NAME)
    assert UserProfile.objects.filter(user=user).exists() is True
    client.logout()
    assert client.login(username=USER_NAME, password=USER_PASSWORD) is True


def test_view_mysite_list(client_login):
    response = client_login.get(reverse('mysite_list'))
    assert response.resolver_match.func.__name__ == views.MySiteListView.as_view().__name__


def test_mysite_list_not_login(client, crm_site):
    response = client.get(reverse('mysite_list'))
    assert response.status_code == 302
    redir_url = reverse('login') + '?next=' + reverse('mysite_list')
    assert response.url == redir_url


def test_template_page(client_login):
    response = client_login.get(reverse('template_list'))
    assert response.context['object_list'].count() == 0
    assert response.resolver_match.func.__name__ == views.MyTemplateListView.as_view().__name__


def test_user_template_page(client_login, user_template):
    # Юзер видит свои шаблоны.
    response = client_login.get(reverse('template_list'))
    assert user_template in response.context['object_list']


def test_user_template_page_hidden(client_login, user_template_hidden):
    # Юзер не видит свои шаблоны, если они скрыты.
    response = client_login.get(reverse('template_list'))
    assert user_template_hidden not in response.context['object_list']


def test_user_template_page_crm_template(client_login, user_template_crm_template):
    response = client_login.get(reverse('template_list'))
    assert user_template_crm_template not in response.context['object_list']


def test_user_delete_temlate(client_login, user_template):
    response = client_login.get(reverse('template_delete', args=[user_template.id, ]))
    assert response.status_code == 200


def test_user_delete_not_user_template(client_login, not_user_template):
    response = client_login.get(reverse('template_delete', args=[not_user_template.id, ]))
    assert response.status_code == 404

# def test_webmaster_custom_site():
#     # Вебмастер добавляет свои сайты
#     pass
