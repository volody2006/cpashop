# -*- coding: utf-8 -*-
from django.core import mail


def test_send_email():
    # Send message.
    mail.send_mail('Subject here', 'Here is the message.',
                   'from@example.com', ['to@example.com'],
                   fail_silently=False)

    # Test that one message has been sent.
    assert len(mail.outbox) == 1

    # Verify that the subject of the first message is correct.
    assert mail.outbox[0].subject == 'Subject here'
