# -*- coding: utf-8 -*-
import os

import pytest
from django.conf import settings
from httmock import urlmatch, HTTMock, all_requests

from import_lots.exceptions import FeedImportError
from import_lots.managers import Downloader


@pytest.fixture
def feed(mixer):
    yield mixer.blend('market.Feed',
                      xml_url='http://test.xxx/xml',
                      run_auto=True,
                      get_param_save=False)
    try:
        os.remove(os.path.join(settings.TMP_ROOT, '%s.yml' % 'test_file'))
    except FileNotFoundError:
        pass


@urlmatch()
def xml_mock(url, request):
    return 'Feeling lucky, punk?'


def test_downloader(feed):
    with HTTMock(xml_mock):
        downloader = Downloader(feed=feed, test_run=True)
        downloader.run()
        assert downloader.filename == os.path.join(settings.TMP_ROOT, '%s.yml' % 'test_file')


@all_requests
def response_content_404(url, request):
    return {'status_code': 404,
            'content': 'Oh hai'}


def test_fail_downloader(feed):
    with HTTMock(response_content_404):
        with pytest.raises(FeedImportError):
            downloader = Downloader(feed=feed, test_run=True)
            downloader.run()
