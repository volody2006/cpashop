# -*- coding: utf-8 -*-
import os
import shutil

import pytest
from django.conf import settings

from import_lots.managers import Handler
from market.models import Lot, ENABLED


@pytest.fixture
def feed(mixer):
    file_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_name = 'test.xml'
    test_file = os.path.join(file_dir, 'import_lots', file_name)
    # Копируем файл в рабочую папку
    filename = os.path.join(settings.TMP_ROOT, '%s.yml' % 'test_parse_file')
    shutil.copyfile(test_file, filename)
    yield mixer.blend('market.Feed',
                      xml_url='http://test.ru/xml',
                      run_auto=True,
                      get_param_save=False)
    try:
        os.remove(filename)
        print("удалили временный файл")
    except FileNotFoundError:
        pass


@pytest.fixture
def parser(feed):
    filename = os.path.join(settings.TMP_ROOT, '%s.yml' % 'test_parse_file')
    handler = Handler(feed=feed, filename=filename)
    handler.run()
    print('Закончили парсить файл')


class TestParseFeedTest:
    # def test_parse_feed(self, feed, parser):
    #     assert Lot.objects.all().count() == 4

    def test_enadled_lots(self, feed, parser):
        print(Lot.objects.all().count())
        for lot in Lot.objects.all():
            print(lot, lot.status)
        lots = Lot.objects.filter(status=ENABLED)

        assert lots.count() == 4

    # def test_good_one_lot(self, feed, parser):
    #     lot_1 = Lot.objects.get(offer_id=1)
    #     assert lot_1.name == 'Test lot name'
    #     assert lot_1.price == 5000
    #     assert lot_1.url == 'https%3A%2F%2Fblackstarwear.ru%2Fproduct%2F10904-219%2F'
    #
    # def test_lot_3_type_vendor_model(self, feed, parser):
    #     lot_3 = Lot.objects.get(offer_id=3)
    #
    #     assert lot_3.description == 'test description'
    #     assert lot_3.name == 'Объектив Tamron SP AF 28-75mm F/2.8 XR Di LD Aspherical (IF) Canon EF (A09E)'
