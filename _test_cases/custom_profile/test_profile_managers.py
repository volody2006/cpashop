# -*- coding: utf-8 -*-
import datetime

import pytest
import pytz
from django.contrib.auth.models import User

from custom_profile.managers import UserTokenGenerator


@pytest.fixture
def user():
    # MQ-7bc6023d578fc33d7e6a
    return User.objects.create_user(username='Test',
                                    email='test@test.ru',
                                    password='passord1',
                                    id=1,
                                    date_joined=pytz.utc.localize(datetime.datetime(2015, 10, 21, 23, 29)))


class TestUserTokenGenerator:
    def get_utg(self):
        utg = UserTokenGenerator()
        return utg

    def test_make_token(self, user):
        assert self.get_utg().make_token(user) == 'MQ-7bc6023d578fc33d7e6a'

    @pytest.mark.parametrize('token', ['MQ-7bc6023d578fc33d7e6a', '"MQ-7bc6023d578fc33d7e6a"'])
    def test_check_token(self, user, token):
        assert self.get_utg().check_token(user, token) == True

    def test_get_user(self, user):
        assert self.get_utg().get_user('MQ') == user

    def test_get_uid(self, user):
        assert self.get_utg().get_uid(user) == 'MQ'

    @pytest.mark.parametrize('key', ['MQ-7bc6023d578fc33d7e6a', '"MQ-7bc6023d578fc33d7e6a"'])
    def test_valide_token(self, user, key):
        assert self.get_utg().valide_token(key) is True

    @pytest.mark.parametrize('key', ['MQ7bc6023d578fc33d7e6a',
                                     'MQ--7bc6023d578fc33d7e6a', '', '-',
                                     None])
    def test_not_valide_token(self, user, key):
        assert self.get_utg().valide_token(key) is False

    @pytest.mark.parametrize('key', ['MQ-7bc6023d578fc33d7e6a', '"MQ-7bc6023d578fc33d7e6a"'])
    def test_not_user_not_valide_token(self, key):
        assert self.get_utg().valide_token(key) is False
