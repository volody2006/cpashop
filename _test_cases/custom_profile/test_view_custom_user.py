# -*- coding: utf-8 -*-

import pytest
from django.contrib.auth.models import User
from django.urls import reverse


@pytest.fixture
def url_register():
    return reverse('register')


@pytest.fixture
def temlate(mixer, my_site):
    temlate = mixer.blend('market.Template',
                          name='dashboard',
                          folder='dashboard',
                          hidden=False,
                          crm_template=True)
    my_site.temlate = temlate
    my_site.save()


def test_register_user(url_register, client):
    response = client.get(url_register)
    assert response.status_code == 200
    # template = response.templates[0]
    # assert template.name == 'dashboard/registration/register.html'


@pytest.mark.parametrize('username, email', [
    ('test', 'test@test.ru'),
    ('Test', 'Test@test.ru'),
    ('Test.test', 'Test.test@test.ru')
])
def test_register_user_lending(url_register, client, username, email, mailoutbox):
    url_register = url_register + str('?email=%s' % email)
    response = client.get(url_register)
    user = User.objects.get(email=email)
    print(111, user.userprofile.referer_url)
    assert user.username == username
    assert user.userprofile.reg_type == user.userprofile.LENDINGS
    assert len(mailoutbox) == 1


@pytest.mark.parametrize('username, email', [
    ('test', 'test@test.ru'),
    ('Test', 'Test@test.ru'),
    ('Test.test', 'Test.test@test.ru')
])
def test_register_user_lending_reset_password(url_register, client, username, email, mailoutbox):
    url_register = url_register + str('?email=%s' % email)
    response = client.get(url_register)
    print('Пользователь два раза отправил данные')
    response = client.get(url_register)
    assert User.objects.filter(email__iexact=email).count() == 1
    assert len(mailoutbox) == 2


@pytest.mark.parametrize('username, email', [
    ('test', 'test_test.ru'),
    ('Test', 'Test@ test.ru'),
    ('Test.test', 'Test.test@/test.ru')
])
def test_register_user_lending_bad_email(url_register, client, username, email, mailoutbox):
    url_register = url_register + str('?email=%s' % email)
    response = client.get(url_register)
    assert User.objects.filter(email=email).exists() is False
    assert len(mailoutbox) == 0
