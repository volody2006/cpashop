# -*- coding: utf-8 -*-
import uuid

import docker as libdocker
import pytest


@pytest.fixture(scope='session')
def session_id():
    return str(uuid.uuid4())


@pytest.fixture(scope='session')
def docker():
    return libdocker.DockerClient(version='auto')


@pytest.yield_fixture(scope='session')
def redis_server(unused_port, session_id, docker):
    docker.pull('redis')
    port = unused_port()
    container = docker.create_container(
        image='redis',
        name='test-redis-{}'.format(session_id),
        ports=[6379],
        detach=True,
        host_config=docker.create_host_config(
            port_bindings={6379: port}))
    docker.start(container=container['Id'])
    yield port
    docker.kill(container=container['Id'])
    docker.remove_container(container['Id'])

# @pytest.fixture
# def redis_client(redis_server):
#     for i in range(100):
#         try:
#             client = redis.StrictRedis(host='127.0.0.1',
#                                        port=port, db=0)
#             client.get('some_key')
#             return client
#         except redis.ConnectionError:
#             time.sleep(0.01)
#
#
# def test_redis(redis_client):
#     redis_client.set(b'key', b'value')
#     assert redis_client.get(b'key') == b'value'
