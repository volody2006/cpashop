# -*- coding: utf-8 -*-
import pytest


@pytest.fixture
def url_goto(url):
    from goto_url import utils
    return utils.goto_url(url)


def test_goto_view(client, url_goto, url):
    response = client.get(url_goto)
    from goto_url.views import GotoView
    assert response.resolver_match.func.__name__ == GotoView.as_view().__name__
    assert response.url == url


@pytest.mark.parametrize('key', ['https://google.com/', 'http://google.com/'])
def test_bad_goto_view(client, key):
    response = client.get('/goto/%s' % key)
    assert response.status_code == 302
    assert response.url == key


@pytest.mark.parametrize('key', ['ya.ru/', 'google.com/'])
def test_bad_goto_view_2(client, key):
    response = client.get('/goto/%s' % key)
    assert response.status_code == 302
    assert response.url == 'http://%s' % key
