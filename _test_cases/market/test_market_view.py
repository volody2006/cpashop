# coding: utf-8

import pytest
from django.contrib.sites.models import Site
from django.urls import reverse

from goto_url.utils import goto_url
from market.models import DISABLED, MySite, Template
from market.templatetags.deeplink_lot import deeplink_lot
from market.views import LotDetailView, MainPageView


@pytest.fixture
def lots(mixer, my_site, clear_index):
    from market import models
    seller = mixer.blend('market.Seller', )
    my_site.sellers.add(seller)
    return mixer.cycle(5).blend('market.Lot',
                                status=models.ENABLED,
                                name=mixer.RANDOM,
                                seller=seller,
                                offer_id=mixer.RANDOM
                                )


@pytest.fixture
def foreign_lots(mixer, clear_index):
    from market import models
    return mixer.cycle(5).blend('market.Lot', status=models.ENABLED, name=mixer.RANDOM)


def test_context_processors_site1(client, url, site):
    response = client.get(url)

    assert response.context['SITE'].domain == site.domain
    assert not response.context['verify_admitad']


def test_context_processors_site2(my_site, url, client):
    my_site.verify_admitad = 'qwerty'
    my_site.save()
    response = client.get(url)

    assert response.context['verify_admitad'] == my_site.verify_admitad


def test_index_page_status_code_200(url, client):
    response = client.get(url)

    assert response.status_code == 200


def test_index_page_renders_a_correct_template(url, client):
    response = client.get(url)
    template = response.templates[0]

    assert template.name == 'default/index.html'


@pytest.mark.parametrize('key, expected_value', [
    ('new_lots', 5),
    ('sale', 5),
    ('special', 0)
], ids=[
    'new lots is 5',
    'sale goods is 5',
    'no special offers'
])
def test_market_view_ENABLED_lot(lots, url, client, key, expected_value):
    response = client.get(url)
    template = response.templates[0]
    # for lot in lots:
    #     lot.save()
    #     print(lot, lot.status, lot.seller.sites)
    # print(response.context)
    assert response.status_code == 200
    assert template.name == 'default/index.html'
    assert len(response.context[key]) == expected_value


@pytest.mark.parametrize('status', ['DISABLED', 'DRAFT', 'DELETE'])
@pytest.mark.parametrize('key, expected_value', [
    ('new_lots', 0),
    ('sale', 0),
    ('special', 0)
], ids=[
    'no new lots',
    'no sales lots',
    'no special offers'
])
def test_market_no_goods_when_non_public_lots(lots, client, url, key, expected_value, status):
    from market import models

    for lot in lots:
        lot.status = getattr(models, status)
        lot.save()

    response = client.get(url)

    assert len(response.context[key]) == expected_value


def test_not_mysite(url, client):
    client.get(url)
    assert MySite.objects.all().count() == 1


class TestMarketMainAndCategory:
    @pytest.fixture
    def template(self):
        template, create = Template.objects.update_or_create(name='trendsetter',
                                                     folder='trendsetter',
                                                     hidden=False,
                                                     crm_template=False)
        return template

    @pytest.fixture
    def my_site_temlate(self, template):
        site, create = Site.objects.update_or_create(domain='testserver')
        mysite, create = MySite.objects.update_or_create(site=site,
                                                 defaults={'template':template})
        return mysite

    def test_main_page_status_200(sel, client):
        response = client.get('/')
        assert response.status_code == 200
        assert response.resolver_match.func.__name__ == MainPageView.as_view().__name__

    def test_template(self, client, my_site_temlate):
        response = client.get('/')
        template = response.templates[0]
        assert template.name == 'trendsetter/index.html'

    # def test_cat_page_status_200(sel, client):
    #     response = client.get('/')


class TestMarketClientLotDetailView:
    @pytest.fixture
    def lot_url(self, lots):
        return reverse('lot_view', args=[lots[0].pk, ])

    def test_lot_detail_view(self, lot_url, client, lots):
        # Товар есть, но пришли не по каноническому урлу
        response = client.get(lot_url)
        assert response.resolver_match.func.__name__ == LotDetailView.as_view().__name__
        assert response.status_code == 301
        assert response.url == reverse('slug_lot_view', kwargs={'slug': lots[0].web_slug, 'pk': lots[0].pk})

    def test_lot_404(self, client):
        # Товара нет и не было, 404
        url_404 = reverse('lot_view', args=[1111, ])
        response = client.get(url_404)
        assert response.resolver_match.func.__name__ == LotDetailView.as_view().__name__
        assert response.status_code == 404

    def test_bad_slug_lot(self, client, lots):
        url_bad_slud = '/lot/bla-bal-baka-%s/' % lots[0].pk
        response = client.get(url_bad_slud)
        assert response.status_code == 301
        assert response.resolver_match.func.__name__ == LotDetailView.as_view().__name__
        assert response.url == reverse('slug_lot_view', kwargs={'slug': lots[0].web_slug, 'pk': lots[0].pk})

    def test_good_slug_lot(sel, client, lots):
        url_good_slug = reverse('slug_lot_view', kwargs={'slug': lots[0].web_slug, 'pk': lots[0].pk})
        response = client.get(url_good_slug)
        assert response.status_code == 200
        assert response.resolver_match.func.__name__ == LotDetailView.as_view().__name__

    def test_lot_disable_redirect_arhive(self, lots, client):
        lot = lots[1]
        lot.name = 'David Johns'
        lot.status = DISABLED
        lot.save()
        url = reverse('lot_view', args=[lot.pk, ])
        response = client.get(url)
        assert response.resolver_match.func.__name__ == LotDetailView.as_view().__name__
        assert response.status_code == 301
        kanon_url = '/%s-%sa/' % ('david-johns', lot.pk)
        assert kanon_url == response.url


class TestMarketDeeplinkTest:
    seller_url = 'http://test_test.test'

    def test_tag_deeplink_lot_not_lot(self, context):
        assert deeplink_lot(context=context) == '#'

    def test_tag_deeplink_lot_is_lot_2(self, mixer, context):
        lot = mixer.blend('market.Lot', url=self.seller_url)
        assert deeplink_lot(context=context, lot=lot) == goto_url(self.seller_url)

    def test_deeplink_not_site_id(self, mixer):
        lot = mixer.blend('market.Lot', url=self.seller_url)
        assert lot.get_deeplink() == goto_url(self.seller_url)
