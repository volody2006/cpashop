# -*- coding: utf-8 -*-

from django.contrib.sites.models import Site
from django.urls import reverse

from market.models import Deeplink, DELETE, DISABLED, ENABLED, DRAFT


class TestMarketModels:
    def test_template(self, mixer):
        folder = 'test_folder'
        tem = mixer.blend('market.Template', folder=folder)
        assert str(tem) == tem.folder
        assert str(tem) == folder

    def test_mysite_str(self, mixer):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend('market.MySite', site=site)
        assert str(mysite) == 'test_test.test'
        assert mysite.name == 'test_test.test'

    def test_mysite_get_verify_admitad_false(self, mixer):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend('market.MySite', site=site)
        assert mysite.get_verify_admitad() is False

    def test_mysite_get_verify_admitad_1(self, mixer):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend('market.MySite', site=site)
        mysite.verify_admitad = 'qwerty1234'
        mysite.save()
        assert mysite.get_verify_admitad() == 'qwerty1234'

    def test_mysite_get_http_domain(self, mixer):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend('market.MySite', site=site)
        assert mysite.get_http_domain() == 'http://test_test.test'

    def test_mysite_get_absolute_url(self, mixer):
        mysite = mixer.blend('market.MySite')
        assert mysite.get_absolute_url() == reverse('mysite', args=[mysite.pk, ])

    def test_mysite_get_admitad_website_none(self, mixer, monkeypatch):
        mysite = mixer.blend('market.MySite')

        def my_f(*args, **kwargs):
            return None

        monkeypatch.setattr('my_admitad_api.managers.search_adm_website', my_f)
        assert mysite.get_admitad_website() is None

    def test_deeplink(self, mixer):
        mysite = mixer.blend('market.MySite')
        seller = mixer.blend('market.Seller')
        dl = Deeplink.objects.create(site=mysite,
                                     seller=seller,
                                     url='http://test.test')
        assert str(dl) == 'http://test.test'

    def test_category_str(self, mixer):
        feed = mixer.blend('market.Feed')
        cat = mixer.blend('market.Category', feed=feed, name='test')
        assert str(cat) == 'test'

    def test_category_get_seller(self, mixer):
        feed = mixer.blend('market.Feed')
        cat = mixer.blend('market.Category', feed=feed)
        assert cat.get_seller() == feed.seller.name

    def test_category_get_category_image_not_lots(self, mixer):
        cat = mixer.blend('market.Category', )
        assert cat.get_category_image() is None

    def test_category_get_category_image(self, mixer):
        cat = mixer.blend('market.Category', )
        lot = mixer.blend('market.Lot', category=cat)
        image = mixer.blend('market.Image', lot=lot, is_main=True)
        assert cat.get_category_image() == image

    def test_category_get_category_image_parent_cat(self, mixer):
        cat = mixer.blend('market.Category', )
        cat_parent = mixer.blend('market.Category', parent=cat)
        lot = mixer.blend('market.Lot', category=cat_parent)
        image = mixer.blend('market.Image', lot=lot, is_main=True)
        assert cat.get_category_image() == image

    def test_category_web_slug(self, mixer):
        cat = mixer.blend('market.Category', name='Правила Игры')
        assert cat.web_slug == 'pravila-igry'

    def test_category_get_absolute_url_slug(self, mixer):
        cat = mixer.blend('market.Category', name='Правила Игры')
        assert cat.get_absolute_url() == reverse('slug-category-view',
                                                 kwargs={'slug': cat.web_slug, 'pk': cat.pk})

    def test_category_get_absolute_url_not_slug(self, mixer):
        cat = mixer.blend('market.Category', name='')
        assert cat.get_absolute_url() == reverse('category-view', args=(cat.id,))

    def test_param_str(self, mixer):
        param = mixer.blend('market.Param', name='test')
        assert str(param) == 'test'

    def test_image_str_downloaded_from(self, mixer):
        # Картинка не была закачена!!!
        image = mixer.blend('market.Image', image=None,
                            downloaded_from='http://test.test')
        assert str(image) == image.downloaded_from

    def test_lot_up_enabled(self, mixer):
        lot = mixer.blend('market.Lot', status=ENABLED)
        lot.up()
        assert lot.status == ENABLED

    def test_lot_up_draft(self, mixer):
        lot = mixer.blend('market.Lot', status=DRAFT)
        lot.up()
        assert lot.status == ENABLED

    def test_lot_up_disable(self, mixer):
        lot = mixer.blend('market.Lot', status=DISABLED)
        lot.up()
        assert lot.status == ENABLED

    def test_lot_up_delete(self, mixer):
        lot = mixer.blend('market.Lot', status=DELETE)
        lot.up()
        assert lot.status == DELETE

    def test_lot_down_disabled(self, mixer):
        lot = mixer.blend('market.Lot', status=DISABLED)
        lot.down()
        assert lot.status == DISABLED

    def test_lot_down_delete(self, mixer):
        lot = mixer.blend('market.Lot', status=DELETE)
        lot.down()
        assert lot.status == DELETE

    def test_lot_down_enabled(self, mixer):
        lot = mixer.blend('market.Lot', status=ENABLED)
        lot.down()
        assert lot.status == DISABLED

    def test_lot_down_draft(self, mixer):
        lot = mixer.blend('market.Lot', status=DRAFT)
        lot.down()
        assert lot.status == DISABLED

    def test_lot_delete(self, mixer):
        lot = mixer.blend('market.Lot', status=ENABLED)
        lot.lot_delete()
        assert lot.status == DELETE

    def test_get_absolute_url_not_slug_lot(self, mixer):
        lot = mixer.blend('market.Lot', status=2)
        assert lot.get_absolute_url() == reverse('lot_view', args=[lot.pk, ])

        # @unittest.skipUnless(run_httpbin_server(), 'Пропускаем тесты, httpbin сервер не запущен')
        # def test_image_fetch_downloaded_not_save_file(self):
        #     # Проверяем скачку картинки, но саму картинку не сохраняем
        #     image_url = '%s/image/jpeg' % settings.HTTPBIN_SERVER
        #     lot = mixer.blend('market.Lot')
        #     image = 'market.Image'.fetch(url=image_url, lot=lot, is_main=True)
        #     self.assertEqual(image_url, image.downloaded_from)
        #     self.assertIsNone(image.image.name)
        #     self.assertTrue(image.is_main)
        #     self.assertEquals(image.get_image(), image.downloaded_from)
        #
        # @unittest.skipUnless(run_httpbin_server(), 'Пропускаем тесты, httpbin сервер не запущен')
        # def test_image_fetch_downloaded_not_save_file_not_main(self):
        #     # Проверяем скачку картинки, но саму картинку не сохраняем
        #     image_url = '%s/image/jpeg' % settings.HTTPBIN_SERVER
        #     lot = mixer.blend('market.Lot')
        #     image = 'market.Image'.fetch(url=image_url, lot=lot)
        #     self.assertFalse(image.is_main)
        #
        # @unittest.skipUnless(run_httpbin_server(), 'Пропускаем тесты, httpbin сервер не запущен')
        # def test_image_fetch_downloaded_save_file(self):
        #     # Проверяем скачку картинки, картинку Cохраняем
        #     image_url = '%s/image/jpeg' % settings.HTTPBIN_SERVER
        #     lot = mixer.blend('market.Lot', seller__load_image=True)
        #     image = 'market.Image'.fetch(url=image_url, lot=lot, is_main=True)
        #     self.assertEqual(image_url, image.downloaded_from)
        #     self.assertNotEquals(image.get_image(), image.downloaded_from)
        #     self.assertIsNotNone(image.image.name)
        #     self.assertTrue(image.is_main)
