# -*- coding: utf-8 -*-
import datetime

import pytest
import pytz
from django.contrib.auth.models import User

# from market.utils import get_resolver_query_txt
from market.models import MySite


@pytest.fixture
def user():
    # MQ-7bc6023d578fc33d7e6a
    return User.objects.create_user(username='Test',
                                    email='test@test.ru',
                                    password='passord1',
                                    id=1,
                                    date_joined=pytz.utc.localize(datetime.datetime(2015, 10, 21, 23, 29)))


@pytest.mark.parametrize('v, success', [
    (['"MQ-7bc6023d578fc33d7e6a"'], True),
    ([], False),
])
def test_validate_site(monkeypatch, my_site, user, success, v):
    def my_f(*args, **kwargs):
        return v
        # return ['"MQ-7bc6023d578fc33d7e6a"', ]

    from market import tasks
    monkeypatch.setattr(tasks, 'get_resolver_query_txt', my_f)
    tasks.validate_site(my_site.site.id)
    if success:
        assert MySite.objects.get(site=my_site.site).user == user
    else:
        assert MySite.objects.get(site=my_site.site).user is None
