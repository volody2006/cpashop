from django.contrib import admin

from webmaster_verification.models import WebmasterVerificationCode


class WebmasterVerificationCodeAdmin(admin.ModelAdmin):
    list_display = ('domain', 'user')
    # list_filter = ['run_auto',]
    raw_id_fields = ['user']
    search_fields = ['domain', ]


admin.site.register(WebmasterVerificationCode, WebmasterVerificationCodeAdmin)
