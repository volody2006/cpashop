# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.contrib.sites.models import _simple_domain_name_validator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class WebmasterVerificationCode(models.Model):
    domain = models.CharField(_('domain name'),
                              max_length=100,
                              validators=[_simple_domain_name_validator], )
    google = models.CharField(max_length=16, blank=True)
    yandex = models.CharField(max_length=16, blank=True)
    bing = models.CharField(max_length=32, blank=True)
    majestic = models.CharField(max_length=32, blank=True)
    alexa = models.CharField(max_length=27, blank=True)
    # admitad = models.CharField(max_length=10, blank=True)
    user = models.ForeignKey(User,
                             null=True,
                             blank=True,
                             related_name='webmaster_verification_user')

    def __str__(self):
        return self.domain
