# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from market.models import MySite, Seller
from my_admitad_api.managers import AdmitadApi
from my_admitad_api.models import Website

mysite = MySite.objects.get(pk=104)
mysite.seller_set.clear()
for seller in Seller.objects.filter(site__site_id=104):
    print(seller)
    seller.sites.remove(seller.sites)

user = User.objects.get(pk=63)
mysite = MySite.objects.get(pk=9)

adm = AdmitadApi(user=user, mysite=mysite)
adm.update_websites()
adm.create_website_adm()
adm.verify_website()
adm.add_program_site(1317)
adm.add_program_site(9896)

adm = AdmitadApi()
adm.referrals()

mysite.adm_website = Website.objects.get(id=755055)
mysite.save()
MySite.objects.update_or_create(site=Site.objects.get(id=116))

from django.contrib.auth.models import User

user = User.objects.first()
from my_admitad_api.managers import AdmitadApi
from market.models import MySite, Seller

mysite = MySite.objects.get(pk=4)
adm = AdmitadApi(user=user, mysite=mysite)
adm.campaigns_for_websites()

adm = AdmitadApi(client_id=mysite.user.userprofile.admitad_identifier,
                 client_secret=mysite.user.userprofile.admitad_secret_key,
                 user=mysite.user,
                 mysite=mysite)

MySite.objects.exclude(user=None, manual_verification=True,
                       user__userprofile__admitad_identifier=None,
                       user__userprofile__admitad_secret_key=None)


def task_adm_validate_site():
    mysites = MySite.objects.filter(user__isnull=False)

    for mysite in MySite.objects.exclude(user=None, manual_verification=True,
                                         user__userprofile__admitad_identifier=None,
                                         user__userprofile__admitad_secret_key=None):
        print(mysite.site_id)
        adm_validate_site(mysite.site_id)


a = {'status': 'active', 'account_id': '', 'kind': 'website', 'is_old': False, 'name': 'shop.lens-club.ru',
     'collecting_method': None, 'verification_code': '34627b5e9f', 'mailing_targeting': False,
     'site_url': 'https://shop.lens-club.ru/', 'regions': [{'region': 'RU', 'id': 1900549}], 'db_size': None,
     'adservice': None, 'id': 755055, 'creation_date': '2017-11-22T12:06:12', 'validation_passed': True,
     'categories': [{'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'}],
     'description': 'Данный сайт зарегистрирован в онлайн-сервисе mycpashop.ru. Планируется привлечение трафика с поисковых систем.'}

b = {'goto_cookie_lifetime': 20, 'rating': '5.7', 'exclusive': False,
     'image': 'http://cdn.admitad.com/campaign/images/2014/08/27/f61cb3a97328db2a72d74d75b0e3b73b.jpg',
     'actions': [{'hold_time': 0, 'payment_size': '0.50%-6%', 'type': 'sale', 'name': 'Оплаченный  заказ', 'id': 4865}],
     'avg_money_transfer_time': 15, 'currency': 'RUB', 'activation_date': '2014-08-29T14:43:57', 'cr': 1.47,
     'max_hold_time': None, 'id': 9896, 'avg_hold_time': 15, 'ecpc': 6.82, 'connection_status': 'declined',
     'gotolink': '', 'site_url': 'http://mediamarkt.ru/', 'regions': [{'region': 'RU'}], 'actions_detail': [
        {'type': 'sale', 'tariffs': [{'action_id': 4865, 'rates': [
            {'price_s': '0.00', 'tariff_id': 9232, 'country': None, 'date_s': '2017-12-01', 'is_percentage': True,
             'id': 109740, 'size': '0.50'}], 'id': 9232, 'name': 'Товары бренда Bork'}, {'action_id': 4865, 'rates': [
            {'price_s': '0.00', 'tariff_id': 5232, 'country': None, 'date_s': '2017-02-18', 'is_percentage': True,
             'id': 62594, 'size': '3.00'}], 'id': 5232, 'name': 'Все категории'}, {'action_id': 4865, 'rates': [
            {'price_s': '0.00', 'tariff_id': 14128, 'country': None, 'date_s': '2017-12-18', 'is_percentage': True,
             'id': 111198, 'size': '6.00'}], 'id': 14128, 'name': 'Товары брендов LG+Bosch'}, {'action_id': 4865,
                                                                                               'rates': [
                                                                                                   {'price_s': '0.00',
                                                                                                    'tariff_id': 9230,
                                                                                                    'country': None,
                                                                                                    'date_s': '2017-07-07',
                                                                                                    'is_percentage': True,
                                                                                                    'id': 80279,
                                                                                                    'size': '1.00'}],
                                                                                               'id': 9230,
                                                                                               'name': 'Товары бренда Apple'},
                                     {'action_id': 4865, 'rates': [
                                         {'price_s': '0.00', 'tariff_id': 9231, 'country': None, 'date_s': '2017-11-01',
                                          'is_percentage': True, 'id': 104151, 'size': '5.00'}], 'id': 9231,
                                      'name': 'Товары бренда Philips'}], 'hold_size': 0, 'name': 'Оплаченный  заказ',
         'id': 4865}], 'geotargeting': False,
     'products_xml_link': 'http://export.admitad.com/ru/webmaster/websites/755055/products/export_adv_products/?user=ishendel&code=7ntypu8pgw&feed_id=9896&format=xml',
     'status': 'active', 'coupon_iframe_denied': False, 'traffics': [{'enabled': True, 'name': 'Cashback', 'id': 1},
                                                                     {'enabled': False, 'name': 'PopUp / ClickUnder',
                                                                      'id': 2},
                                                                     {'enabled': True, 'name': 'Контекстная реклама',
                                                                      'id': 3},
                                                                     {'enabled': False, 'name': 'Дорвей - трафик',
                                                                      'id': 4},
                                                                     {'enabled': True, 'name': 'Email - рассылка',
                                                                      'id': 5}, {'enabled': False,
                                                                                 'name': 'Контекстная реклама на Бренд',
                                                                                 'id': 6}, {'enabled': True,
                                                                                            'name': 'Реклама в социальных сетях',
                                                                                            'id': 7},
                                                                     {'enabled': False, 'name': 'Мотивированный трафик',
                                                                      'id': 8},
                                                                     {'enabled': False, 'name': 'Toolbar', 'id': 9},
                                                                     {'enabled': False, 'name': 'Adult - трафик',
                                                                      'id': 14}, {'enabled': True,
                                                                                  'name': 'Мобильный трафик для мобильной версии',
                                                                                  'id': 16},
                                                                     {'enabled': True, 'name': 'Тизерные сети',
                                                                      'id': 18},
                                                                     {'enabled': True, 'name': 'Youtube Канал',
                                                                      'id': 19}],
     'description': 'Невероятно! Новые выгодные условия от MediaMarkt!\r\n\r\n\r\n\r\n*Веб-мастера, нарушающие правила работы, не вознаграждаются бонусами.\r\n\r\nС 19 апреля 2017г. &nbsp;года вводится запрет на купонный трафик!\r\n\r\nC&nbsp;28.06.2017 г. введен запрет на e-mail Ремаркетинг/ретаргетинг!\r\n\r\nДополнительные вопросы вы можете задать на официальном форуме программы.\r\n\r\n&nbsp;\r\n\r\nПрисоединяйтесь к партнерской программе MediaMarkt &ndash; это выгодно и перспективно!\r\n&nbsp;\r\n\r\n1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Преимущества&nbsp;MediaMarkt&nbsp;для покупателей:\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Магазин электроники с мировым именем вызывающий доверие.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Простая политика возврата товара, надёжная гарантия.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Бонусная программа для покупателей (оплачивая бонусами заказы, сделанные на сайте, веб-мастера получают своё вознаграждение полностью)\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Регулярные и выгодные акции\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Более 200&nbsp;000 тысяч наименований товаров, по низким или не уступающим конкурентам ценам!\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Бесплатный самовывоз из магазинов Media Markt&nbsp; по всей России.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Возможность покупки в кредит.\r\n&nbsp;\r\n\r\n2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Преимущества&nbsp;MediaMarkt&nbsp;для веб-мастеров:\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Отличная процентная ставка в категории Цифровая &amp; Бытовая техника!\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Самые большие выплаты за оплаченный заказ. (Средний чек 15&nbsp;000 рублей) Возможность заработать с одной продажи до 20&nbsp;000 рублей!\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Бонусная программа и конкурсы для веб-мастеров!\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ГЕО-Вся Россия, 67 городов 30 магазинов. Узнаваемый бренд.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Скидки, акции и купоны всегда в системе.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Быстрая обратная связь через форум, баннеры на заказ.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Высокая конверсия заказов. Высокий средний чек.\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ваш оффер всегда будет учтён, даже если клиент купит в кредит!\r\n\r\n&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Отсутствует перезапись кук с прямого и поискового трафика, а также с рассылок: ваше вознаграждение не &laquo;потеряется&raquo;\r\n&nbsp;\r\n\r\n3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MediaMarkt&nbsp;входит в топ-10 интернет магазинов России. Покупатели выбирают нас за качественный сервис, лояльную политику возврата, отличные цены и удобство покупки, это обеспечивает нам\r\n\r\nстабильные продажи, а веб-мастерам хорошую конверсию с оффера.&nbsp;\r\n\r\n\r\nМы создали один из лучших офферов на рынке, у нас стабильные выплаты и прозрачная система ставок, присоединяйтесь к одному из мировых лидеров по продажам электроники!&nbsp;',
     'cr_trend': '0.0400',
     'raw_description': '<p style="text-align:center"><span style="font-size:16px"><strong>Невероятно! Новые выгодные условия от MediaMarkt!</strong></span></p>\r\n\r\n<p style="text-align:center"><img alt="" src="https://hq.admitad.com/public/storage/2017/01/09/image001_1.jpg" /></p>\r\n\r\n<p style="text-align:center">*Веб-мастера, нарушающие правила работы, не вознаграждаются бонусами.</p>\r\n\r\n<p style="text-align:center"><span style="color:#FF0000"><strong>С 19 апреля 2017г. &nbsp;года вводится запрет на купонный трафик!<br />\r\n<br />\r\nC&nbsp;28.06.2017 г. введен запрет на e-mail Ремаркетинг/ретаргетинг!</strong></span></p>\r\n\r\n<p style="text-align:center">Дополнительные вопросы вы можете задать на <a href="http://forum.admitad.com/index.php?/topic/5671-%D0%BF%D0%B0%D1%80%D1%82%D0%BD%D1%91%D1%80%D1%81%D0%BA%D0%B0%D1%8F-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0-media-markt/" target="_blank">официальном форуме</a> программы.</p>\r\n\r\n<p style="text-align:center">&nbsp;</p>\r\n\r\n<p><span style="font-size:16px"><em><strong>Присоединяйтесь к партнерской программе MediaMarkt &ndash; это выгодно и перспективно!</strong></em></span><br />\r\n&nbsp;</p>\r\n\r\n<p><strong>1.<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong><strong>Преимущества&nbsp;</strong><strong>Media</strong><strong>Markt&nbsp;для покупателей:</strong></p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Магазин электроники с мировым именем вызывающий доверие.</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Простая политика возврата товара, надёжная гарантия.</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Бонусная программа для покупателей (оплачивая бонусами заказы, сделанные на сайте, веб-мастера получают своё вознаграждение полностью)</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Регулярные и выгодные акции</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Более 200&nbsp;000 тысяч наименований товаров, по низким или не уступающим конкурентам ценам!</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Бесплатный самовывоз из магазинов Media Markt&nbsp; по всей России.</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Возможность покупки в кредит.<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>2.<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong><strong>Преимущества&nbsp;</strong><strong>Media</strong><strong>Markt&nbsp;для веб-мастеров:</strong></p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Отличная процентная ставка в категории Цифровая &amp; Бытовая техника!</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Самые большие выплаты за оплаченный заказ. (Средний чек 15&nbsp;000 рублей) Возможность заработать с одной продажи до 20&nbsp;000 рублей!</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Бонусная программа и конкурсы для веб-мастеров!</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>ГЕО-Вся Россия, 67 городов 30 магазинов. Узнаваемый бренд.</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Скидки, акции и купоны всегда в системе.</p>\r\n\r\n<p><span style="color:black; font-size:9pt">&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>Быстрая обратная связь через форум, баннеры на заказ.</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Высокая конверсия заказов. Высокий средний чек.</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Ваш оффер всегда будет учтён, даже если клиент купит в кредит!</p>\r\n\r\n<p>&middot;<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Отсутствует перезапись кук с прямого и поискового трафика, а также с рассылок: ваше вознаграждение не &laquo;потеряется&raquo;<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>3.<span style="font-size:7pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong><strong>Media</strong><strong>Markt&nbsp;</strong>входит в топ-10 интернет магазинов России. Покупатели выбирают нас за качественный сервис, лояльную политику возврата, отличные цены и удобство покупки, это обеспечивает нам</p>\r\n\r\n<p>стабильные продажи, а веб-мастерам хорошую конверсию с оффера.<strong>&nbsp;</strong></p>\r\n\r\n<p><br />\r\n<span style="color:#333333"><strong>Мы создали один из лучших офферов на рынке, у нас стабильные выплаты и прозрачная система ставок, присоединяйтесь к одному из мировых лидеров по продажам электроники!&nbsp;</strong></span></p>\r\n',
     'modified_date': '2017-11-22T09:28:09', 'denynewwms': False, 'moderation': True,
     'categories': [{'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'},
                    {'language': 'ru', 'id': 65,
                     'parent': {'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'},
                     'name': 'Цифровая & Бытовая техника'}, {'language': 'ru', 'id': 92,
                                                             'parent': {'language': 'ru', 'id': 62, 'parent': None,
                                                                        'name': 'Интернет-магазины'},
                                                             'name': 'Автотовары'}, {'language': 'ru', 'id': 95,
                                                                                     'parent': {'language': 'ru',
                                                                                                'id': 62,
                                                                                                'parent': None,
                                                                                                'name': 'Интернет-магазины'},
                                                                                     'name': 'Софт & Игры'},
                    {'language': 'ru', 'id': 100,
                     'parent': {'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'},
                     'name': 'Музыка & Звук'}], 'name': 'Media Markt', 'retag': True,
     'products_csv_link': 'http://export.admitad.com/ru/webmaster/websites/755055/products/export_adv_products/?user=ishendel&code=7ntypu8pgw&feed_id=9896&format=csv',
     'feeds_info': [{'advertiser_last_update': '2017-12-20 01:21:00', 'admitad_last_update': '2017-12-20 06:28:48',
                     'csv_link': 'http://export.admitad.com/ru/webmaster/websites/755055/products/export_adv_products/?user=ishendel&code=7ntypu8pgw&feed_id=9896&format=csv',
                     'name': 'Media Markt',
                     'xml_link': 'http://export.admitad.com/ru/webmaster/websites/755055/products/export_adv_products/?user=ishendel&code=7ntypu8pgw&feed_id=9896&format=xml'}],
     'landing_code': None, 'ecpc_trend': '0.2500', 'landing_title': None, 'allow_deeplink': True,
     'show_products_links': True}

c = {'goto_cookie_lifetime': 15, 'rating': '3.5', 'exclusive': False,
     'image': 'http://cdn.admitad.com/campaign/images/2013/06/07/6661b9774b3949e82ffecad149f0ad78.jpg',
     'actions': [{'hold_time': 0, 'payment_size': '1%-5%', 'type': 'sale', 'name': 'Оплаченный  заказ', 'id': 5442}],
     'avg_money_transfer_time': 17, 'currency': 'RUB', 'activation_date': '2013-09-10T15:20:54', 'retag': True,
     'cr': 0.63, 'max_hold_time': None, 'id': 3231, 'avg_hold_time': 16, 'ecpc': 2.37, 'connection_status': 'active',
     'gotolink': 'https://ad.admitad.com/g/cf1399ae9605ab174a71dd61df5557/', 'site_url': 'http://003.ru/',
     'regions': [{'region': 'RU'}], 'actions_detail': [{'type': 'sale', 'tariffs': [{'action_id': 5442, 'rates': [
        {'price_s': '0.00', 'tariff_id': 6186, 'country': None, 'date_s': '2015-12-23', 'is_percentage': True,
         'id': 34171, 'size': '5.00'}], 'id': 6186, 'name': 'Все категории'}, {'action_id': 5442, 'rates': [
        {'price_s': '0.00', 'tariff_id': 6187, 'country': None, 'date_s': '2017-07-07', 'is_percentage': True,
         'id': 80280, 'size': '1.00'}], 'id': 6187, 'name': 'Товары бренда Apple'}], 'hold_size': 0,
                                                        'name': 'Оплаченный  заказ', 'id': 5442}],
     'geotargeting': False, 'status': 'active', 'coupon_iframe_denied': False,
     'traffics': [{'enabled': True, 'name': 'Cashback', 'id': 1},
                  {'enabled': True, 'name': 'PopUp / ClickUnder', 'id': 2},
                  {'enabled': True, 'name': 'Контекстная реклама', 'id': 3},
                  {'enabled': False, 'name': 'Дорвей - трафик', 'id': 4},
                  {'enabled': True, 'name': 'Email - рассылка', 'id': 5},
                  {'enabled': False, 'name': 'Контекстная реклама на Бренд', 'id': 6},
                  {'enabled': True, 'name': 'Реклама в социальных сетях', 'id': 7},
                  {'enabled': False, 'name': 'Мотивированный трафик', 'id': 8},
                  {'enabled': False, 'name': 'Toolbar', 'id': 9},
                  {'enabled': False, 'name': 'Adult - трафик', 'id': 14},
                  {'enabled': True, 'name': 'Мобильный трафик для мобильной версии', 'id': 16},
                  {'enabled': True, 'name': 'Тизерные сети', 'id': 18},
                  {'enabled': True, 'name': 'Youtube Канал', 'id': 19}],
     'description': 'Интересные факты о интернет-магазине 003.ru!\r\n\r\n&bull; Интернет-магазину 003.ru более 15 лет.\r\n&bull; 11 место в рейтинге ТОП-100 КРУПНЕЙШИХ интернет-магазинов в России, в отрасли электроника и техника.\r\n&bull; Ежедневно более 600 покупателей выбирают наш магазин за доступные цены и высокий уровень сервиса.\r\n&bull; За месяц сайт 003.ru посещает более 1 500 000 уникальных посетителей, из которых приблизительно 50% - это жители Москвы и Подмосковья.\r\n&bull; Быстрая и удобная доставка по всей России (3727 городов)\r\n&bull; Более 30 000 товаров.\r\n&bull; Все товары в наличии на складе, услуги по установке и настройки техники.&nbsp;\r\n\r\nПреимущества &nbsp;работы с партнерской программой 003.ru:\r\n\r\n&bull; Самая высокая ставка в сегменте, МБТ, КБТ 5%! (Больше чем у конкурентов)\r\n&bull; Средний чек 12 132 рублей. (Выше чем у конкурентов)\r\n&bull; Бонусная программа, мы предлагаем деньги, а не проценты.&nbsp;*Веб-мастера, нарушающие правила работы, не вознаграждаются бонусами.\r\n&bull; Быстрый заказ без регистрации (Нет у конкурентов)\r\n&bull; Все заказы легко оплатить online.\r\n&bull; Высокая конверсия заказов за счёт: наличия товара, низких цен, акций, скидок и подарков для покупателей. (Зайдите на сайт убедитесь сами)\r\n&bull; Поддержка веб-мастеров на форуме.\r\n&bull; Предоставление промо-материалов, купонов, специальных условий по запросу.&nbsp;\r\n&bull; Широкий выбор разрешенных источников трафика.&nbsp;\r\n&bull; ГЕО &ndash; вся Россия.\r\n&bull; c 1 мая 2017 г. ставка для купонных&nbsp;сайтов = 2.5%.\r\n&bull; C&nbsp;28.06.2017 г. введен запрет на e-mail Ремаркетинг/ретаргетинг!\r\n\r\nТОП-товары данного интернет-магазина.',
     'cr_trend': '-0.0100',
     'raw_description': '<p style="text-align:center"><img alt="" src="https://www.admitad.com/public/storage/2015/12/30/1003_680x300.jpg" /></p>\r\n\r\n<p style="text-align:center"><strong><img alt="" src="https://hq.admitad.com/public/storage/2015/11/27/003_2.jpg" /></strong></p>\r\n\r\n<p><strong>Интересные факты о интернет-магазине 003.ru!</strong></p>\r\n\r\n<p><span style="font-size:14px"><span style="color:#000000"><strong>&bull; Интернет-магазину 003.ru более 15 лет.<br />\r\n&bull; 11 место в рейтинге ТОП-100 КРУПНЕЙШИХ интернет-магазинов в России, в отрасли электроника и техника.<br />\r\n&bull; Ежедневно более 600 покупателей выбирают наш магазин за доступные цены и высокий уровень сервиса.<br />\r\n&bull; За месяц сайт 003.ru посещает более 1 500 000 уникальных посетителей, из которых приблизительно 50% - это жители Москвы и Подмосковья.<br />\r\n&bull; Быстрая и удобная доставка по всей России (3727 городов)<br />\r\n&bull; Более 30 000 товаров.<br />\r\n&bull; Все товары в наличии на складе, услуги по установке и настройки техники.&nbsp;</strong></span></span></p>\r\n\r\n<p><span style="color:rgb(0, 128, 128)"><span style="font-size:16px"><strong>Преимущества &nbsp;работы с партнерской программой 003.ru:</strong></span></span><br />\r\n<br />\r\n<span style="color:#000000"><span style="font-size:14px"><strong>&bull; Самая высокая ставка в сегменте, МБТ, КБТ 5%! (Больше чем у конкурентов)<br />\r\n&bull; Средний чек 12 132 рублей. (Выше чем у конкурентов)<br />\r\n&bull; Бонусная программа, мы предлагаем деньги, а не проценты.&nbsp;*Веб-мастера, нарушающие правила работы, не вознаграждаются бонусами.<br />\r\n&bull; Быстрый заказ без регистрации (Нет у конкурентов)<br />\r\n&bull; Все заказы легко оплатить online.<br />\r\n&bull; Высокая конверсия заказов за счёт: наличия товара, низких цен, акций, скидок и подарков для покупателей. (Зайдите на сайт убедитесь сами)<br />\r\n&bull; Поддержка веб-мастеров на форуме.<br />\r\n&bull; Предоставление промо-материалов, купонов, специальных условий по запросу.&nbsp;<br />\r\n&bull; Широкий выбор разрешенных источников трафика.&nbsp;</strong></span></span><br />\r\n<strong><span style="font-size:14px">&bull; ГЕО &ndash; вся Россия.<br />\r\n<span style="color:#FF0000">&bull; c 1 мая 2017 г. ставка для купонных&nbsp;сайтов = 2.5%.<br />\r\n&bull; C&nbsp;28.06.2017 г. введен запрет на e-mail Ремаркетинг/ретаргетинг!</span></span></strong></p>\r\n\r\n<p><a href="https://www.admitad.com/public/storage/2015/05/06/003_Top_produktov.xlsx">ТОП-товары данного интернет-магазина.</a></p>\r\n',
     'modified_date': '2017-12-21T11:15:41', 'denynewwms': False, 'moderation': True,
     'categories': [{'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'},
                    {'language': 'ru', 'id': 65,
                     'parent': {'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'},
                     'name': 'Цифровая & Бытовая техника'}, {'language': 'ru', 'id': 92,
                                                             'parent': {'language': 'ru', 'id': 62, 'parent': None,
                                                                        'name': 'Интернет-магазины'},
                                                             'name': 'Автотовары'}, {'language': 'ru', 'id': 95,
                                                                                     'parent': {'language': 'ru',
                                                                                                'id': 62,
                                                                                                'parent': None,
                                                                                                'name': 'Интернет-магазины'},
                                                                                     'name': 'Софт & Игры'},
                    {'language': 'ru', 'id': 100,
                     'parent': {'language': 'ru', 'id': 62, 'parent': None, 'name': 'Интернет-магазины'},
                     'name': 'Музыка & Звук'}, {'language': 'ru', 'id': 102,
                                                'parent': {'language': 'ru', 'id': 62, 'parent': None,
                                                           'name': 'Интернет-магазины'},
                                                'name': 'Инструменты & Садовая техника'}],
     'products_csv_link': 'http://export.admitad.com/ru/webmaster/websites/180756/products/export_adv_products/?user=svsmarketvgorode&code=97093f4173&feed_id=15579&format=csv',
     'products_xml_link': 'http://export.admitad.com/ru/webmaster/websites/180756/products/export_adv_products/?user=svsmarketvgorode&code=97093f4173&feed_id=15579&format=xml',
     'name': '003', 'feeds_info': [
        {'advertiser_last_update': '2017-12-23 12:09:00', 'admitad_last_update': '2017-12-23 16:20:26',
         'csv_link': 'http://export.admitad.com/ru/webmaster/websites/180756/products/export_adv_products/?user=svsmarketvgorode&code=97093f4173&feed_id=15579&format=csv',
         'name': 'Основной',
         'xml_link': 'http://export.admitad.com/ru/webmaster/websites/180756/products/export_adv_products/?user=svsmarketvgorode&code=97093f4173&feed_id=15579&format=xml'}],
     'landing_code': None, 'ecpc_trend': '-0.0200', 'landing_title': None, 'allow_deeplink': True,
     'show_products_links': True}

from newsletter.models import Submission


class Job(HourlyJob):
    help = "Submit pending messages."

    def execute(self):
        logger.info(_('Submitting queued newsletter mailings'))
        Submission.submit_queue()