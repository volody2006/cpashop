# -*- coding: utf-8 -*-


import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cpashop.settings')

app = Celery('cpashop')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

task_routes = {
    'import_lots.tasks.images_load': {'queue': 'images'},
    'import_lots.tasks.down_lot': {'queue': 'images'},
}

app.conf.task_routes = task_routes


@app.task(bind=True)
def debug_task(self):
    print(('Request: {0!r}'.format(self.request)))
