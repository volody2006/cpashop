# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

DEBUG = True
CELERY_START = True

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

WEBMASTER_VERIFICATION = 'm,dhnfsdj'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'cpashop_test',
#         'USER': 'postgres',
#         'PASSWORD': 'postgres',
#         'HOST': 'localhost',
#         'PORT': '5432',
#     },
# }

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'search.elasticsearch2_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'cpashop_test',
    },
}

CELERY_HAYSTACK_QUEUE = 'celery_haystack_test'

# CELERY_ROUTES = {
#     'import_lots.tasks.images_load': {'queue': 'images'},
#     'import_lots.tasks.down_lot': {'queue': 'images'},
#     # 'celery_haystack_index': {},
# }

CELERY_RESULT_BACKEND = 'django-cache'
