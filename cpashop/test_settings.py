# -*- coding: utf-8 -*-
import logging
import sys

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = 'CHANGEME!!!'

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# In-memory email backend stores messages in django.core.mail.outbox
# for unit testing purposes
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

# CACHING
# ------------------------------------------------------------------------------
# Speed advantages of in-memory caching without having to run Memcached
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# HAYSTACK
# ------------------------------------------------------------------------------
# Выбираем тестовый индекс и сигналы в режиме реального времени
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'search.elasticsearch2_backend.ElasticsearchSearchEngine',
        'URL': 'http://localhost:9200/',
        'INDEX_NAME': 'test_cpashop',
    },
}
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# PASSWORD HASHING
# ------------------------------------------------------------------------------
# Use fast password hasher so tests run faster
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

# TEMPLATE LOADERS
# ------------------------------------------------------------------------------
# Keep templates in memory so tests run faster
# TEMPLATES[0]['OPTIONS']['loaders'] = [
#     ['django.template.loaders.cached.Loader', [
#         'django.template.loaders.filesystem.Loader',
#         'django.template.loaders.app_directories.Loader',
#     ], ],
# ]


WEBMASTER_VERIFICATION = {}

HTTPBIN_SERVER = 'http://localhost:8008'


# HTTPBIN_SERVER = 'http://httpbin:8008'


class DisableMigrations:
    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return None


if 'test' in sys.argv[1:] or 'jenkins' in sys.argv[1:]:
    logging.disable(logging.CRITICAL)
    TESTS_IN_PROGRESS = True
    MIGRATION_MODULES = DisableMigrations()
    CELERY_START = False
    DATABASE_URL = 'sqlite: //:memory'
