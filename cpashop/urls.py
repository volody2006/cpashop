# -*- coding: utf-8 -*-
"""cpashop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from market.models import Lot, ENABLED
from my_sitemaps.sitemaps import GenericSitemap

# handler404 = 'market.views.page_not_found'

sitemap_lot = {
    'queryset': Lot.objects.filter(status=ENABLED),
    'date_field': 'date_changed'
}
# sitemap_category = {
#     'queryset': Category.objects.filter(lot__status=ENABLED),
#     'date_field': 'date_changed',
# }

sitemaps = {
    # 'flatpages': FlatPageSitemap,
    'lot': GenericSitemap(sitemap_lot, priority=0.5, changefreq='daily'),
    # 'sitemap_category': GenericSitemap(sitemap_category, priority=0.2, changefreq = 'monthly'),
}

urlpatterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^crm/', include('crm.urls')),
    # url(r'^', include('webmaster_verification.urls')),
    url(r'^', include('custom_profile.urls')),

    url(r'^robots\.txt', include('robots.urls')),
    url(r'^', include('favicon.urls')),
    url(r'^', include('sitemaps.urls')),

    url(r'', include('goto_url.urls')),
    url(r'', include('verify_cpa.urls')),

    url(r'^', include('market.urls')),
    url(r'^search/', include('search.urls')),

    url(r'^pages/', include('django.contrib.flatpages.urls')),

    url(r'^newsletter/', include('newsletter.urls')),
    url(r'^tinymce/', include('tinymce.urls')),]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [url(r'^__debug__/', include(debug_toolbar.urls)),
                   ] + urlpatterns
