# -*- coding: utf-8 -*-
# from django.contrib.auth.models import User
# from django.db import models
#
# from base.models import CommonAbstractModel
#

# class Template(CommonAbstractModel):
#     name = models.CharField(max_length=255, verbose_name='Название')
#     folder = models.CharField(max_length=255, verbose_name='Название директории в папке шаблонов')
#
#     paginate_by = models.IntegerField(default=12, verbose_name='Оптимальное количество товаров на странице, '
#                                                                'зависит от шаблона')
#     number_of_categories = models.IntegerField(default=4, verbose_name='Количество пунктов меню не считая Главного')
#     number_of_subcategories = models.IntegerField(default=6, verbose_name='Количество подкатегорий в меню')
#     user = models.ForeignKey(User, null=True, blank=True)
#     hidden = models.BooleanField(default=False, help_text='Шаблон скрыт')
#     crm_template = models.BooleanField(default=False, help_text='Шаблон для админки?')
