# -*- coding: utf-8 -*-
from django.db import models


class CommonAbstractModel(models.Model):
    """
    Абстрактный класс, с двумя предопределенными полями

        1. Поле даты создания
        2. Поле даты модификации

    Оба поля заполняются автоматически и через админку не редактируются.
    """
    # date_created = models.DateTimeField(default=timezone.now,
    #                                     editable=False,
    #                                     db_index=True)
    # date_changed = AutoDateTimeField(editable=False)

    date_created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    date_changed = models.DateTimeField('Last Update Date/Time', auto_now=True)

    def get_full_url(self, mysite):
        """
        Умная функция, возвращающая полный URL объекта вместе с доменом.

        Если у модели нет метода get_absolute_url(), будет
        возвращено значение None

        Если у модели нет внешнего ключа с именем department,
        будет возвращено значение get_absolute_url()

        """
        if not hasattr(self, 'get_absolute_url'):
            return None
        #
        # if not hasattr(self, 'department'):
        #     return self.get_absolute_url()

        return '{host}{path}'.format(host=mysite.http_domain,
                                     path=self.get_absolute_url())

    class Meta(object):
        abstract = True


class MyError(CommonAbstractModel):
    name_error = models.CharField(max_length=120, blank=True, null=True, help_text='Ошибка')
    text = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True, help_text='Урл ошибки')
    # HTTP_REFERER
    http_referer = models.TextField(blank=True, null=True, help_text='HTTP_REFERER')
    request_meta = models.TextField(blank=True, null=True, help_text='request.META')
    file_name = models.CharField(max_length=120, blank=True, null=True, help_text='Имя файла')
    level = models.CharField(max_length=20, blank=True, null=True, help_text='Уровень ошибки')

    def __str__(self):
        # return self.name_error.encode('utf-8')
        return self.name_error
