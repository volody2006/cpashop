# -*- coding: utf-8 -*-
from django.core.management import call_command
# from django.utils.translation import ugettext as _


def clear_search_index():
    """
    штука очищает текущий default индекс (который с лотами)
    """
    call_command('clear_index', interactive=False)


def reindex_search_queue():
    """
    синхронно отправляем накопившиеся изменения в солр (в default индекс)
    """
    call_command('rebuild_index')
