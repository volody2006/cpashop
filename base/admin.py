# -*- coding: utf-8 -*-
from django.contrib import admin

from base.models import MyError


class MyErrorAdmin(admin.ModelAdmin):
    list_display = ('name_error', 'level', 'url', 'http_referer')
    list_filter = ['level', 'file_name']


admin.site.register(MyError, MyErrorAdmin)
