#!/usr/bin/env bash

celery -A cpashop worker -l info -B
#celery -A cpashop worker --loglevel=info
#
## Если нужны очереди
#celery -A cpashop worker -Q celery_haystack,celery,images --loglevel=INFO --concurrency=4 -B
#
#
#The default port is http://localhost:5555, but you can change this using the --port argument:
#
#$ celery -A cpashop flower --port=5555
#
#$ celery -A cpashop worker --loglevel=INFO --concurrency=2 -n worker1@%h -Q celery -B
#$ celery -A cpashop worker --loglevel=INFO --concurrency=2 -n worker2@%h -Q images
#$ celery -A cpashop worker --loglevel=INFO --concurrency=2 -n worker3@%h -Q celery_haystack
#
#celery -A cpashop worker --loglevel=INFO --concurrency=2 -n worker@all -Q celery,images,celery_haystack -B
#celery -A cpashop worker --loglevel=INFO --concurrency=5 -n worker1@all -Q celery,images,celery_haystack
#celery -A cpashop beat -l info -S django
#
#celery -A cpashop worker --loglevel=INFO --concurrency=5 -n worker@all -Q celery,images,celery_haystack


#ps ww | grep 'celery' | grep -v grep | awk '{print $1}' | xargs kill
#
#django-admin.py makemessages --all --ignore=env/* --ignore=.env/* --ignore=_*
## Компиляция файлов с сообщениями
#python manage.py compilemessages
#
#ps ww | grep 'redis-server' | grep -v grep | awk '{print $1}'
#
#ps ww | grep 'gunicorn' | grep -v grep | awk '{print $1}' | xargs kill
#
#
#docker run -p 8001:8000 apsl/thumbor


#docker exec -it 62af976d980e /bin/bash
#root@user-desktop:/usr/share/elasticsearch/bin# bin/plugin install royrusso/elasticsearch-HQ
#bin/plugin install http://dl.bintray.com/content/imotov/elasticsearch-plugins/org/elasticsearch/elasticsearch-analysis-morphology/2.4.2/elasticsearch-analysis-morphology-2.4.2.zip

#./.env/bin/celery --verbosity=3 --loglevel=INFO --concurrency=4 -E



#solr
#docker run --rm --name my_solr -p 8983:8983 -t -v $HOME/docker/solr/mydata:/opt/solr/mydata solr
#docker run --rm -p 8983:8983 -t -v $HOME/docker/solr/new_core:/opt/solr/server/solr/new_core -v $HOME/docker/solr/mydata:/opt/solr/mydata solr


#/opt/solr/server/solr/new_core

#docker run -d -p 8983:8983 -v /home/user/program/MyProject/djmarket/devel/solr/karla_v3:/var/lib/solr-4.4.0/example/multicore/karla_v3/ -v /home/user/program/MyProject/djmarket/devel/solr/karla_pirate:/var/lib/solr-4.4.0/example/multicore/karla_pirate/ -t onjin/solr
#
#
#docker run --rm -p 8983:8983 -v /home/user/docker/solr/multicore/cpashop:/var/lib/solr-4.4.0/example/multicore/cpashop -t onjin/solr
#
#docker run --rm -p 8983:8983 -v /home/user/docker/solr/multicore/cpashop:/var/lib/solr-4.4.0/example/multicore/cpashop carlosamartins/solr
#
#
##celery
#python manage.py celerycam --loglevel=INFO
#python manage.py celeryd -Q celery_haystack --verbosity=2 --loglevel=INFO --concurrency=4 -E
#python manage.py celeryd -Q images --verbosity=2 --loglevel=INFO --concurrency=2 -E
#python manage.py celeryd --verbosity=2 --loglevel=INFO --concurrency=4 -B -E
#
#ps ww | grep 'celeryd' | grep -v grep | awk '{print $1}' | xargs kill
#ps ww | grep 'chromedriver' | grep -v grep | awk '{print $1}' | xargs kill