# -*- coding: utf-8 -*-
from blog.models import Entry
from django.contrib.sitemaps import Sitemap


class BlogSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Entry.objects.filter(is_draft=False)

    def lastmod(self, obj):
        return obj.pub_date
