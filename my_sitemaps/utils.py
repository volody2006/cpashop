# coding: utf-8
import os
import re

from django.conf import settings
from django.core.urlresolvers import reverse

DIRECTORY = 'sitemaps'


def get_sitemap_chapter_url(mysite, filename):
    return mysite.http_domain + reverse('sitemap_section', args=[int(re.compile('\D+').sub('', filename))])


def get_sitemap_url():
    return os.path.join(settings.MEDIA_URL, DIRECTORY)


def get_sitemap_url_for_mysite(mysite):
    return os.path.join(settings.MEDIA_URL, DIRECTORY, mysite.name)


def get_sitemap_root():
    return os.path.join(settings.MEDIA_ROOT, DIRECTORY)


def get_sitemap_directory(mysite):
    return os.path.join(get_sitemap_root(), mysite.name)


def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)
