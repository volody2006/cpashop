# -*- coding: utf-8 -*-
import dns.resolver
from django.contrib.sites.models import Site
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from haystack.backends import SQ
from haystack.query import SearchQuerySet


def not_escape(text):
    """
    Returns the given text with ampersands, quotes and angle brackets encoded
    for use in HTML.

    This function always escapes its input, even if it's already escaped and
    marked as such. This may result in double-escaping. If this is a concern,
    use conditional_escape() instead.
    """

    return mark_safe(force_text(text).replace('&amp;', '&').replace('&lt;', '<')
                     .replace('&gt;', '>').replace('&quot;', '"').replace('&#39;', "'"))


# def image_resize(file_name):
#     filter = Image.ANTIALIAS
#     image = Image.open(fp=file_name)
#
#     image = image.resize((width, height), filter)


def canonize(source):
    """
    http://www.codeisart.ru/python-shingles-algorithm/
    Функция canonize очищает текст от стоп-символов и стоп-слов,
    приводит все символы строки к нижнему регистру и возвращает список,
    оставшихся после чистки слов.
    """
    stop_symbols = '.,!?:;-\n\r()'

    stop_words = ('это', 'как', 'так',
                  'и', 'в', 'над',
                  'к', 'до', 'не',
                  'на', 'но', 'за',
                  'то', 'с', 'ли',
                  'а', 'во', 'от',
                  'со', 'для', 'о',
                  'же', 'ну', 'вы',
                  'бы', 'что', 'кто',
                  'он', 'она')

    return [x for x in [y.strip(stop_symbols) for y in source.lower().split()] if x and (x not in stop_words)]


def get_query_for_object(obj):
    """
    составляем запрос в солр из SQ объектов для заданного объекта
    странно что объект то может быть любым = а мы смело опираемся только на его имя .name
    """
    words = canonize(obj.name)
    sqs = SearchQuerySet()
    # tags = Tag.objects.get_for_object(obj)
    # if tags:
    #     words += [tag.name for tag in tags]

    name_query = SQ()
    clean_query = sqs.query.clean(words)
    for word in clean_query:
        if word:
            name_query.add(SQ(text__contains=word), SQ.OR)

    return name_query


def get_resolver_query_txt(site_id):
    '''
    Проверяем записи TXT и возвращаем списком.
    :param site_id:
    :return: список записей TXT
    '''
    site = Site.objects.get(pk=site_id)
    domain = site.domain
    var = []
    try:
        txt = dns.resolver.query(domain, 'TXT')
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer, dns.exception.Timeout, dns.resolver.NoNameservers) as e:
        # ('error:', e)
        return []
    for i in txt.response.answer:
        for j in i.items:
            # print (j.to_text())
            var.append(''.join(f for f in j.to_text()
                               if f not in ('"', '\'', ' ')
                               )
                       )
    return var


def get_list_seller_for_site(site_id):
    """
    Возвращаем список продавцов для сайта в виде объектов. Учитывает фильтр в мойсайте.
    :param site_id:
    :return: Список объектов Seller
    """
    from market.models import Seller
    site = Site.objects.get(pk=site_id)
    kwargs = {}
    kwargs.update(sites=site.domain,
                  is_published=True)
    queryset_theme = site.mysite.parse_query()
    q_object = SQ(**kwargs)
    q_object.add(queryset_theme, SQ.AND)
    facet = SearchQuerySet().filter(q_object).facet('seller').facet_counts()
    seller_list = []
    for name, coint in facet['fields']['seller']:
        seller_list.append(Seller.objects.filter(name=name)[0])
    return seller_list
