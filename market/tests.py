import unittest

from django.conf import settings
from django.contrib.sites.models import Site
from django.test import TestCase
# Create your tests here.
from django.urls import reverse
from mixer.backend.django import mixer

from base.base_test_lib import clear_search_index
# from cpashop.test_settings import HTTPBIN_SERVER
from goto_url.utils import goto_url
from market import models
from market.templatetags.deeplink_lot import deeplink_lot
from market.views import LotDetailView


class MarketClientTest(TestCase):
    def setUp(self):
        self.url = reverse('main_view')
        self.site = Site.objects.get(pk=1)
        self.mysite = models.MySite.objects.create(site=self.site)
        self.lots = mixer.cycle(5).blend(models.Lot, status=models.ENABLED, name=mixer.random)
        for lot in self.lots:
            lot.seller.sites.add(self.mysite)
            lot.save()

    def tearDown(self):
        clear_search_index()
        # pass

    def test_data(self):
        self.assertEqual(len(self.lots), 5)

    def test_context_processors_site1(self):
        response = self.client.get(self.url)
        self.assertEqual(response.context['SITE'].domain, self.site.domain)
        self.assertFalse(response.context['verify_admitad'])

    @unittest.expectedFailure
    def test_context_processors_site2(self):
        self.mysite.verify_admitad = 'qwerty'
        self.mysite.save()
        response = self.client.get(self.url)
        self.assertEqual(response.context['SITE'].domain, self.site.domain)
        self.assertEqual(response.context['verify_admitad'], 'qwerty')

    @unittest.skip('Сейчас тут ошибка в тестах')
    def test_market_view_ENABLED_lot(self):
        response = self.client.get(self.url)
        template = response.templates[0]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(template.name, 'default/index.html')
        self.assertEqual(len(response.context['new_lots']), 5)
        self.assertEqual(len(response.context['sale']), 5)
        self.assertEqual(len(response.context['special']), 0)

    def test_market_view_DISABLED_lot(self):
        for lot in self.lots:
            lot.status = models.DISABLED
            lot.save()
        response = self.client.get(self.url)
        self.assertEqual(len(response.context['new_lots']), 0)
        self.assertEqual(len(response.context['sale']), 0)
        self.assertEqual(len(response.context['special']), 0)

    def test_market_view_DRAFT_lot(self):
        for lot in self.lots:
            lot.status = models.DRAFT
            lot.save()
        response = self.client.get(self.url)
        self.assertEqual(len(response.context['new_lots']), 0)
        self.assertEqual(len(response.context['sale']), 0)
        self.assertEqual(len(response.context['special']), 0)

    def test_market_view_DELETE_lot(self):
        for lot in self.lots:
            lot.status = models.DELETE
            lot.save()
        response = self.client.get(self.url)
        self.assertEqual(len(response.context['new_lots']), 0)
        self.assertEqual(len(response.context['sale']), 0)
        self.assertEqual(len(response.context['special']), 0)


class MarketClientLotDetailViewTest(TestCase):
    def setUp(self):
        # self.url = reverse('lot_view', args=[1,])
        self.lots = mixer.cycle(5).blend(models.Lot, status=models.ENABLED, name=mixer.random)
        self.url = reverse('lot_view', args=[self.lots[0].pk, ])

    def tearDown(self):
        clear_search_index()

    def test_lot_detail_view(self):
        # Товар есть, но пришли не по каноническому урлу
        response = self.client.get(self.url)
        self.assertEqual(response.resolver_match.func.__name__, LotDetailView.as_view().__name__)
        self.assertEqual(response.status_code, 301)
        self.assertEqual(response.url,
                         reverse('slug_lot_view', kwargs={'slug': self.lots[0].web_slug, 'pk': self.lots[0].pk}))

    def test_lot_404(self):
        # Товара нет и не было, 404
        url_404 = reverse('lot_view', args=[1111, ])
        response = self.client.get(url_404)
        self.assertEqual(response.resolver_match.func.__name__, LotDetailView.as_view().__name__)
        self.assertEqual(response.status_code, 404)

    def test_bad_slug_lot(self):
        url_bad_slud = '/lot/bla-bal-baka-%s/' % self.lots[0].pk
        response = self.client.get(url_bad_slud)
        self.assertEqual(response.status_code, 301)
        self.assertEqual(response.resolver_match.func.__name__, LotDetailView.as_view().__name__)
        self.assertEqual(response.url,
                         reverse('slug_lot_view', kwargs={'slug': self.lots[0].web_slug, 'pk': self.lots[0].pk}))

    def test_good_slug_lot(self):
        url_good_slug = reverse('slug_lot_view', kwargs={'slug': self.lots[0].web_slug, 'pk': self.lots[0].pk})
        response = self.client.get(url_good_slug)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func.__name__, LotDetailView.as_view().__name__)

    def test_get_absolute_url_not_slug_lot(self):
        lot = mixer.blend(models.Lot, status=2)
        self.assertEqual(lot.get_absolute_url(), reverse('lot_view', args=[lot.pk, ]))

    def test_lot_disable_redirect_arhive(self):
        lot = models.Lot.objects.first()
        lot.name = 'David Johns'
        lot.status = models.DISABLED
        lot.save()
        url = reverse('lot_view', args=[lot.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.resolver_match.func.__name__, LotDetailView.as_view().__name__)
        self.assertEqual(response.status_code, 301)
        kanon_url = '/%s-%sa/' % ('david-johns', lot.pk)
        self.assertEqual(kanon_url, response.url)

    def test_lot_up_enabled(self):
        lot = mixer.blend(models.Lot, status=models.ENABLED)
        self.assertEqual(lot.status, models.ENABLED)
        lot.up()
        self.assertEqual(lot.status, models.ENABLED)

    def test_lot_up_draft(self):
        lot = mixer.blend(models.Lot, status=models.DRAFT)
        self.assertEqual(lot.status, models.DRAFT)
        lot.up()
        self.assertEqual(lot.status, models.ENABLED)

    def test_lot_up_disable(self):
        lot = mixer.blend(models.Lot, status=models.DISABLED)
        self.assertEqual(lot.status, models.DISABLED)
        lot.up()
        self.assertEqual(lot.status, models.ENABLED)

    def test_lot_up_delete(self):
        lot = mixer.blend(models.Lot, status=models.DELETE)
        self.assertEqual(lot.status, models.DELETE)
        lot.up()
        self.assertEqual(lot.status, models.DELETE)

    def test_lot_down_disabled(self):
        lot = mixer.blend(models.Lot, status=models.DISABLED)
        self.assertEqual(lot.status, models.DISABLED)
        lot.down()
        self.assertEqual(lot.status, models.DISABLED)

    def test_lot_down_delete(self):
        lot = mixer.blend(models.Lot, status=models.DELETE)
        self.assertEqual(lot.status, models.DELETE)
        lot.down()
        self.assertEqual(lot.status, models.DELETE)

    def test_lot_down_enabled(self):
        lot = mixer.blend(models.Lot, status=models.ENABLED)
        self.assertEqual(lot.status, models.ENABLED)
        lot.down()
        self.assertEqual(lot.status, models.DISABLED)

    def test_lot_down_draft(self):
        lot = mixer.blend(models.Lot, status=models.DRAFT)
        self.assertEqual(lot.status, models.DRAFT)
        lot.down()
        self.assertEqual(lot.status, models.DISABLED)

    def test_lot_delete(self):
        lot = mixer.blend(models.Lot, status=models.ENABLED)
        self.assertEqual(lot.status, models.ENABLED)
        lot.lot_delete()
        self.assertEqual(lot.status, models.DELETE)


class MarketModelsTest(TestCase):
    def test_template(self):
        folder = 'test_folder'
        tem = mixer.blend(models.Template, folder=folder)
        self.assertEqual(str(tem), tem.folder)
        self.assertEqual(str(tem), folder)

    def test_mysite_str(self):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend(models.MySite, site=site)
        self.assertEqual(str(mysite), 'test_test.test')
        self.assertEqual(mysite.name, 'test_test.test')

    def test_mysite_get_verify_admitad_false(self):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend(models.MySite, site=site)
        self.assertFalse(mysite.get_verify_admitad())

    def test_mysite_get_verify_admitad_1(self):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend(models.MySite, site=site)
        mysite.verify_admitad = 'qwerty1234'
        mysite.save()
        self.assertEqual(mysite.get_verify_admitad(), 'qwerty1234')

    def test_mysite_get_http_domain(self):
        site = Site.objects.create(domain='test_test.test', name='test_test.test')
        mysite = mixer.blend(models.MySite, site=site)
        self.assertEqual(mysite.get_http_domain(), 'http://test_test.test')

    @unittest.expectedFailure
    def test_mysite_get_absolute_url(self):
        mysite = mixer
        self.assertEqual(mysite.get_absolute_url(), reverse('mysite-update', args=[mysite.pk, ]))

    def test_mysite_get_admitad_website_none(self):
        mysite = mixer.blend(models.MySite)
        self.assertIsNone(mysite.get_admitad_website())

    def test_deeplink(self):
        mysite = mixer.blend(models.MySite)
        seller = mixer.blend(models.Seller)
        dl = models.Deeplink.objects.create(site=mysite,
                                            seller=seller,
                                            url='http://test.test')
        self.assertEqual(str(dl), 'http://test.test')

    def test_category_str(self):
        feed = mixer.blend(models.Feed)
        cat = mixer.blend(models.Category, feed=feed, name='test')
        self.assertEqual(str(cat), 'test')

    def test_category_get_seller(self):
        feed = mixer.blend(models.Feed)
        cat = mixer.blend(models.Category, feed=feed)
        self.assertEqual(cat.get_seller(), feed.seller.name)

    def test_category_get_category_image_not_lots(self):
        cat = mixer.blend(models.Category, )
        self.assertIsNone(cat.get_category_image())

    def test_category_get_category_image(self):
        cat = mixer.blend(models.Category, )
        lot = mixer.blend(models.Lot, category=cat)
        image = mixer.blend(models.Image, lot=lot, is_main=True)
        self.assertEqual(cat.get_category_image(), image)

    def test_category_get_category_image_parent_cat(self):
        cat = mixer.blend(models.Category, )
        cat_parent = mixer.blend(models.Category, parent=cat)
        lot = mixer.blend(models.Lot, category=cat_parent)
        image = mixer.blend(models.Image, lot=lot, is_main=True)
        self.assertEqual(cat.get_category_image(), image)

    def test_category_web_slug(self):
        cat = mixer.blend(models.Category, name='Правила Игры')
        self.assertEqual(cat.web_slug, 'pravila-igry')

    def test_category_get_absolute_url_slug(self):
        cat = mixer.blend(models.Category, name='Правила Игры')
        self.assertEqual(cat.get_absolute_url(), reverse('slug-category-view',
                                                         kwargs={'slug': cat.web_slug, 'pk': cat.pk}))

    def test_category_get_absolute_url_not_slug(self):
        cat = mixer.blend(models.Category, name='')
        self.assertEqual(cat.get_absolute_url(), reverse('category-view', args=(cat.id,)))

    def test_param_str(self):
        param = mixer.blend(models.Param, name='test')
        self.assertEqual(str(param), 'test')

    def test_image_str_downloaded_from(self):
        # Картинка не была закачена!!!
        image = mixer.blend(models.Image, image=None, downloaded_from='http://test.test')
        self.assertEqual(str(image), image.downloaded_from)

    def test_image_fetch_downloaded_not_save_file(self):
        # Проверяем скачку картинки, но саму картинку не сохраняем
        image_url = '%s/image/jpeg' % settings.HTTPBIN_SERVER
        lot = mixer.blend(models.Lot)
        image = models.Image.fetch(url=image_url, lot=lot, is_main=True)
        self.assertEqual(image_url, image.downloaded_from)
        self.assertIsNone(image.image.name)
        self.assertTrue(image.is_main)
        self.assertEquals(image.get_image(), image.downloaded_from)

    def test_image_fetch_downloaded_not_save_file_not_main(self):
        # Проверяем скачку картинки, но саму картинку не сохраняем
        image_url = '%s/image/jpeg' % settings.HTTPBIN_SERVER
        lot = mixer.blend(models.Lot)
        image = models.Image.fetch(url=image_url, lot=lot)
        self.assertFalse(image.is_main)

    def test_image_fetch_downloaded_save_file(self):
        # Проверяем скачку картинки, картинку Cохраняем
        image_url = '%s/image/jpeg' % settings.HTTPBIN_SERVER
        lot = mixer.blend(models.Lot, seller__load_image=True)
        image = models.Image.fetch(url=image_url, lot=lot, is_main=True)
        self.assertEqual(image_url, image.downloaded_from)
        self.assertNotEquals(image.get_image(), image.downloaded_from)
        self.assertIsNotNone(image.image.name)
        self.assertTrue(image.is_main)


class MarketDeeplinkTest(TestCase):
    def setUp(self):
        site, create = Site.objects.update_or_create(pk=2,
                                                     defaults={'domain': 'testserver',
                                                               'name': 'testserver'})
        mysite, create = models.MySite.objects.get_or_create(site=site)
        seller = mixer.blend(models.Seller, site=mysite)
        self.deeplink_url = 'http://deeplink_seller.ru'
        self.deeplink = mixer.blend(models.Deeplink, site=mysite, seller=seller, url=self.deeplink_url)
        self.seller_url = 'http://test_test.test'
        self.lot = mixer.blend(models.Lot, url=self.seller_url, seller=seller)
        self.url_is_deeplink = goto_url('%s?ulp=%s' % (self.deeplink_url, self.seller_url))
        response = self.client.get('/')
        self.context = response.context

    def test_tag_deeplink_lot_not_lot(self):
        self.assertEqual(deeplink_lot(context=self.context), '#')

    # def test_tag_deeplink_lot_is_lot(self):
    #     self.assertEqual(self.lot.get_deeplink(site_id=2), self.url_is_deeplink)
    #     self.assertEqual(deeplink_lot(context=self.context, lot=self.lot), self.url_is_deeplink)

    def test_tag_deeplink_lot_is_lot_2(self):
        lot = mixer.blend(models.Lot, url=self.seller_url)
        self.assertEqual(deeplink_lot(context=self.context, lot=lot), goto_url(self.seller_url))

    def test_deeplink_not_site_id(self):
        self.assertEqual(self.lot.get_deeplink(), goto_url(self.seller_url))
