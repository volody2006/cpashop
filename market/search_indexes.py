# -*- coding: utf-8 -*-
# http://django-haystack.readthedocs.io/en/v2.4.1/tutorial.html

from celery_haystack.indexes import CelerySearchIndex
from haystack import indexes

from market.models import Lot, ENABLED


class LotIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    description = indexes.CharField(model_attr='description', default='')
    # is_available = indexes.BooleanField()
    seller = indexes.CharField(model_attr='seller', faceted=True)
    # Конечная категория товара (тег)
    category = indexes.CharField(model_attr='category', faceted=True, null=True)
    # category_ancestors = indexes.CharField(model_attr='category_get_ancestors', null=True, faceted=True)
    # Родительские категории
    category_parent = indexes.CharField(model_attr='category_parent', null=True, faceted=True)
    categories = indexes.MultiValueField(faceted=True)
    # category_children = indexes.MultiValueField(faceted=True)

    # статус лота
    # is_published = indexes.BooleanField()
    # adult = indexes.BooleanField(model_attr='adult', indexed = False)
    price = indexes.IntegerField(default=0, indexed=False)
    old_price = indexes.IntegerField(default=0, indexed=False)
    currency = indexes.CharField(model_attr='currency', indexed=False, default='')
    model = indexes.CharField(model_attr='model', default='')
    vendor_code = indexes.CharField(model_attr='vendor_code', default='')
    vendor = indexes.CharField(model_attr='vendor', default='', faceted=True)
    type_prefix = indexes.CharField(model_attr='type_prefix', default='', indexed=False)
    type = indexes.CharField(model_attr='type', default='', indexed=False)
    amount_discount = indexes.IntegerField()
    sites = indexes.MultiValueField(model_attr='get_sites_list', faceted=True, indexed=False)
    date_changed = indexes.DateTimeField(model_attr='date_changed', indexed=False)

    # def prepare_is_available(self, obj):
    #     return obj.is_available

    # def prepare_status(self, obj):
    #     return obj.status

    # def prepare_is_published(self, obj):
    #     return obj.status == ENABLED

    def prepare_amount_discount(self, obj):
        return obj.amount_discount()

    def prepare_price(self, obj):
        return obj.price

    def prepare_old_price(self, obj):
        return obj.old_price

    def prepare_categories(self, obj):
        # return [obj.category_id]
        list_categorys = []
        try:
            list_categorys.append(obj.category.pk)
            for category in obj.category.get_ancestors():
                list_categorys.append(category.pk)
        except AttributeError:
            pass
            # cat_s = Category.objects.filter(name = category.name)
            # for cat in cat_s:
            #     list_categorys.append(cat.pk)
        return list_categorys

    def prepare_children(self, obj):
        # return [obj.category_id]
        list_categorys = []
        try:
            list_categorys.append(obj.category.pk)
            for category in obj.category.get_children():
                list_categorys.append(category.pk)
        except AttributeError:
            pass
        return list_categorys

    def get_model(self):
        return Lot

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        # return self.get_model().objects.all()
        return self.get_model().objects.filter(status=ENABLED)
