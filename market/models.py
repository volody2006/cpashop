# -*- coding: utf-8 -*-
import logging
import os
import urllib.error
import urllib.parse
import urllib.request
import uuid

from PIL import Image as PI
from dj_upload_to import UploadTo
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.managers import CurrentSiteManager
from django.contrib.sites.models import Site
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from django.http import Http404
from django.utils import six
from mptt.models import MPTTModel, TreeForeignKey
from slugify import slugify

from base.models import CommonAbstractModel
from goto_url.utils import goto_url
from market.image_lib import get_image_size
from market.utils import not_escape
from my_admitad_api.models import Website, AdmCampaign

logger = logging.getLogger(__name__)

DRAFT = 1
ENABLED = 2
DISABLED = 3
DELETE = 4

LOT_STATUS_CHOICES = (
    (DRAFT, 'Черновик'),
    (ENABLED, 'Опубликован'),
    (DISABLED, 'Не опубликован'),
    (DELETE, 'Удален'),
)


# Черновик - Товар ни разу не опубликованный
# Опубликован - Товар может быть показан
# Не опубликован - Товар снять с показа, но имеет возможность стать опубликованным
# Удален - товар снят с показа навсегда. Например, по решению администратора.


def upload_to(prefix=None, **kwargs):
    class MyUploadTo(UploadTo):
        def get_prefix(self, model_instance, filename):
            # return 'test_media_file'
            if not model_instance.lot:
                return 'default'
            return "seller/%s" % model_instance.lot.seller.pk

    if prefix is None:
        prefix = MyUploadTo()
    return MyUploadTo(prefix=prefix, **kwargs)


class Template(CommonAbstractModel):
    name = models.CharField(max_length=255, verbose_name='Название')
    folder = models.CharField(max_length=255, verbose_name='Название директории в папке шаблонов', unique=True)

    paginate_by = models.IntegerField(default=12, verbose_name='Оптимальное количество товаров на странице, '
                                                               'зависит от шаблона')
    number_of_categories = models.IntegerField(default=4, verbose_name='Количество пунктов меню не считая Главного')
    number_of_subcategories = models.IntegerField(default=6, verbose_name='Количество подкатегорий в меню')
    user = models.ForeignKey(User, null=True, blank=True)
    hidden = models.BooleanField(default=False, help_text='Шаблон скрыт')
    crm_template = models.BooleanField(default=False, help_text='Шаблон для админки?')

    def __str__(self):
        return self.folder

    @property
    def is_crm_template(self):
        admin_template = settings.ADMIN_FOLDER
        if str(admin_template) == str(self.folder):
            return True
        return False

    def get_absolute_url(self):
        return reverse('template_update', args=[self.pk, ])


def escape_query(q):
    return u' '.join(q.split('/'))


class MySite(models.Model):
    site = models.OneToOneField(Site, unique=True, primary_key=True, null=False, editable=False)
    template = models.ForeignKey(Template, null=True)

    verify_admitad = models.CharField(max_length=255, verbose_name='Верификационный код Адмитад',
                                      null=True, blank=True)
    verify_yandex = models.CharField(max_length=255, verbose_name='Код подтверждения yandex',
                                     null=True, blank=True)
    verify_bing = models.CharField(max_length=32, verbose_name='Код подтверждения bing.com',
                                   null=True, blank=True)

    web_property_id = models.CharField(blank=True, null=True, max_length=15, verbose_name='web property id google')

    objects = models.Manager()
    on_site = CurrentSiteManager('site')
    user = models.ForeignKey(User, null=True, blank=True)
    manual_verification = models.BooleanField(default=False, help_text='Верификация не требуется')

    # Дальше блок идет для адмитад
    DESCRIPTION_DEFAULT = 'Данный сайт зарегистрирован в онлайн-сервисе mycpashop.ru. ' \
                          'Планируется привлечение трафика с поисковых систем.'

    description = models.CharField(max_length=2000,
                                   default=DESCRIPTION_DEFAULT,
                                   verbose_name='Описание',
                                   help_text='Как вы будете рекламировать партнерскую программу?')
    adm_website = models.OneToOneField(Website, null=True, blank=True)
    # adm_campaigns = models.ManyToManyField(AdmCampaign,
    #                                        help_text='Подключенные программы в адмитад')
    sellers = models.ManyToManyField('Seller',
                                     help_text='Подключенные программы')
    is_https = models.BooleanField(default=False)

    # Персонализация сайта. Показ только определенных товаров. Пока реализовываем так.
    query = models.TextField(verbose_name="Запрос на поиск",
                             help_text='Пример: (text:acer OR text:телефон) AND (seller:Связной OR seller:otto) '
                                       'AND price__gt:110',
                             null=True, blank=True, default='')

    @property
    def query_string(self):
        from django.utils.encoding import smart_str
        return escape_query(smart_str(self.query))

    def parse_query(self):
        from haystack_queryparser import ParseSQ
        parser = ParseSQ()
        return parser.parse(self.query)

    @property
    def is_crm(self):
        # Сайт у которого админский шаблон, автоматом является админским
        try:
            return self.template.is_crm_template
        except AttributeError:
            return False

    @property
    def name(self):
        return self.site.name

    @property
    def domain(self):
        return self.site.domain

    def get_admitad_website(self):
        if self.adm_website:
            return self.adm_website
        from my_admitad_api.managers import search_adm_website
        logger.info('Пытаемся найти сайт в системе адмитад для сайта %s' % self.domain)
        return search_adm_website(self.site_id)

    def get_verify_admitad(self):
        # website = self.get_admitad_website()
        if self.verify_admitad:
            return self.verify_admitad
        if self.adm_website:
            self.verify_admitad = self.adm_website.verification_code
            self.save()
            return self.verify_admitad
        return False

    def get_http_domain(self):
        if self.is_https:
            return 'https://%s' % self.site.domain
        return 'http://%s' % self.site.domain

    http_domain = property(get_http_domain)

    def __str__(self):
        return self.site.domain

    def get_absolute_url(self):
        return reverse('mysite', args=[self.pk, ])
        # return reverse('mysite_update', args=[self.pk, ])


class CpaSite(CommonAbstractModel):
    name = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(verbose_name='Описание', null=True, blank=True)

    def __str__(self):
        return self.name


class Seller(CommonAbstractModel):
    """
    Продавец товара
    """
    name = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(verbose_name='Описание', null=True, blank=True)
    # site = models.ManyToManyField(MySite)
    xml_feed = models.TextField(help_text='ссылка на xml с товарами', null=True, blank=True)
    load_image = models.BooleanField(default=False, help_text='Скачивать картинки?')
    url_home = models.TextField(help_text='Главная страница продавца', null=True, blank=True)
    url_delivery = models.TextField(help_text='Страница доставки', null=True, blank=True)
    url_payment = models.TextField(help_text='страница оплаты', null=True, blank=True)
    url_contact = models.TextField(help_text='страница контактов', null=True, blank=True)
    adm_campaign = models.ForeignKey(AdmCampaign, null=True, blank=True)

    # cpa = models.ForeignKey(CpaSite)
    @property
    def sites(self):
        return self.mysite_set.all()


    def get_seller_url(self):
        return urllib.parse.unquote_plus(self.url_home)

    def get_deeplink(self, site_id=None):

        # Возвращаем ссылку в зависимости от сайта.
        if site_id is None:
            # Незнаем какой сайт? возвращаем не партнерский урл
            return goto_url(self.get_seller_url())
        try:
            site = MySite.objects.get(site=site_id)
            deeplink_url = Deeplink.objects.get(seller=self,
                                                site=site)
            return goto_url(('%s?ulp=%s' % (deeplink_url, self.url_home)))
        except Exception as e:
            return goto_url(self.get_seller_url())

    def __str__(self):
        if self.name:
            return self.name
        else:
            return 'No name - id %s' % self.pk


class Deeplink(models.Model):
    url = models.TextField(help_text='Целевая страница для трафика')
    site = models.ForeignKey(MySite)
    seller = models.ForeignKey(Seller)
    active = models.BooleanField(default=False, help_text='Ссылка активна?')

    def __str__(self):
        return self.url

    @property
    def user(self):
        return self.site.user

    def get_absolute_url(self):
        return reverse('deeplink_update', args=[self.pk, ])

    def seller_add(self):
        if self.active:
            self.seller.sites.add(self.site)

    def seller_remove(self):
        if not self.active:
            self.seller.sites.remove(self.site)

    def seller_status(self):
        if self.site in self.seller.sites.all():
            return True
        return False

    class Meta(object):
        unique_together = ('site', 'seller')


class Category(MPTTModel, CommonAbstractModel):
    feed = models.ForeignKey('Feed', null=True, blank=True, related_name='category_feed')
    name = models.CharField(max_length=255, default='')
    parent = TreeForeignKey('self', related_name='children',
                            null=True,
                            blank=True)
    source_id = models.CharField(max_length=255)
    source_parent_id = models.CharField(max_length=255,
                                        null=True,
                                        blank=True)

    def __str__(self):
        return self.name

    def get_seller(self):
        return self.feed.seller.name

    def get_category_image(self):
        # TODO: Результат надо кешировать
        if Lot.objects.filter(category=self).exists():
            # Если в категории есть товары
            for lot in Lot.objects.filter(category=self):
                if lot.main_foto() is not None:
                    return lot.main_foto()
                    # Если в категории нет товаров, шарим по "детям" категории
        for category in Category.objects.filter(parent=self):
            for lot in Lot.objects.filter(category=category):
                if lot.main_foto() is not None:
                    return lot.main_foto()
        return None

    @property
    def web_slug(self):
        return slugify(self.name)

    def get_absolute_url(self, user_id=None):
        """
        Урл категории
        """
        if self.web_slug:
            return reverse('slug-category-view', kwargs={'slug': self.web_slug, 'pk': self.pk})
        return reverse('category-view', args=(self.id,))

    class Meta(object):
        unique_together = ("feed", "source_id")
        indexes = [
            models.Index(['lft', 'tree_id'], name='strange_index')
        ]


class Param(CommonAbstractModel):
    name = models.CharField(max_length=255, verbose_name='название параметра', null=True, blank=True)
    value = models.CharField(max_length=255, verbose_name='значение параметра', null=True, blank=True)
    unit = models.CharField(max_length=255, verbose_name='единица измерения (для числовых параметров)',
                            null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta(object):
        unique_together = ('name', 'value', 'unit')


class Image(CommonAbstractModel):
    """
    картинка
    """

    image = models.ImageField(upload_to=upload_to(),
                              verbose_name='Картинка', blank=True, null=True)
    downloaded_from = models.TextField('Скачано с', blank=True, null=True)
    is_main = models.BooleanField(default=False)
    lot = models.ForeignKey('Lot', related_name='image_lot', blank=True, null=True)

    def check_image_format(self):
        """
        Проверяет, является ли прикреплённый к модели файл корректным
        изображением.

        Возвращает True в случае, если является. Если не является, генерируется
        исключение ValueError.

        """
        if not bool(self.image):
            raise ValueError('No file presented')
        if not os.path.exists(self.image.path):
            raise ValueError('Presented file is not exists in file system')

        get_image_size(self.image)

        return True

    def image_resize(self):
        if self.lot.seller.load_image is False:
            return
        file_name = self.image.path
        filter = PI.ANTIALIAS
        image_file = PI.open(fp=file_name)
        # width = image_file.width
        # height = image_file.height
        # if width > MAX_IMAGE_SIZE[0]:
        #     width = MAX_IMAGE_SIZE[0]
        # if height > MAX_IMAGE_SIZE[1]:
        #     height = MAX_IMAGE_SIZE[1]
        image = image_file.resize(image_file.size, filter)
        image.save(file_name)

    def load(self):
        if settings.IS_TEST:
            logger.info('В тестах закачка файлов не работает.')
            return
        if not self.lot.seller.load_image:
            logger.info('Закачка файла не требуется')
            return
        try:
            import requests
            response = requests.get(self.downloaded_from)
            logger.info('Connected with URL and got response: %d' % response.status_code)

            if response.status_code > 399:
                logger.info('Failed to fetch %s due HTTP error %s' % (self.downloaded_from, response.status_code))
                return None

            # Теперь нужно проверить, является ли файл картинкой
            fp = six.BytesIO(response.content)
            try:
                PI.open(fp).verify()
                del fp
            except:
                logger.exception('Pillow thinks that %s is not an image file' % self.downloaded_from)
                return None

            ext = (os.path.splitext(self.downloaded_from)[1].strip('.').lower() or 'jpg').split('?')[0]

            if ext not in ['jpg', 'jpeg', 'gif', 'png']:
                ext = 'jpg'

            if not ext:
                logger.info('[Unable determine file extension - it\'s abnormal]')

            filename = '%s.%s' % (uuid.uuid4(), ext)

            logger.info('Got image filename: %s' % filename)


            self.image.save(filename, ContentFile(response.content))
            self.save()

        except requests.exceptions.ConnectionError as e:
            logger.info('Не удалось загрузить %s. Ошибка подключения. %s' % (self.downloaded_from, e))

        except urllib.error.ContentTooShortError:
            logger.info('couldnt retreive full image. path: %s' % self.downloaded_from)

        except urllib.error.HTTPError as e:
            logger.info(e.code)

        except urllib.error.URLError as e:
            logger.info(e.reason)

        except IOError as e:
            logger.info('IO error: %s' % e)
        except OSError as e:
            logger.info('OS error: %s' % e)

    @staticmethod
    def fetch(url, lot, is_main=False):
        if not lot.seller.load_image:
            return None
        try:
            image = Image.objects.filter(lot=lot,
                                         downloaded_from=url)[0]
            logger.info('This file (%s) had already downloaded' % url)
            if image.is_main == is_main:
                image.is_main = is_main
                image.save()
            return image

        except (Image.DoesNotExist, IndexError):
            logger.info('File %s not exists, downloading it' % url)

            try:
                import requests
                response = requests.get(url)
                logger.info('Connected with URL and got response: %d' % response.status_code)

                # if response.status_code != 200:
                if response.status_code > 399:
                    logger.info('Failed to fetch %s due HTTP error %s' % (url, response.status_code))
                    return None

                # Теперь нужно проверить, является ли файл картинкой
                fp = six.BytesIO(response.content)
                try:
                    PI.open(fp).verify()
                    del fp
                except:
                    logger.exception('Pillow thinks that %s is not an image file' % url)
                    return None

                ext = (os.path.splitext(url)[1].strip('.').lower() or 'jpg').split('?')[0]

                if ext not in ['jpg', 'jpeg', 'gif', 'png']:
                    ext = 'jpg'

                if not ext:
                    logger.info('[Unable determine file extension - it\'s abnormal]')

                filename = '%s.%s' % (uuid.uuid4(), ext)

                logger.info('Got image filename: %s' % filename)

                image = Image(lot=lot,
                              is_main=is_main,
                              downloaded_from=url)

                image.image.save(filename, ContentFile(response.content))
                image.save()
                # image.image_resize()

                return image

            except requests.exceptions.ConnectionError as e:
                logger.info('Failed to fetch %s due connection error: %s' % (url, e))

            except urllib.error.ContentTooShortError:
                logger.info('couldnt retreive full image. path: %s' % url)

            except urllib.error.HTTPError as e:
                logger.info(e.code)

            except urllib.error.URLError as e:
                logger.info(e.reason)

            except IOError as e:
                logger.info('IO error: %s' % e)
            except OSError as e:
                logger.info('OS error: %s' % e)

        return None

    def set_main(self):
        pass

    def delete_main(self):
        pass

    def destroy(self):
        pass

    def _createdir(self, dir):
        if not os.path.exists(dir):
            try:
                os.makedirs(dir, 0o755)
            except OSError as e:
                raise EnvironmentError(
                    "Cache directory '%s' does not exist "
                    "and could not be created'" % self._dir)

    def get_image(self):
        if self.image:
            return "%s" % self.image.url
        else:
            return self.downloaded_from

    def __str__(self):
        return self.get_image()

    class Meta:
        # ordering = ['date_created']
        unique_together = (("lot", "downloaded_from"),)


class Currency(CommonAbstractModel):
    iso_code = models.CharField(max_length=4, unique=True,
                                verbose_name='ISO 4217 Код')
    short_name = models.CharField(default='', blank=True, max_length=5, verbose_name='Короткое имя, например руб')
    full_name = models.CharField(default='', blank=True, max_length=30, verbose_name='Полное имя, например рубль')

    def __str__(self):
        if self.short_name:
            return self.short_name
        return '%s' % self.iso_code


class Lot(CommonAbstractModel):
    """
    Лот
    """
    offer_id = models.CharField(max_length=255, default='default',
                                blank=True)
    name = models.CharField(max_length=255, default='',
                            verbose_name='Название товара', blank=True, db_index=True)
    description = models.TextField(verbose_name='Описание',
                                   blank=True, db_index=True)

    is_available = models.BooleanField(default=True,
                                       verbose_name='Статус доступности товара')

    seller = models.ForeignKey(Seller, verbose_name='Продавец',
                               related_name='seller_lots')
    category = models.ForeignKey(Category, verbose_name='Категория',
                                 blank=True, db_index=True, null=True,
                                 related_name='rel_category_lots')

    # статус лота
    status = models.PositiveIntegerField(choices=LOT_STATUS_CHOICES,
                                         default=DRAFT, db_index=True,
                                         verbose_name='Статус лота')
    adult = models.BooleanField(default=False, verbose_name='Для взрослых')
    price = models.PositiveIntegerField(null=True, blank=True, verbose_name='цена')

    old_price = models.PositiveIntegerField(null=True, blank=True, verbose_name='старая цена')
    currency = models.ForeignKey(Currency, blank=True, null=True, verbose_name='Валюта')
    model = models.CharField(max_length=255, null=True, blank=True)
    vendor_code = models.CharField(max_length=255, null=True, blank=True)
    vendor = models.CharField(max_length=255, null=True, blank=True)
    type_prefix = models.CharField(max_length=255, null=True, blank=True)
    type = models.CharField(max_length=255, null=True, blank=True)
    url = models.TextField(default='')

    lot_params = models.ManyToManyField(Param)

    @property
    def web_slug(self):
        return slugify(self.name)

    @property
    def is_published(self):
        if self.status == ENABLED:
            return True
        return False

    def category_parent(self):
        try:
            return self.category.parent
        except AttributeError:
            return None

    def category_get_ancestors(self):
        if not self.category:
            return []
        categorys = self.category.get_ancestors()
        list_categorys = []
        for category in categorys:
            # list_categorys.append(category.name.encode('utf8'))
            list_categorys.append(category.name)
        return list_categorys

    def get_not_escape_description(self):
        return not_escape(self.description)

    def get_keywords(self):
        # Мета keywords: имя, продавец, категория, модель, вендор, размер, цвет, (все через запятую)
        keywords = []
        keywords.append(self.name)
        keywords.append(', ')
        keywords.append(self.seller.name)

        if self.category:
            if self.category != '':
                keywords.append(', ')
                keywords.append(self.category.name)

        if self.model:
            keywords.append(', ')
            keywords.append(self.model)
        try:
            if self.vendor:
                if str(self.vendor).lower() != str(self.seller.name).lower():
                    keywords.append(', ')
                    keywords.append(self.vendor)
        except UnicodeEncodeError:
            pass
        spisik = ''
        for key in keywords:
            spisik += key.lower()
        return spisik

    def get_image_alt(self):
        # Имя, модель, размер, цвет.
        keywords = []
        keywords.append(self.name)

        if self.model:
            keywords.append(', ')
            keywords.append(self.model)

        spisik = ''
        for key in keywords:
            spisik += key
        return spisik

    def get_image_title(self):
        # Картинка титл: Имя, цена
        keywords = []
        keywords.append(self.name)
        keywords.append(', ')
        keywords.append(str(self.price))
        if self.currency == 'RUR' or self.currency == 'RUB':
            keywords.append(' ')
            keywords.append('рублей')

        spisik = ''
        for key in keywords:
            spisik += key
        return spisik

    # @property
    def amount_discount(self):
        if self.old_price:
            if self.old_price > self.price:
                # discount = (1 - (self.price/self.old_price)) * 100
                discount = int(100 * ((self.old_price - self.price) / float(self.old_price)))
                return discount
        return 0

    # @property
    def get_sites(self):
        # Возвращает список сайтов на которых данный лот может отображаться
        sites = self.seller.sites.all()
        return sites

    @property
    def get_sites_list(self):
        sites = self.get_sites()
        # if self.pk % 2 == 0:
        #     site = sites[0]
        # else:
        #     site = sites[1]
        list_site = []
        # list_site.append(site.site.domain.encode('utf8'))
        for site in sites:
            list_site.append(site.site.domain.encode('utf8'))
        return list_site

    def get_seller_url(self):
        return urllib.parse.unquote_plus(self.url)

    def get_deeplink(self, site_id=None):

        # Возвращаем ссылку в зависимости от сайта.
        if site_id is None:
            # Незнаем какой сайт? возвращаем не партнерский урл
            return goto_url(self.get_seller_url())
        try:
            site = MySite.objects.get(site=site_id)
            deeplink_url = Deeplink.objects.get(seller=self.seller,
                                                site=site)
            return goto_url(('%s?ulp=%s' % (deeplink_url, self.url)))
        except Deeplink.DoesNotExist:
            return goto_url(self.get_seller_url())
        except Exception as e:
            logging.exception("ER get_deeplink, site_id %s, lot_id %s" % (site_id, self.pk))
            return goto_url(self.get_seller_url())

    def get_absolute_url(self):
        if not self.is_published:
            return self.get_arche_url()
        if self.web_slug:
            return reverse('slug_lot_view', kwargs={'slug': self.web_slug, 'pk': self.id})
        return reverse('lot_view', args=[self.pk, ])

    def get_arche_url(self):
        if self.name:
            return reverse('arche-view', args=[slugify(self.name), self.id])
        else:
            raise Http404

    # def get_full_url(self, site):
    #     return '{host}{path}'.format(host=site.http_domain,
    #                                  path=self.get_absolute_url())

    def up(self):
        if self.status == DELETE:
            logger.info('Deleted items can not be raised. Lot %s', self.name)
            return False
        if self.status != ENABLED:
            self.status = ENABLED
            self.save()
            return True
        return False

    def down(self):
        if self.status == DELETE:
            logger.info('Supplies and so removed. Lot %s', self.name)
            return False
        logger.debug('Down lot %s %s', self.seller, self.pk)
        if self.status != DISABLED:
            self.status = DISABLED
            self.save()
            return True
        return False

    def fotos(self):
        """
        Фотки лота
        """
        if Image.objects.filter(lot=self).count() == 0:
            logger.info('lot not image, down %s', self.name)
            self.down()
            return []
        return Image.objects.filter(lot=self).order_by('pk')

    def main_foto(self):
        if Image.objects.filter(lot=self).count() == 0:
            logger.info('lot not image, down %s', self.name)
            self.down()
            # TODO: Возвращаем заглушку
            # return Image.objects.first()
            return None
        else:
            if Image.objects.filter(lot=self, is_main=True) == 0:
                logger.info('Main picture is not set')
                image = Image.objects.filter(lot=self).order_by('pk')[0]
                image.is_main = True
                image.save()
                return image
            return Image.objects.filter(lot=self, is_main=True)[0]

    def lot_delete(self):
        self.image_delete()
        self.status = DELETE
        self.save()

    def image_delete(self):
        for image in self.fotos():
            # Удаление изображений
            logger.debug('Remove image %s %s' % (self.seller, image.pk))
            try:
                image.image.delete()
                image.delete()
                self.image.delete()
            except:
                pass

    def __str__(self):
        return '%s - %s' % (self.pk, self.name)

    class Meta(object):
        unique_together = ("seller", "offer_id")


class Feed(CommonAbstractModel):
    xml_url = models.URLField('URL', max_length=1024,
                              help_text='адрес, по которому доступен файл прайслиста в формате YML')
    seller = models.ForeignKey(Seller, related_name='seller_feeds', verbose_name='продавец')
    run_auto = models.BooleanField(default=False)
    get_param_save = models.BooleanField(default=False,
                                         help_text='Сохранять GET параметры? Для Адмитада нет, для бабаду да.')
    md5_checksum = models.CharField('MD5', max_length=255,
                                    editable=False,
                                    default='',
                                    help_text=('контрольная сумма скачанного файла. Используется для '
                                               'того, чтобы не запускать обработку одинаковых прайслистов'))
    note = models.TextField(default="", blank=True)

    def __str__(self):
        return self.xml_url



def create_mysite(sender, instance, created, **kwargs):
    if created:
        template, create = Template.objects.get_or_create(folder=settings.DEFOLT_FOLDER,
                                                          defaults={'name': settings.DEFOLT_FOLDER})
        MySite.objects.update_or_create(site=instance,
                                        defaults={
                                            'template': template,
                                        })
        # logger.info('Новый сайт создан!  %s %s' % (sender.id, sender.domain))


post_save.connect(create_mysite, sender=Site)
