# -*- coding: utf-8 -*-
import logging

from celery.schedules import crontab
from celery.task import task, periodic_task
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from custom_profile.managers import UserTokenGenerator
from market.models import MySite, Seller, Feed
from market.utils import get_resolver_query_txt
from my_mail.managers import send_notification

logger = logging.getLogger(__name__)


def seller_add_mysite():
    mysites = MySite.objects.filter(sellers__isnull=False)
    for mysite in mysites:
        sellers = mysite.sellers.all()
        for seller in sellers:
            if mysite in seller.sites.all():
                print(1)

def validate_site_to_user(user_id, ):
    User.objects.get(pk=user_id)
    # get_resolver_query_txt()


@periodic_task(run_every=crontab(hour=1, minute=3))
def check_domain():
    mysites = MySite.objects.filter(manual_verification=False)
    for mysite in mysites:
        if settings.CELERY_START:
            validate_site.delay(mysite.site_id)
        else:
            validate_site(mysite.site_id)


@task()
def validate_site(site_id):
    utg = UserTokenGenerator()
    site = Site.objects.get(pk=site_id)
    logger.info('Начали валидацию сайта %s' % site)
    text_list = get_resolver_query_txt(site_id=site.pk)
    print(text_list)
    for token in text_list:
        if utg.valide_token(token):
            uidb64, hasht = utg.parse_token(token)
            user = utg.get_user(uidb64=uidb64)
            mysite = MySite.objects.get(site=site)
            if mysite.user != user:
                mysite.user = user
                mysite.save()
                logger.info('Права на сайт %s потверждены для пользователя %s', site, user)
                send_notification(sender='System',
                                  recipient=user,
                                  text='Права на сайт %s потверждены для пользователя %s' % (site, user),
                                  group_notifications=None)


@task()
def adm_campaign_to_seller():
    from my_admitad_api.models import AdmCampaign
    cams = AdmCampaign.objects.filter(categories=62, status='active')
    mysite1 = MySite.objects.get(pk=2)
    mysite2 = MySite.objects.get(pk=4)
    for cam in cams:
        print(cam)
        xml = cam.get_xml_feed()
        if xml:
            # cam.seller_set.update_or_create()
            seller, create = Seller.objects.update_or_create(
                url_home=cam.site_url,
                defaults={
                    'name': cam.name,
                    'adm_campaign': cam,
                    'xml_feed': xml
                })
    if settings.CELERY_START:
        seller_feed_to_feed.delay()
    else:
        seller_feed_to_feed()


@task()
def seller_feed_to_feed():
    sellers = Seller.objects.exclude(xml_feed=None)
    for seller in sellers:
        print(seller)
        feed, create = Feed.objects.update_or_create(
            seller=seller,
            defaults={
                'xml_url': seller.xml_feed,
                # 'run_auto': True
            })
        if create:
            feed.run_auto = True
            feed.save()
