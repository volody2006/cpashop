# -*- coding: utf-8 -*-
from market.models import MySite
from template_selection.managers import custom_get_current_site


def site(request):
    # print request.META['HTTP_HOST']
    site = custom_get_current_site(request)
    try:
        mysite = MySite.objects.get(site=site)
        verify_admitad = mysite.get_verify_admitad()
    except MySite.DoesNotExist:
        verify_admitad = False
    return {'SITE': site,
            'verify_admitad': verify_admitad}


def vendor(request):
    site = custom_get_current_site(request)
    return site
