# -*- coding: utf-8 -*-
import logging

from django import template
from django.contrib.sites.shortcuts import get_current_site

from market.models import Deeplink

logger = logging.getLogger(__name__)
register = template.Library()


@register.simple_tag(takes_context=True)
def deeplink_lot(context, lot=None):
    if lot is None or lot == '':
        logger.warning('Не передан лот в шаблоне')
        return '#'
    # site_id = context['site_id']
    try:
        site_id = get_current_site(context["request"]).id
        url = lot.get_deeplink(site_id=site_id)
    except (AttributeError) as e:
        logger.warning('Ошибка AttributeError, context=%s' % context)
        return '#'
    return url


@register.simple_tag(takes_context=True)
def get_seller_url(context, seller=None):
    if seller is None or seller == '':
        logger.warning('Не передан продавец в шаблоне.')
        return '#'
    try:
        site_id = get_current_site(context["request"]).id
        return seller.get_deeplink(site_id=site_id)
    except AttributeError:
        logger.warning('Ошибка AttributeError, context=%s' % context)
        return '#'


@register.simple_tag(takes_context=True)
def get_deeplink(context, seller=None, mysite=None):
    if seller is None or mysite is None:
        return False
    if Deeplink.objects.filter(seller=seller, site=mysite).exists():
        return Deeplink.objects.get(seller=seller, site=mysite)
    return False
