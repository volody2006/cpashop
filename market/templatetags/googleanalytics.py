from django import template
from django.conf import settings
from django.contrib.sites.models import Site
from django.template import loader

from market.models import MySite

register = template.Library()


@register.tag
def analytics(parser, token):
    return AnalyticsNode()


class AnalyticsNode(template.Node):
    def render(self, context):
        request = context['request']
        try:
            current_site = Site.objects.get_current(request)
        except Site.DoesNotExist:
            return ''
        # current_site = 1
        # print request
        # print current_site
        try:
            web_property_id = MySite.objects.get(site=current_site).web_property_id
            if web_property_id is None:
                return ''
            t = loader.get_template('googleanalytics/analytics.html')
            c = {'web_property_id': web_property_id, }
            return t.render(c)
        except MySite.DoesNotExist:
            return ''

    def get_current_site(self, request):
        if settings.SITE_BY_REQUEST:
            return Site.objects.get(domain=request.get_host())
        else:
            return Site.objects.get_current()
