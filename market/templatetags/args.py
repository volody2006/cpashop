# -*- encoding: utf-8 -*-
from django.template import Library
from django.utils.datastructures import MultiValueDict
from django.utils.http import urlencode as django_urlencode
from django.utils.http import urlquote as django_urlquote

register = Library()


@register.simple_tag
def args(vars, var, value):
    vars = vars.copy()

    # Переменную с номером страницы нелогично передать
    try:
        del vars['page']
    except KeyError:
        pass

    if isinstance(var, list):
        i = 0
        for v in var:
            if isinstance(value, list):
                value_for = value[i]
                i += 1
            else:
                value_for = value
            if value_for:
                vars[v] = value_for
            else:
                try:
                    del vars[v]
                except KeyError:
                    pass
    else:
        if value:
            vars[var] = value
        else:
            del vars[var]

    return vars.urlencode()


# //оставить только vars_leave
@register.simple_tag
def args_leave(vars, vars_leave):
    vars = vars.copy()
    vars_exit = vars.copy()

    # Переменную с номером страницы нелогично передать
    try:
        del vars['page']
    except KeyError:
        pass

    vars_leave = vars_leave.split(',')
    for var in vars:
        if var not in vars_leave:
            del vars_exit[var]

    return vars_exit.urlencode()


@register.simple_tag
def multi_args(vars, var, value):
    vars = vars.copy()

    # Переменную с номером страницы нелогично передать
    try:
        del vars['page']
    except KeyError:
        pass

    vars.appendlist(var, value)

    unique = []
    for item in vars.getlist(var):
        if item not in unique:
            unique.append(item)
    vars.setlist(var, unique)

    return urlquote(vars)


@register.simple_tag
def arg_omit(vars, omit_vars):
    vars = vars.copy()

    for var in omit_vars.split(','):

        try:
            # Переменную с номером страницы нелогично передать
            del vars['page']
        except KeyError:
            pass

        try:
            del vars[var]
        except:
            pass

    return vars.urlencode()


@register.simple_tag
def allargs(vars):
    vars = vars.copy()

    try:
        # Переменную с номером страницы нелогично передать
        del vars['page']
    except KeyError:
        pass

    if vars:
        return '?' + vars.urlencode()
    else:
        return ''


def urlquote(link=None, get={}):
    '''
    This method does both: urlquote() and urlencode()

    urlqoute(): Quote special characters in 'link'

    urlencode(): Map dictionary to query string key=value&...

    HTML escaping is not done.

    Example:

      urlquote('/wiki/Python_(programming_language)')     --> '/wiki/Python_%28programming_language%29'
      urlquote('/mypath/', {'key': 'value'})              --> '/mypath/?key=value'
      urlquote('/mypath/', {'key': ['value1', 'value2']}) --> '/mypath/?key=value1&key=value2'
      urlquote({'key': ['value1', 'value2']})             --> 'key=value1&key=value2'
    '''
    assert link or get
    if isinstance(link, dict):
        # urlqoute({'key': 'value', 'key2': 'value2'}) --> key=value&key2=value2
        assert not get, get
        get = link
        link = ''
    assert isinstance(get, dict), 'wrong type "%s", dict required' % type(get)
    assert not (link.startswith('http://') or link.startswith('https://')), \
        'This method should only quote the url path. It should not start with http(s)://  (%s)' % (
            link)
    if get:
        # http://code.djangoproject.com/ticket/9089
        if isinstance(get, MultiValueDict):
            get = get.lists()
        if link:
            link = '%s?' % django_urlquote(link)
        return '%s%s' % (link, django_urlencode(get, doseq=True))
    else:
        return django_urlquote(link)
