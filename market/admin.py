from django.contrib import admin
from mptt.admin import MPTTModelAdmin

# Register your models here.
from market.models import Feed, CpaSite, Seller, Category, Lot, Param, Image, MySite, Deeplink, Template, Currency


class FeedAdmin(admin.ModelAdmin):
    list_display = ('seller', 'id', 'run_auto', 'xml_url')
    list_filter = ['run_auto', ]
    search_fields = ['seller__name', ]
    # raw_id_fields = ['user']
    # search_fields = ['yml_url', 'user__username', 'user__shop_slug', 'user__name_shop']
    # list_display = ('id', 'shop_link', 'yml_url', 'pretty_message','run_auto', 'controls',
    #                 'status_display', 'type_display', 'progress_display', 'pretty_message',
    #                 'running_display',
    #                 'date_changed'
    #                 )
    pass


admin.site.register(Feed, FeedAdmin)


class CpaSiteAdmin(admin.ModelAdmin):
    pass


admin.site.register(CpaSite, CpaSiteAdmin)


class SellerAdmin(admin.ModelAdmin):
    raw_id_fields = ['adm_campaign', ]
    list_display = ['name', 'id', 'load_image']
    list_filter = ['load_image', ]
    search_fields = ['name', 'description', 'xml_feed', 'url_home', ]


admin.site.register(Seller, SellerAdmin)


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('iso_code', 'short_name')


admin.site.register(Currency, CurrencyAdmin)


class CategoryAdmin(MPTTModelAdmin):
    list_display = ('pk', 'name')
    search_fields = ['name', ]
    raw_id_fields = ['parent', ]

    def get_queryset(self, request):
        return super().get_queryset(request).only('pk', 'level', 'name')


admin.site.register(Category, CategoryAdmin)


class LotAdmin(admin.ModelAdmin):
    raw_id_fields = ['category', 'seller', 'lot_params']
    list_display = ('pk', 'offer_id', 'name', 'seller', 'category', 'price', 'old_price', 'status')
    list_filter = ['status', ]
    search_fields = ['name', 'offer_id', ]


admin.site.register(Lot, LotAdmin)


class ParamAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'unit')


admin.site.register(Param, ParamAdmin)


class ImageAdmin(admin.ModelAdmin):
    raw_id_fields = ['lot', ]
    list_display = ('pk', 'image', 'lot', 'is_main')
    list_filter = ('is_main',)


admin.site.register(Image, ImageAdmin)


class MySiteAdmin(admin.ModelAdmin):
    search_fields = ['site__domain', ]
    list_display = ('domain', 'pk')
    list_filter = ('is_https',)
    raw_id_fields = ['user', ]


admin.site.register(MySite, MySiteAdmin)


class DeeplinkAdmin(admin.ModelAdmin):
    list_display = ('pk', 'seller', 'site', 'url')
    # list_filter = ('is_main',)


admin.site.register(Deeplink, DeeplinkAdmin)


class TemplateAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'folder')

    # list_filter = ('is_main',)


admin.site.register(Template, TemplateAdmin)
