# -*- coding: utf-8 -*-
import logging

from django.contrib.sites.models import Site
from django.views.generic import DetailView, ListView
from haystack.backends import SQ
from haystack.query import SearchQuerySet

from market.mixpanel.utils import GetTemplateNamesMixin, SlugMixin, CRMMixin, QuerySetFacetMixin
from market.models import Lot, Category
from market.utils import get_query_for_object
from template_selection.managers import custom_get_current_site

logger = logging.getLogger(__name__)


class MainPageView(GetTemplateNamesMixin, CRMMixin, QuerySetFacetMixin, ListView):
    """
    главная
    """
    template = 'index.html'
    model = Lot
    paginate_by = 12

    # queryset = Lot.objects.filter(status=ENABLED)

    def get_context_data(self, **kwargs):
        context = super(MainPageView, self).get_context_data(**kwargs)
        site = custom_get_current_site(self.request)
        try:
            new_lots = self.get_queryset()[:self.get_paginate_by(self.get_queryset())]
            logger.info('new_lots: %s', len(new_lots))

            sale = SearchQuerySet().filter(
                sites=site.domain).order_by('-amount_discount')[:100]

            logger.info('sale: %s', len(sale))

        except (Site.DoesNotExist) as e:
            logger.error('Error %s', e)
            new_lots = []
            sale = new_lots

        context.update({
            'new_lots': new_lots,
            'sale': sale[:12],
            "special": sale[12:15]
        })
        return context

    # def get_queryset(self):
    #     q_object = SQ(**self.get_queryset_kwargs())
    #
    #     if self.get_queryset_theme():
    #         q_object.add(self.get_queryset_theme(), SQ.AND)
    #     lots = SearchQuerySet().filter(q_object).order_by(*self.get_order_by())
    #     return lots

    # def get_site(self):
    #     return custom_get_current_site(self.request)
    #
    # def get_mysite(self):
    #     return self.get_site().mysite
    #
    # def get_queryset_kwargs(self, **kwargs):
    #     kwargs.update(sites=self.get_site().domain)
    #     return kwargs
    #
    # def get_queryset_theme(self):
    #     if self.get_mysite().query is not None:
    #         return self.get_mysite().parse_query()
    #     return None

    def get_order_by(self):
        return []


class LotDetailView(GetTemplateNamesMixin, SlugMixin, CRMMixin, DetailView):
    template = 'single.html'
    model = Lot

    # queryset = Lot.objects.all()

    def get_context_data(self, **kwargs):
        context = super(LotDetailView, self).get_context_data(**kwargs)
        context['cpa_url'] = context['object'].get_deeplink(site_id=custom_get_current_site(self.request).id)
        context['category'] = self.object.category
        context['site_id'] = custom_get_current_site(self.request).id
        related = self.get_related()
        context['related_lots'] = related[:8]
        context['related_more_lots'] = related[8:16]
        # context['gfu'] = self.object.get_full_url(custom_get_current_site(self.request).mysite)

        return context

    def get_related(self):
        # Поиск похожих товаров, запрос ограничиваем именим товара.
        q_object = SQ()
        q_object.add(SQ(seller__in=self.get_sellers()), SQ.OR)
        # sqs = SearchQuerySet().filter(q_object)
        if self.get_queryset_theme():
            q_object.add(SQ(self.get_queryset_theme()), SQ.AND)
            # sqs = sqs.filter_and(SQ(**self.get_queryset_theme()))
        lot = self.get_object()
        self.lot_name = lot.name.lower()
        # q_object = SQ()
        q_object.add(get_query_for_object(lot), SQ.OR)
        sqs = SearchQuerySet().filter(q_object)[:32]
        # sqs = sqs.filter(q_object)[:100]
        # random.seed(os.urandom(20))
        # random.shuffle(sqs)
        return sqs


    def get_queryset_theme(self):
        if custom_get_current_site(self.request).mysite.query is not None:
            return custom_get_current_site(self.request).mysite.parse_query()
        return None

    def get_sellers(self):
        return custom_get_current_site(self.request).mysite.sellers.all()


class LotBuyView(GetTemplateNamesMixin, SlugMixin, CRMMixin, QuerySetFacetMixin, ListView):
    template = 'category.html'
    model = Lot

    def get_queryset(self):
        queryset = super(LotBuyView, self).get_queryset()
        lot = self._obj
        self.lot_name = lot.name.lower()
        q_object = SQ()
        q_object.add(get_query_for_object(lot), SQ.AND)
        name_query = SQ()
        for cat in lot.category_get_ancestors():
            name_query.add(SQ(text__contains=cat), SQ.OR)
        try:
            q_object.add(SQ(text__contains=lot.category.name), SQ.AND)
        # q_object.add(name_query, SQ.AND)
        except AttributeError:
            q_object.add(SQ(text__contains=lot.name), SQ.AND)
        queryset = queryset.filter(q_object)
        # Эластик больше 10000 отдавать не хочет, ограничим queryset
        return queryset[:10000]

    def get_context_data(self, **kwargs):
        context = super(LotBuyView, self).get_context_data(**kwargs)
        context['query'] = self._obj.name.lower()
        context['gfu'] = self._obj.get_full_url(custom_get_current_site(self.request).mysite)
        return context


class CategoryList(GetTemplateNamesMixin, SlugMixin, CRMMixin, QuerySetFacetMixin, ListView):
    template = 'category.html'
    model = Category
    root_category = None

    def get_queryset_kwargs(self, **kwargs):
        kwargs = super(CategoryList, self).get_queryset_kwargs()
        kwargs.update(categories=self.get_obj().pk)
        # print(kwargs)
        return kwargs

    def get_order_by(self):
        return []

    def get_context_data(self, **kwargs):
        context = super(CategoryList, self).get_context_data(**kwargs)
        # print(self.get_facet_counts('vendor').get('fields').get('vendor'))
        context.update({'param_filters': self.get_param_filters(),
                        'vendor': self.get_facet_counts('vendor').get('fields').get('vendor'),
                        })
        if self.root_category:
            context['category'] = self.root_category
        else:
            context['category'] = self._obj

        getvars = self.request.GET.copy()
        if 'page' in getvars:
            del getvars['page']
        if len(list(getvars.keys())) > 0:
            context['getvars'] = "&%s" % getvars.urlencode()
        else:
            context['getvars'] = ''

        if self.request.GET.get('price_from'):
            try:
                context['price_from'] = int(self.request.GET.get('price_from'))
            except ValueError:
                pass
        if self.request.GET.get('price_to'):
            try:
                context['price_to'] = int(self.request.GET.get('price_to'))
            except ValueError:
                pass
        if self.request.GET.get('vendor'):
            try:
                context['is_vendor'] = self.request.GET.get('vendor')
            except ValueError:
                pass

        context['gfu'] = self._obj.get_full_url(custom_get_current_site(self.request).mysite)
        return context


