# -*- coding: utf-8 -*-
"""cpashop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from market import views

urlpatterns = [
    url(r'^$', views.MainPageView.as_view(), name='main_view'),
    url(r'^lot/(?P<pk>[0-9]+)/$', views.LotDetailView.as_view(), name='lot_view'),
    url(r'^lot/(?P<slug>[-\w\d]+)-(?P<pk>[0-9]+)/$', views.LotDetailView.as_view(), name='slug_lot_view'),
    url(r'^lot/(?P<pk>[0-9]+)/buy$', views.LotBuyView.as_view(), name='lot_buy'),
    url(r'^category/(?P<pk>[0-9]+)/$', views.CategoryList.as_view(), name='category-view'),
    url(r'^(?P<slug>[-\w\d]+)-(?P<pk>\d+)/$', views.CategoryList.as_view(), name="slug-category-view"),
    url(r'^(?P<slug>[-\w\d]+)-(?P<pk>\d+)a/$', views.LotBuyView.as_view(), name="arche-view"),
]
