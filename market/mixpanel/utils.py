# coding: utf-8
import logging

from django.http import Http404, QueryDict
from django.shortcuts import get_object_or_404, redirect
from haystack.backends import SQ
from haystack.query import SearchQuerySet

from template_selection.managers import get_custom_template_name, get_custom_template, custom_get_current_site

logger = logging.getLogger(__name__)


class QuerySetFacetMixin:

    def get_param_filters(self):
        """
        возвращает словарь со всеми параметрами гет-запроса
        убираем пустые, пропускаем по white-листу
        """
        param_filters = self.request.GET

        allowed_params = ['price_from', 'price_to', 'o', 'sf', 'seller', 'q', 'theme', 'tag', 'f', 'vendor']
        filtered_params = QueryDict(u'').copy()
        for k in param_filters.keys():
            if k in allowed_params or k.startswith('attr_'):
                if param_filters[k]:
                    filtered_params.setlist(k, param_filters.getlist(k))
        return filtered_params

    def get_filter_qs(self):
        return self.get_param_filters().dict()


    def get_queryset(self):
        q_filter = {}
        # print(self.get_param_filters())
        # q_filter.update(self.get_param_filters().items())


        if self.request.GET.get('price_from'):
            try:
                q_filter.update({'price__gte': int(self.request.GET.get('price_from'))})
            except ValueError:
                pass
        if self.request.GET.get('price_to'):
            try:
                q_filter.update({'price__lte': int(self.request.GET.get('price_to'))})
            except ValueError:
                pass
        if self.request.GET.get('price_exact'):
            try:
                q_filter.update({'price__exact': int(self.request.GET.get('price_exact'))})
            except ValueError:
                pass
        # if self.get_param_filters.get('vendor'):
        #     try:
        #         q_filter.update({'vendor': str(self.request.GET.get('vendor'))})
        #     except ValueError:
        #         pass

        usr = 'price'
        if self.request.GET.get('order'):
            order = self.request.GET.get('order')
            usr = 'price'

            if order == 'pa' or order == 'price':
                usr = 'price'
            elif order == 'pd':
                usr = '-price'
            elif order == 'sd':
                usr = '-date_created'
            elif order == 'ea':
                usr = 'date_created'
            elif order == 'dcd':
                usr = '-deal_count'
            elif order == 'r':
                usr = '-rank'

        q_object = SQ()
        q_object.add(SQ(seller__in = self.get_sellers()), SQ.OR)
        sqs = SearchQuerySet().filter(q_object).order_by(usr)
        if self.get_queryset_theme():
            sqs = sqs.filter(self.get_queryset_theme())

        if self.get_queryset_kwargs():
            sqs = sqs.filter(SQ(**self.get_queryset_kwargs()))

        # Фасеты считаем после фильтрам по темам и кваркам(категориям), но до фильтров

        self._facet_counts = sqs.facet('vendor').facet_counts()
        if self.get_filter_qs():
            sqs = sqs.filter(SQ(**dict(self.get_filter_qs())))
        if q_filter:
            sqs = sqs.filter(SQ(**q_filter))

        logger.info('sqs.count(): %s', sqs.count())
        return sqs

    def get_queryset_theme(self):
        if self.get_mysite().query is not None:
            return self.get_mysite().parse_query()
        return None

    def get_facet_counts(self, facet):
        if not hasattr(self, '_facet_counts'):
            self._facet_counts = dict(self.get_queryset().facet(facet).facet_counts())
        return self._facet_counts

    def get_site(self):
        return custom_get_current_site(self.request)

    def get_mysite(self):
        return self.get_site().mysite

    def get_queryset_kwargs(self, **kwargs):
        return kwargs

    def get_sellers(self):
        return self.get_mysite().sellers.all()


class GetObjectUserMixin:
    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(GetObjectUserMixin, self).get_object()
        if not obj.user == self.request.user:
            if self.request.user.is_superuser:
                return obj
            logger.info('User %s не имеет права удалять Object %s, пользователя %s', self.request.user, obj, obj.user)
            # TODO: Скорее всего 403, чем 404. Все таки доступ запрещен.
            raise Http404

        return obj


class GetTemplateNamesMixin:
    """
    Выбор шаблона, подставляем шаблон взависимости от сайта
    """

    def get_template_names(self):

        if self.template is None:
            names = super(GetTemplateNamesMixin, self).get_template_names()
        else:
            self.template_name = get_custom_template_name(self.request, self.template)
            names = []
            names.append(self.template_name)
        return names

    def get_paginate_by(self, queryset):
        template_obj = get_custom_template(self.request)
        if self.paginate_by:
            return self.paginate_by
        try:
            if template_obj.paginate_by:
                if template_obj.paginate_by > 0:
                    self.paginate_by = template_obj.paginate_by
                    return self.paginate_by
        except AttributeError:
            return self.paginate_by


class SlugMixin:
    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.args = args
        self.kwargs = kwargs

        odj = self.get_obj()

        if odj.web_slug:
            # ЧПУ обязательно нужен! Если он у нас есть, а пришел запрос без ЧПУ
            # то редиректим на правильный урл
            if 'slug' not in self.kwargs:
                logger.debug('if slug not in self.kwargs')
                return redirect(odj, permanent=True)

            # визит на неправильный ЧПУ редиректим на правильный ЧПУ
            if self.kwargs['slug'] != odj.web_slug:
                logger.debug('if self.kwargs[slug] != odj.web_slug')
                return redirect(odj, permanent=True)
        else:
            # категория без чпу открывается только без ЧПУ
            if 'slug' in self.kwargs:
                logger.debug('if slug in self.kwargs:')
                return redirect(odj, permanent=True)

        return super(SlugMixin, self).dispatch(request, *args, **kwargs)

    def get_obj(self):
        self._obj = get_object_or_404(self.model, pk=self.kwargs['pk'])
        return self._obj


class CRMMixin:
    """
    Если сайт относится к CRM сайту системы, редирект на главную страницу нехер там показывать товары и каталоги,
    для этого есть демо сайт

    Микс ставииться на обычные вьюхи
    """

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        site = custom_get_current_site(self.request)
        if site.mysite.is_crm:
            logger.info('Это CRM сайт, перенаправляем на основную страницу')
            from cpashop.settings import CRM_URL
            return redirect(CRM_URL)
        # return Http404
        return super(CRMMixin, self).dispatch(request, *args, **kwargs)


class NotCRMMixin:
    '''
    Если это не crm сайт, что ему тут делать? Показываем 404
    Микс ставиться на вьюхи CRM
    '''

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        site = custom_get_current_site(self.request)
        if not site.mysite.is_crm:
            logger.info('Это не CRM сайт, 404')
            raise Http404
        return super(NotCRMMixin, self).dispatch(request, *args, **kwargs)
