#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
import sys
import os
import argparse
import subprocess
import signal
import fcntl


def log(message):
    """
    Выводит в STDOUT сообщение `message` и принудительно очищает буфер
    """
    sys.stdout.write(message + "\n")
    sys.stdout.flush()


def clear_screen():
    """
    Очищает экран.

    Используется внешний вызов к утилите очистки терминала (reset в
    linux-системах, cls в windows) вместо стандартной библиотеки
    curses.

        >>> import curses
        >>> curses.initscr().clear()

    Встроенный вариант работает с ошибками (потеря LF), поэтому
    не используется

    """
    os.system('reset' if os.name != 'nt' else 'cls')


def virtualenv_found():
    """
    Возвращает True, если скрипт был запущен либо в активированном виртуальном
    окружении, либо не "системным" интерпретатором python.

    На текущий момент системными интерпретаторами считаются те, которые
    располагаются по путям:

        /usr/bin/python          # Харакетерно для Linux-систем
        /usr/local/bin/python    # Характерно для FreeBSD
        C:/Python                # для Windows-систем

    """
    system_pythons = (
        '/usr/bin/python',
        '/usr/bin/python',
        '/usr/local/bin/python',
        'c:/python26',
        'c:/python27',
        'c:/python3',
    )
    return 'VIRTUAL_ENV' in os.environ or not sys.executable.lower().startswith(system_pythons)


def manage_py(command, settings='cpashop.settings', trace=False):
    """
    Выполняет системный запуск management-команды django

    Пример выполнения команды ./manage py validate:

        >>> manage_py('validate')

    В качестве необязательного аргумента можно указать
    модуль с настройками (аналог ключа --settings=settings.module):

        >>> manage_py('validate', settings='local_settings')

    Функция возвращает объект subprocess.Popen; работа с потоками
    внутри не производится.

    """
    command = ('{python} ./manage.py {command} '
               '--settings={settings} '
               ).format(python=sys.executable,
                        command=command,
                        settings=settings)
    if trace:
        command += '--trace'

    if command not in ran_processes:
        proc = subprocess.Popen(command, shell=True,
                                close_fds=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)

        # Установка системных дескрипторов в неблокирующий режим
        for fd in ['stdout', 'stderr']:
            flags = fcntl.fcntl(getattr(proc, fd), fcntl.F_GETFL)
            fcntl.fcntl(getattr(proc, fd), fcntl.F_SETFL, flags | os.O_NONBLOCK)

        ran_processes[command] = proc
        log("Run %s with PID=%s" % (command, proc.pid))

    return ran_processes[command]


def main():
    """
    Запуск всех сервер
    """
    if not virtualenv_found():
        log('It seems you\'re tried this script outside of virtualenv?')
        sys.exit(1)

    parser = argparse.ArgumentParser(description='Django development toolkit')
    parser.add_argument('--run-all', action='store_true', default=True)
    parser.add_argument('--reset-screen', action='store_true', default=False)
    parser.add_argument('--settings', action='store', default='settings', metavar='settings.module')
    parser.add_argument('--with-celery', action='store_true', default=False)

    args = parser.parse_args(sys.argv[1:])

    if args.run_all:
        if args.reset_screen:
            clear_screen()
        processes = {
            'cpashop': manage_py('runserver 0.0.0.0:8000', settings='cpashop.settings', trace=True),
        }

        try:
            while True:
                for name, proc in processes.items():
                    try:
                        try:
                            line = os.read(proc.stdout.fileno(), 1024)
                        except OSError:
                            try:
                                line = os.read(proc.stderr.fileno(), 1024)
                            except OSError:
                                continue
                    except (UnicodeDecodeError, UnicodeEncodeError) as e:
                        log('[%s] %s: %s' % (name, e.__class__.__name__, e))
                        continue

                    if not line and proc.poll() is not None:
                        log("[%s] process is stopping. Don't serve it anymore" % name)
                        processes.pop(name, None)
                        continue

                    if line:
                        try:
                            log('[%s] %s' % (name, line.strip()))
                        except UnicodeDecodeError:
                            log("UnicodeDecodeError")

                if not processes:
                    log('All processes are shut down')
                    break
        except KeyboardInterrupt:
            for name, proc in processes.items():
                log("Stopping %s [PID=%s]" % (name, proc.pid))
                try:
                    os.killpg(proc.pid, signal.SIGTERM)
                except OSError:
                    pass


ran_processes = {}

if __name__ == '__main__':
    main()
