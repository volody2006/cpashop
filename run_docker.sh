#!/usr/bin/env bash

### START LOCAL ###
#postgres latest
docker run -d -e PGDATA=/var/lib/postgresql/data/pgdata -e POSTGRES_PASSWORD=postgres -v /home/user/program/docker/postgres_9_6/pgdata:/var/lib/postgresql/data/pgdata -p 5432:5432 postgres:9.6

#elasticsearch
docker run -d -p 9200:9200 -p 9300:9300 -v /home/user/program/docker/elasticsearch_2_4_2/esdata/esdata:/usr/share/elasticsearch/data -v /home/user/program/docker/elasticsearch_2_4_2/plugins/:/usr/share/elasticsearch/plugins elasticsearch:2.4.2
# ERROR: max virtual memory areas vm.max_map_count [65530] likely too low, increase to at least [262144]
# sudo sysctl -w vm.max_map_count=262144


# rabbitmq
docker run -d --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management

#redis
docker run -d -p 6379:6379 redis
