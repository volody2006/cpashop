# -*- coding: utf-8 -*-


from django.db import models
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
from base.models import CommonAbstractModel


@python_2_unicode_compatible
class UserEmail(CommonAbstractModel):
    email = models.EmailField()

    def __str__(self):
        return self.email
