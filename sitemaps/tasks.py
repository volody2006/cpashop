# -*- coding: utf-8 -*-
from datetime import timedelta

from celery.task import PeriodicTask

from sitemaps.management.commands.generate_sitemaps import Command

__author__ = 'xaralis'


# Create class conditionally so the task can be bypassed when repetition
# is set to something which evaluates to False.

class GenerateSitemap(PeriodicTask):
    run_every = timedelta(hours=23)

    def run(self, **kwargs):
        generator = Command()
        generator.handle()
