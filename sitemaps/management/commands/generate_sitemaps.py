# coding: utf-8
import logging
import os
from collections import OrderedDict

# from django.core.urlresolvers import reverse
from django.core.management import call_command
from django.core.management.base import BaseCommand

# from django.utils.timezone import now
from market.models import Lot, ENABLED
from sitemaps.utils import get_sitemap_directory, queryset_iterator

logger = logging.getLogger('generate_sitemaps')


class Command(BaseCommand):
    # Максимально допустимое количество урлов в одном сайтмапе
    MAX_URLS_IN_SET = (50 * 1000) - 10

    # Максимально допустимый размер файла сайтмапа
    MAX_FILE_SIZE = (50 * 1024 * 1024) - 300
    SITEMAP_PROLOGUE = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'

    def __init__(self):
        super(Command, self).__init__()
        self.opened_handlers = OrderedDict()
        self.counters = OrderedDict()
        self.files = OrderedDict()
        self.num = 0
        self.fp_name = OrderedDict()

    def get_output_directory(self, site):
        """
        Возвращает путь к директории, в которой будут сохраняться сгенерированные сайтмапы
        """
        return get_sitemap_directory(site)

    def get_output_filename(self, site, suffix):
        """
        Возвращает имя файла, в который будет производиться запись
        """
        directory = self.get_output_directory(site)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return os.path.join(directory, '%s.xml' % suffix)

    def _open(self, site, suffix):
        path = self.get_output_filename(site, suffix)
        logger.debug('Opening new file %s' % path)
        fp = open(path + '.tmp', 'w')
        fp.write('<?xml version="1.0" encoding="UTF-8"?>%s' % self.SITEMAP_PROLOGUE)
        self.files[site.name].append(path)
        self.fp_name[fp] = path
        return fp

    def _close(self, fp):
        filename = self.fp_name[fp]

        fp.write('</urlset>')
        fp.close()

        os.rename(filename + '.tmp', filename)

    def _rotate(self, site):
        # Закрыть старый дескриптор
        c = site.name
        fp = self.opened_handlers[c]
        self._close(fp)

        # Открыть новый, увеличив счётчик на единицу
        self.counters[c]['num'] += 1
        self.counters[c]['urls'] = 0
        self.counters[c]['bytes'] = 0
        self.opened_handlers[c] = self._open(site, self.counters[c]['num'])

    def create_file_if_not_exist(self, site):
        """
        Создаёт файл для города, если его (файла) раньше не существовало
        """
        c = site.name
        if c not in self.opened_handlers:
            logger.debug('No file for %s site. Create one' % c)
            self.counters[c] = {'urls': 0, 'bytes': 0, 'num': 1, 'urls_total': 0}
            self.files[c] = []
            self.opened_handlers[c] = self._open(site, self.counters[c]['num'])

    def rotate_file_if_need(self, site):
        if self.limit_exceed(site):
            logger.debug('Need rotate for %s site. Close old file' % site.name)
            self._rotate(site)

    def update_counters(self, site, url):
        self.num += 1
        self.counters[site.name]['urls'] += 1
        self.counters[site.name]['urls_total'] += 1
        self.counters[site.name]['bytes'] += len(url)

    def limit_exceed(self, site):
        """
        Возвращает True, если текущий размер файла либо количество добавленных URL приблизились
        к максимально допустимому  значению
        """
        stats = self.counters[site.name]
        return not (not (stats['urls'] >= self.MAX_URLS_IN_SET) and not (stats['bytes'] >= self.MAX_FILE_SIZE))

    def get_queryset(self):
        """
        Возвращает кверисет всех товаров, которые
        """
        # return Lot.objects_all\
        #     .select_related('site__city_en')\
        #     .only('id', 'trade_type', 'date_changed',
        #           'site__city_en', )
        return Lot.objects.filter(status=ENABLED)

    def get_fp_for_site(self, site):
        """
        Возвращает файловый дескриптор для записи YML
        """
        self.create_file_if_not_exist(site)
        self.rotate_file_if_need(site)
        return self.opened_handlers[site.name]

    def close_handlers(self):
        for name_en, fp in list(self.opened_handlers.items()):
            self.stdout.write("Closing file descriptor for %s... " % name_en)
            try:
                self._close(fp)
                self.stdout.write("Done!")
            except (OSError, IOError) as e:
                self.stdout.write("failed: %s" % e)
            self.stdout.write("\n")
            self.stdout.flush()

    def get_priority(self, lot):
        """
        Возвращает значение priority для текущего товара
        """
        return 0.8

    def generate_row(self, obj, site):
        return ''.join(['<url>'
                        '<loc>%s</loc>' % obj.get_full_url(site),
                        '<lastmod>%s</lastmod>' % obj.date_changed.date(),
                        '<changefreq>weekly</changefreq>',
                        '<priority>%s</priority>' % self.get_priority(obj),
                        '</url>'])

    def handle(self, **options):
        # Категории
        # for dep in site.objects.all():
        #     for c in Category.objects.all():
        #         # Если нечто ходит как утка и крякает как утка...
        #         for m in ['category-view', 'echo_list_category']:
        #             c.get_full_url = lambda: dep.http_domain + reverse(m, args=[c.id])
        #             c.date_changed = now()
        #             c.site = dep
        #             self.write_url(c)
        #
        # # Пользователи
        # for user in User.objects.filter(is_shop=True):
        #     self.write_url(user)
        #
        # # Флетпейджи
        # for flatpage in DepFlatPage.objects.filter(date_publication__isnull=False):
        #     for dep in flatpage.sites.all():
        #         flatpage.site = dep
        #         self.write_url(flatpage)
        #
        # # Форум
        # for topic in queryset_iterator(ForumTopic.objects.only()):
        #     self.write_url(topic)

        # Лоты
        for lot in queryset_iterator(self.get_queryset()):
            self.write_url(lot)

        self.close_handlers()
        call_command('generate_sitemap_indexes')

    def write_url(self, obj):

        for site in obj.get_sites():
            url = self.generate_row(obj, site)
            fp = self.get_fp_for_site(site)
            fp.write(url)

            # Обновляем счётчики
            self.update_counters(site, url)

            if self.num and not (self.num % 1000):
                self.stdout.write("%d entries handled\n" % self.num)
                self.stdout.flush()
