# coding: utf-8
import glob
import logging
import os
import shutil

from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand
from django.utils.timezone import now

from market.models import MySite
from sitemaps.utils import get_sitemap_root, get_sitemap_chapter_url, natural_sort

logger = logging.getLogger('generate_sitemap_indexes')


class Command(BaseCommand):
    SITEMAPINDEX_PROLOGUE = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'

    def handle(self, **opts):
        for dep_directory in glob.glob(get_sitemap_root() + '/*'):
            files = glob.glob(dep_directory + '/*')
            files = natural_sort(files)

            sitemap_index_filename = os.path.join(dep_directory, 'sitemap.xml')

            name = os.path.basename(dep_directory)
            try:
                site = MySite.objects.get(site=Site.objects.get(name=name))
            except Site.DoesNotExist:
                # Есть директория, но нет сайта в базе. Удаляем.
                logger.info('Сайт %s не найден в базе, директория %s ', name, dep_directory)
                shutil.rmtree(dep_directory)
                logger.info('Удалили директорию %s' % dep_directory)
                continue

            # Директория пустая. Это что-то нездоровое. Игнорируем
            if not files:
                logger.debug('Директория пустая. Это что-то нездоровое. Игнорируем %s.' % name)
                continue

            # Если у города только один файл сайтмапа, переименовываем его в sitemap.xml
            if len(files) == 1:
                filename = files[0]
                os.rename(filename, sitemap_index_filename)
                logger.debug('Rename %s into sitemap.xml for %s' % (filename, name))
                continue

            # Если у города больше одного файла сайтмапа, нужно сгенерировать индекс
            self.stdout.write("Creating index file for sitemaps %s" % name)

            with open(sitemap_index_filename, 'w') as fp:
                fp.write(self.SITEMAPINDEX_PROLOGUE)
                for f in files:
                    filename = os.path.basename(f)

                    if filename.lower() == 'sitemap.xml':
                        continue

                    url = get_sitemap_chapter_url(site, filename)
                    fp.write(''.join(['<sitemap>',
                                      '<loc>%s</loc>' % url,
                                      '<lastmod>%s</lastmod>' % now().date(),
                                      '</sitemap>']))
                    self.stdout.write("Add %s into %s\n" % (url, name))
                fp.write('</sitemapindex>')

# os.rename(file, os.path.join(os.path.dirname(file), 'sitemap.xml'))
