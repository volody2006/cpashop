# coding: utf-8


from django.conf.urls import url
from sitemaps.views import root, content

urlpatterns = [
    url('^sitemap\.xml$', root, name='sitemap_root'),
    url('^sitemap\.xml$', root, name='django.contrib.sitemaps.views.sitemap'),
    url('^sitemap(?P<suffix>.*?)\.xml$', content, name='sitemap_section'),
]
