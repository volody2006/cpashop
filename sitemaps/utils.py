# coding: utf-8
import gc
import os
import re

from django.conf import settings
from django.core.urlresolvers import reverse

DIRECTORY = 'sitemaps'


def get_sitemap_chapter_url(site, filename):
    return site.http_domain + reverse('sitemap_section', args=[int(re.compile('\D+').sub('', filename))])


def get_sitemap_url():
    return os.path.join(settings.MEDIA_URL, DIRECTORY)


def get_sitemap_url_for_site(site):
    return os.path.join(settings.MEDIA_URL, DIRECTORY, site.name)


def get_sitemap_root():
    return os.path.join(settings.MEDIA_ROOT, DIRECTORY)


def get_sitemap_directory(site):
    return os.path.join(get_sitemap_root(), site.name)


def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def queryset_iterator(queryset, chunksize=1000):
    '''''
    Iterate over a Django Queryset ordered by the primary key

    This method loads a maximum of chunksize (default: 1000) rows in it's
    memory at the same time while django normally would load all rows in it's
    memory. Using the iterator() method only causes it to not preload all the
    classes.

    Note that the implementation of the iterator does not support ordered query sets.
    '''
    pk = 0
    try:
        last_pk = queryset.order_by('-pk')[0].pk
    except IndexError:
        last_pk = 0
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            yield row
        gc.collect()
