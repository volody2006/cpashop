# coding: utf-8
from django.contrib import admin

from sitemaps.models import SitemapItem


class SitemapItemAdmin(admin.ModelAdmin):
    list_display = ('site', 'name', 'url',)
    search_fields = ('url', 'name',)
    list_filter = ('site',)


admin.site.register(SitemapItem, SitemapItemAdmin)
