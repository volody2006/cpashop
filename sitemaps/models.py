# coding: utf-8
from django.db import models

from market.models import CommonAbstractModel, MySite


class SitemapItem(CommonAbstractModel):
    """
    Урлы которые нужно включить в генерящююся карту сайта
    """
    site = models.ForeignKey(MySite)
    url = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

    class Meta:
        unique_together = ('site', 'url',)
