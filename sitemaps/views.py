# coding: utf-8
import os

from django.contrib.sites.shortcuts import get_current_site
from django.http.response import HttpResponse, Http404

from market.models import MySite
from sitemaps.utils import get_sitemap_directory


def root(request):
    site = MySite.objects.get(site=get_current_site(request))
    filename = os.path.join(get_sitemap_directory(site), 'sitemap.xml')
    try:
        with open(filename, 'r') as fp:
            content = fp.readlines()
            fp.close()
            return HttpResponse(content, content_type='application/xml')
    except FileNotFoundError:
        raise Http404


def content(request, suffix):
    site = MySite.objects.get(site=get_current_site(request))
    filename = os.path.join(get_sitemap_directory(site), '%s.xml' % suffix)
    try:
        with open(filename, 'r') as fp:
            content = fp.readlines()
            fp.close()
            return HttpResponse(content, content_type='application/xml')
    except FileNotFoundError:
        raise Http404
