# from django.db.models.loading import get_model

# from haystack import site
from celery.schedules import crontab
from celery.task import Task, PeriodicTask
from haystack import connections
from haystack.exceptions import NotHandled
from haystack.management.commands import update_index
# from celery.task.schedules import crontab
from haystack.utils.app_loading import haystack_get_model

get_model = haystack_get_model


class SearchIndexUpdateTask(Task):
    name = "search.index.update"
    routing_key = 'search.index.update'
    default_retry_delay = 5 * 60
    max_retries = 1

    def get_index(self, model_class):
        """Fetch the model's registered ``SearchIndex`` in a standarized way."""
        try:
            return connections['default'].get_unified_index().get_index(model_class)
        except NotHandled:
            self.log.error("Couldn't find a SearchIndex for %s." % model_class)
            return None

    def run(self, app_name, model_name, pk, **kwargs):
        logger = self.get_logger(**kwargs)
        try:
            model_class = get_model(app_name, model_name)
            instance = model_class.objects.get(pk=pk)
            search_index = self.get_index(model_class)
            search_index.update_object(instance)
        except Exception as exc:
            logger.error(exc)
            self.retry([app_name, model_name, pk], kwargs, exc=exc)


class SearchIndexUpdatePeriodicTask(PeriodicTask):
    routing_key = 'periodic.search.update_index'
    run_every = crontab(hour=4, minute=0)

    def run(self, **kwargs):
        logger = self.get_logger(**kwargs)
        logger.info("Starting update index")
        # Run the update_index management command
        update_index.Command().handle()
        logger.info("Finishing update index")
