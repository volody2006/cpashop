from celery_haystack.indexes import CelerySearchIndex
from celery_haystack.utils import enqueue_task
from django.db.models import signals
from haystack.exceptions import NotHandled
from haystack.signals import BaseSignalProcessor

from market.models import ENABLED, Lot


class CelerySignalProcessor(BaseSignalProcessor):
    def setup(self):
        signals.post_save.connect(self.enqueue_save)
        signals.post_delete.connect(self.enqueue_delete)

    def teardown(self):
        signals.post_save.disconnect(self.enqueue_save)
        signals.post_delete.disconnect(self.enqueue_delete)

    def enqueue_save(self, sender, instance, **kwargs):
        if sender == Lot:
            if instance.status != ENABLED:
                return self.enqueue('delete', instance, sender, **kwargs)
        return self.enqueue('update', instance, sender, **kwargs)

    def enqueue_delete(self, sender, instance, **kwargs):
        return self.enqueue('delete', instance, sender, **kwargs)

    def enqueue(self, action, instance, sender, **kwargs):
        """
        Given an individual model instance, determine if a backend
        handles the model, check if the index is Celery-enabled and
        enqueue task.
        """
        using_backends = self.connection_router.for_write(instance=instance)

        for using in using_backends:
            try:
                connection = self.connections[using]
                index = connection.get_unified_index().get_index(sender)
            except NotHandled:
                continue  # Check next backend

            if isinstance(index, CelerySearchIndex):
                if action == 'update' and not index.should_update(instance):
                    continue
                enqueue_task(action, instance)


class RealtimeSignalProcessor(BaseSignalProcessor):
    def setup(self):
        signals.post_save.connect(self.enqueue_save)
        signals.post_delete.connect(self.enqueue_delete)

    def teardown(self):
        signals.post_save.disconnect(self.enqueue_save)
        signals.post_delete.disconnect(self.enqueue_delete)

    def enqueue_save(self, sender, instance, **kwargs):
        if sender == Lot:
            if instance.status != ENABLED:
                return self.enqueue('delete', instance, sender, **kwargs)
        return self.enqueue('update', instance, sender, **kwargs)

    def enqueue_delete(self, sender, instance, **kwargs):
        return self.enqueue('delete', instance, sender, **kwargs)

    def enqueue(self, action, instance, sender, **kwargs):
        """
        Given an individual model instance, determine if a backend
        handles the model, check if the index is Celery-enabled and
        enqueue task.
        """
        using_backends = self.connection_router.for_write(instance=instance)

        for using in using_backends:
            try:
                index = self.connections[using].get_unified_index().get_index(sender)
                # if action == 'update' and not index.should_update(instance):
                if action == 'update':
                    index.update_object(instance, using=using)
                if action == 'delete':
                    index.remove_object(instance, using=using)
            except NotHandled:
                pass
