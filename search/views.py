# -*- coding: utf-8 -*-
from django.http.request import QueryDict
# from django.utils.translation import ugettext as _
from haystack.backends import SQ
from haystack.generic_views import SearchView, FacetedSearchView

# from haystack.models import SearchResult
from template_selection.managers import get_custom_template_name, custom_get_current_site


class MySearchView(SearchView):
    """My custom search view."""
    # template_name = 'trendsetter/category.html'
    #
    # template_name = 'smart_shop/search.html'
    template = 'category.html'

    def get_template_names(self):

        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """

        if self.template is None:
            names = super(MySearchView, self).get_template_names()
        else:
            self.template_name = get_custom_template_name(self.request, self.template)
            names = []
            names.append(self.template_name)
        return names


    def get_queryset(self):
        queryset = super(MySearchView, self).get_queryset()
        q_filter = {}
        if self.request.GET.get('price_from'):
            try:
                q_filter.update({'price__gte': int(self.request.GET.get('price_from'))})
            except ValueError:
                pass
        if self.request.GET.get('price_to'):
            try:
                q_filter.update({'price__lte': int(self.request.GET.get('price_to'))})
            except ValueError:
                pass
        if self.request.GET.get('price_exact'):
            try:
                q_filter.update({'price__exact': int(self.request.GET.get('price_exact'))})
            except ValueError:
                pass

        if self.request.GET.get('vendor'):
            try:
                q_filter.update({'vendor': self.request.GET.get('vendor')})
            except ValueError:
                pass

        usr = 'price'
        if self.request.GET.get('order'):
            order = self.request.GET.get('order')
            usr = 'price'
            if order == 'pa' or order == 'price':
                usr = 'price'
            elif order == 'pd':
                usr = '-price'
            elif order == 'sd':
                usr = '-date_created'
            elif order == 'ea':
                usr = 'date_created'
            elif order == 'dcd':
                usr = '-deal_count'
            elif order == 'r':
                usr = '-rank'
        q_filter.update({'sites': custom_get_current_site(self.request).domain})
        q_object = SQ(**q_filter)
        if self.get_queryset_theme():
            q_object.add(self.get_queryset_theme(), SQ.AND)
        lots = queryset.filter(q_object).order_by(usr)

        return lots.all()  # filter(update__lte=datetime.now())

    def get_queryset_theme(self):
        if self.get_mysite().query is not None:
            return self.get_mysite().parse_query()
        return None

    def get_site(self):
        return custom_get_current_site(self.request)

    def get_mysite(self):
        return self.get_site().mysite

    def get_context_data(self, *args, **kwargs):
        # SearchResult
        context = super(MySearchView, self).get_context_data(*args, **kwargs)

        if context['object_list'] == []:
            # TODO: Если запрос возвращает пустой список, показываем рекомендованные товары.
            # name = context['query']
            pass

        getvars = self.request.GET.copy()
        context.update({'param_filters': self.get_param_filters(),
                        # 'active_filters': self.get_active_filters(),
                        # 'eav_attrs': self.get_eav(),

                        })
        if 'page' in getvars:
            del getvars['page']
        if len(list(getvars.keys())) > 0:
            context['getvars'] = "&%s" % getvars.urlencode()
        else:
            context['getvars'] = ''

        if self.request.GET.get('price_from'):
            try:
                context['price_from'] = int(self.request.GET.get('price_from'))
            except ValueError:
                pass
        if self.request.GET.get('price_to'):
            try:
                context['price_to'] = int(self.request.GET.get('price_to'))
            except ValueError:
                pass

        return context

    def get_param_filters(self):
        """
        возвращает словарь со всеми параметрами гет-запроса
        убираем пустые, пропускаем по white-листу
        """
        param_filters = self.request.GET

        allowed_params = ['price_from', 'price_to', 'o', 'sf', 'seller', 'q', 'theme', 'tag', 'f', 'order']
        filtered_params = QueryDict('').copy()
        for k in list(param_filters.keys()):
            if k in allowed_params or k.startswith('attr_'):
                filtered_params.setlist(k, param_filters.getlist(k))
        return filtered_params


class CustomFacetedSearchView(FacetedSearchView):
    facet_fields = ['category']
    template_name = 'smart_shop/search_facet.html'

    def get_context_data(self, *args, **kwargs):
        # SearchResult
        context = super(CustomFacetedSearchView, self).get_context_data(*args, **kwargs)
        # context['facet_fields'] = self.facet_fields
        context.update({'facet_counts': self.get_queryset().facet_counts()})
        return context
