# -*- coding: utf-8 -*-
# from haystack.forms import FacetedSearchForm
from search.views import MySearchView, CustomFacetedSearchView
from django.conf.urls import url

urlpatterns = [
    url(r'^$', MySearchView.as_view(), name='search'),
    url(r'^facet$', CustomFacetedSearchView.as_view(facet_fields=['category']), name='haystack_search1111'),
]
