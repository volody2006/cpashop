# coding: utf-8

"""
Обработчик модели SAX для прайс-листов в формате YML

"""

import logging
from urllib.parse import urlparse
from xml.sax import ContentHandler

from django.conf import settings
from django.utils import six
from django.utils.encoding import force_str

from import_lots.exceptions import InvalidOffer
from lib import to_unicode, absolute_url
from market.models import Category, Lot, Param, Image, Currency
from market.utils import not_escape
from menu.templatetags.menu_tags import cleaned_text

logger = logging.getLogger(__name__)


class LoggerMixin(object):
    feed = None

    def __repr__(self):
        try:
            u = six.text_type(self)
        except (UnicodeEncodeError, UnicodeDecodeError):
            u = '[Bad Unicode data]'
        return force_str('<%s: %s>' % (self.__class__.__name__, u))

    def _format(self, message):
        return '[Feed=%s] %s' % (self.feed.pk, message)

    def debug(self, message):
        logger.debug(self._format(message))

    def info(self, message):
        logger.info(self._format(message))

    def warn(self, message):
        logger.warning(self._format(message))

    def error(self, message):
        logger.error(self._format(message))


class SAXContentHandlerBase(ContentHandler):
    def startElement(self, name, attrs):
        self._text = ''
        method = 'start_%s' % name

        if hasattr(self, method):
            getattr(self, method)(attrs)

    def endElement(self, name):
        method = 'end_%s' % name

        if hasattr(self, 'end_%s' % name):
            getattr(self, method)()

    def characters(self, content):
        self._text += content


class FeedContentHandler(SAXContentHandlerBase, LoggerMixin):
    """
    SAX-обработчкик файлов в формате YML.

    Пример использования в консоли django:
    """

    def __init__(self, feed):
        super(FeedContentHandler).__init__()
        self.feed = feed
        self._inside = False
        # self.offers_list = None
        self.current_offer = None
        self.offer_count = 0
        self.categories = []

    def reached_maximum_value_offer_count(self):
        if settings.TASK_IMPORT_LOTS_MAX_OFFER_COUNT:
            return self.current_offer >= settings.TASK_IMPORT_LOTS_MAX_OFFER_COUNT
        return False

    def read_string(self):
        """
        Интерфейс для считывания текстового содержимого текущего узла
        """
        return self._text.strip()

    def read_boolean(self):
        """
        Приводит строку 'true' или 'false', полученную из
        содержимого узла, к соответствующему булевому значению.
        """
        return self.read_string().upper() == 'TRUE'

    def read_number(self):
        """
        Приводит строку, полученную из содержимого узла, к
        соответствующему целочисленному значению.
        """
        try:
            raw = self.read_string()
            return int(float((raw or '0').replace(',', '.')))
        except(TypeError, UnicodeDecodeError, ValueError):
            return 0

    # def is_adult(self):
    #     """
    #     Возвращает глобальное значение флага <adult> для всего прайса
    #     """
    #     # return self.offers_list.is_adult

    def in_offer(self):
        """
        Возвращает True, если курсор находится внутри узла <offer></offer>
        """
        return self._inside

    def enter_the_offer(self):
        """
        Устанавливает флаг "курсор находится ВНУТРИ узла <offer></offer>
        """
        self._inside = True
        self.offer_count += 1

    def left_the_offer(self):
        """
        Устанавливает флаг "курсор находится ВНЕ узла <offer></offer>
        """
        self._inside = False

    # def start_currency(self, attrs):
    #     """
    #     Находит и разбирает упоминание о валюте
    #     """
    #     # self.offers_list.add_currency(Currency(feed=self.feed,
    #     #                                        id=attrs.get('id'),
    #     #                                        plus=attrs.get('plus', 0),
    #     #                                        rate=attrs.get('rate', 1)))

    def start_category(self, attrs):
        """
        Поиск категорий и метаинформации о них (id, parentId)
        """
        self._category = {'id': attrs.get('id'),
                          'parent_id': attrs.get('parentId')}

    def end_category(self):
        """
        Сохранение категории
        """
        category = FeedCategory(feed=self.feed,
                                name=self.read_string().lower(),
                                source_id=self._category['id'],
                                parent_id=self._category['parent_id'])
        category.add_category()
        del self._category

    # def start_categories(self):
    #     pass

    def end_categories(self):
        self.debug('Блок категории обработали')

    def end_name(self):
        """
        Сохранение названия предложения (название магазина, которое хранится в одноимённом
        узле, не обрабатывается)
        """
        if not self.in_offer():
            return
        self.current_offer.update(name=self.read_string())

    def end_adult(self):
        """
        Обработка узла <adult></adult>.
        Действует как глобально, так и в пределах текущего <offer></offer>
        """
        is_adult = self.read_boolean()

        if self.in_offer():
            self.current_offer.update(adult=is_adult)
            # else:
            #     self.offers_list.is_adult = is_adult

    def start_offer(self, attrs):
        """
        Сигнал о начале разбора нового <offer></offer>.
        Генерируется начальная структура  и уникальный идентификатор временного FeedOffer
        """
        self.debug('Вошли в блок offer')
        self.enter_the_offer()
        self.is_main = True
        self.current_offer = FeedOffer(self.feed)
        if 'id' in attrs:
            self.current_offer.update(offer_id=attrs['id'])
            self.current_offer.id = attrs['id']
        if 'type' in attrs:
            self.current_offer.update(type=attrs['type'])
        if 'available' in attrs:
            self.current_offer.update(available=attrs['available'])

    def end_offer(self):
        """
        Завершение блока <offer></offer>
        """
        # Обработку офера заканчиваем, записываем в базу, очищаем переменную
        self.debug('offer_count: %s' % self.offer_count)
        # if self.offer_count > 10000:
        if self.reached_maximum_value_offer_count():
            self.warn('Больше 10000 товаров!')
            raise ImportError
        self.current_offer.add_offer()
        self.current_offer = None
        self.left_the_offer()
        self.debug('Завершение блока <offer></offer>')

    def end_offers(self):
        """
        Завершение блока <offers></offers>
        """
        # self.offers_list.push()
        self.info('всего предложений было %s' % self.offer_count)
        self.debug('Завершение блока <offers></offers>')

    def end_url(self):
        """
        URL товара или магазина
        """
        if not self.in_offer():
            return
        self.current_offer.update(url=self.read_string().strip())

    def end_price(self):
        """
        Получает значение цены без указания валюты и помещает его в данные
        текущщего оффера.
        """
        self.current_offer.update(price=self.read_number())

    def end_oldprice(self):
        """
        Получает значение старой цены
        """
        self.current_offer.update(oldprice=self.read_number())

    def end_currencyId(self):
        """
        Получает индекс валюты <currencyId></currencyId> для текущего <offer></offer>.
        Допускается несколько значений (узлов)
        """
        # currency = Currency(self.feed, id=self.read_string())
        # self.current_offer.add_currency(currency)
        if not self.in_offer():
            return
        self.current_offer.update(currencyId=self.read_string())

    def end_categoryId(self):
        """
        Получает ID категории
        """
        self.current_offer.update(categoryId=self.read_string())

    def start_param(self, attrs):
        """
        Разбирает атрибуты характеристики
        """
        self._param = FeedParam(feed=self.feed.pk,
                                name=attrs.get('name'),
                                unit=attrs.get('unit'))

    def end_param(self):
        """
        Добавляет параметр-характеристику
        """
        self._param.set_value(value=self.read_string())
        self.current_offer.add_param(self._param.__dict__)
        del self._param

    def end_picture(self):
        """
        Добавляет изображение из YML
        """
        self.debug('Завершение блока picture')
        picture = FeedPicture(feed=self.feed,
                              offer_id=self.current_offer.id,
                              url=self.read_string(),
                              is_main=self.is_main,
                              )
        self.current_offer.add_picture(picture_id=picture.add_image())
        self.is_main = False

    def end_delivery(self):
        """
        Добавляет флаг доставки
        """
        if not self.in_offer():
            return
        self.current_offer.update(delivery=self.read_boolean())

    def end_description(self):
        """
        Добавляет описание товара
        """
        self.current_offer.update(description=self.read_string())

    def end_vendorCode(self):
        """
        Добавляет артикул производителя. Обязательно
        """
        self.current_offer.update(vendorCode=self.read_string())

    def end_vendor(self):
        """
        Добавляет производителя. Обязательно
        """
        self.current_offer.update(vendor=self.read_string())

    def end_model(self):
        """
        Добавляет производителя. Обязательно
        """
        self.current_offer.update(model=self.read_string())

    def end_typePrefix(self):
        """
        Добавляет typePrefox
        """
        self.current_offer.update(typePrefix=self.read_string())

    def end_sales_notes(self):
        """
        Добавляет параметр sales_notes
        """
        self.current_offer.update(sales_notes=self.read_string())


class FeedCategory(LoggerMixin):
    def __init__(self, feed, source_id, name='', parent_id=None):
        self.feed = feed
        self.name = name
        self.source_id = source_id
        self.parent_id = parent_id
        self.parent = None

    def add_category(self):
        self.add_parent_category()
        category_obj, created = Category.objects.update_or_create(
            feed=self.feed,
            source_id=self.source_id,
            defaults={'name': self.name,
                      'parent': self.parent,
                      'source_parent_id': self.parent_id
                      }
        )
        if created:
            self.info('Создали категорию: %s - %s' % (category_obj.pk, category_obj.name))
        else:
            self.info('Обновили категорию: %s - %s' % (category_obj.pk, category_obj.name))

    def add_parent_category(self):
        if self.parent_id is not None:
            self.debug('Got parent category: %s' % self.parent_id)
            self.parent, parent_created = Category.objects.get_or_create(
                feed=self.feed,
                source_id=self.parent_id
            )


class FeedOffer(LoggerMixin):
    field_coerce = {'typePrefix': 'string',
                    'name': 'string',
                    'sales_notes': 'string',
                    'adult': 'boolean',
                    'available': 'boolean',
                    'group_id': 'string',
                    'offer_id': 'string',
                    'type': 'string',
                    'url': 'string',
                    'bid': 'number',
                    'cbid': 'number',
                    'model': 'string',
                    'vendor': 'string',
                    'vendorCode': 'string',
                    'description': 'string',
                    'categoryId': 'string',
                    'price': 'number',
                    'delivery': 'string',
                    'oldprice': 'number',
                    'currencyId': 'string',
                    }

    def __init__(self, feed, id=None):
        self.feed = feed
        self.id = id
        self.currencies = []
        self.pictures = []
        self.params = []
        self.is_valid = True
        self.cached_data = {k: '' for k in self.field_coerce}
        if not self.cached_data['adult']:
            self.cached_data['adult'] = False

    def add_offer(self):
        self.info('Add offer %s' % self.cached_data['offer_id'])
        self.validate()
        if self.is_valid:
            category, created_category = Category.objects.get_or_create(feed=self.feed,
                                                                        source_id=self.cached_data['categoryId'])
            currency, created_currency = Currency.objects.get_or_create(iso_code=self.cached_data['currencyId'])

            lot, created = Lot.objects.update_or_create(
                offer_id=self.cached_data['offer_id'],
                seller=self.feed.seller,
                defaults={
                    'category': category,
                    'name': self.get_normal_name(),
                    'description': cleaned_text(not_escape(self.cached_data['description'])),
                    'is_available': self.cached_data['available'],
                    'adult': self.cached_data['adult'],
                    'price': self.cached_data['price'],
                    'old_price': self.get_oldprice(),
                    'currency': currency,
                    'model': self.cached_data['model'],
                    'vendor_code': self.cached_data['vendorCode'],
                    'vendor': self.cached_data['vendor'],
                    "type_prefix": self.cached_data['typePrefix'],
                    'url': self.get_url(),
                })

            # lot.name = self.get_normal_name()
            # lot.description = cleaned_text(not_escape(self.cached_data['description']))
            # lot.is_available = self.cached_data['available']
            # lot.adult = self.cached_data['adult']
            # lot.price = self.cached_data['price']
            # lot.old_price = self.get_oldprice()
            # lot.currency = currency
            # lot.model = self.cached_data['model']
            # lot.vendor_code = self.cached_data['vendorCode']
            # lot.vendor = self.cached_data['vendor']
            # lot.type_prefix = self.cached_data['typePrefix']
            # lot.url = self.get_url()
            # lot.save()

            if created:
                self.info('Cоздали товар %s - %s' % (lot.pk, lot.name))
            else:
                lot.lot_params.clear()
                self.info('Обновили товар %s - %s' % (lot.pk, lot.name))

            for param in self.params:
                # TODO: Добавить исключения
                try:
                    param, created_param = Param.objects.get_or_create(name=param['name'],
                                                                       value=param['value'],
                                                                       unit=param['unit'])
                except Param.MultipleObjectsReturned:
                    params = Param.objects.filter(name=param['name'],
                                                  value=param['value'],
                                                  unit=param['unit'])
                    param = params[0]
                    for param_del in params[1:]:
                        param_del.delete()
                lot.lot_params.add(param)
                # except:
                #    self.info('Failed to get the goods and recording settings. meaning: name=%s, value=%s, unit=%s' % (
                #        param['name'], param['value'], param['unit']))

            from import_lots.tasks import images_load
            pictures = self.pictures
            if settings.CELERY_START:
                self.debug('CELERY On')
                images_load.delay(lot.pk, pictures)
            else:
                self.debug('CELERY OFF')
                images_load(lot.pk, pictures)
        else:
            self.info('Этот продукт не прошел проверку, пропустить.')

    def add_picture(self, picture_id):
        """
        Добавляет id картинок в базе данных для текущего оффера)
        """
        self.pictures.append(picture_id)

    def add_param(self, param):
        """
        Добавляет предложению параметр (объект Param, содержащий
        нормализованные название, единицы измерения и значение)
        """
        self.params.append(param)

    def validate(self):
        """
        Валидирует информацию из предложения
        """
        data = self.data()
        error_valid = []

        if not data.get('offer_id'):
            error_valid.append('Нет оффера id %s' % data)
            self.is_valid = False

        name = self.get_normal_name()

        if not name or name.lower() == 'none':
            error_valid.append('Оффер без имени, печаль %s' % data)
            self.is_valid = False

        if len(name) > 255:
            error_valid.append('Длина имени больше 255 символов %s' % name)
            self.is_valid = False

        if not data.get('categoryId'):
            self.is_valid = False
            error_valid.append('Оффер без категории? Неподходит!')

        if data.get('price', 0) <= 0:
            self.is_valid = False
            error_valid.append('Цена меньше или равна нулю, нам такое ненужно.')

        if not self.has_pictures():
            self.is_valid = False
            error_valid.append('У оффера нет картинок, пропускаем')

        if self.get_url() == '':
            self.is_valid = False
            error_valid.append('URL нет?! А нам такое зачем?')

        if self.is_valid is False:
            self.error_valid = error_valid
            print(self.error_valid)

    def get_url(self):

        data = self.cached_data
        # print data
        url = data['url']
        if self.feed.get_param_save is True:
            self.info('URL as is %s' % url)
            return url
        o = urlparse(url)
        get_params = o.query.split('&')
        for get in get_params:
            if get.startswith('ulp='):
                return get.split('ulp=')[1]
        return ''

    def get_oldprice(self):
        if not self.cached_data['oldprice'] == '':
            if self.cached_data['oldprice'] > self.cached_data['price']:
                return self.cached_data['oldprice']
        return None

    def get_name(self):
        data = self.cached_data
        name = ''

        # в упрощенном формате - name - обязательный параметр
        if data.get('type') in ['', 'book'] and data.get('name'):
            name = data['name']
        if data.get('type') == 'vendor.model':
            type_prefix = data.get('typePrefix')
            vendor = data.get('vendor', '')
            model = data.get('model', '')
            bits = []
            vals = {}

            if type_prefix:
                bits.append('%(typePrefix)s')
                vals.update(typePrefix=to_unicode(type_prefix))

            if vendor:
                vendor = to_unicode(vendor)

                if vendor not in to_unicode(model):
                    bits.append('%(vendor)s')
                    vals.update(vendor=vendor)

            if model:
                bits.append('%(model)s')
                vals.update(model=to_unicode(model))
            name = to_unicode(' '.join(bits) % vals)
            vendor_code = data.get('vendorCode', '')

            if vendor_code and to_unicode(vendor_code) not in to_unicode(name):
                name = '%s %s' % (to_unicode(name), to_unicode(vendor_code))
            # if isinstance(name, str):
            #     name = name.decode('utf-8')
            if model and to_unicode(model) not in to_unicode(name):
                name = '%s %s' % (to_unicode(name), to_unicode(model))
        name = to_unicode(name).replace('&amp;', '&').replace('&quot;', '"')
        return name

    def get_normal_name(self):
        # if self.cached_data['name'] is None or self.cached_data['name'] == '':
        #     name = self.get_name()
        # else:
        #     name = self.cached_data['name']
        name = self.get_name()
        if name.isupper():
            return name.capitalize()
        return name

    def has_pictures(self):
        """
        Возвращает True, если у предложения есть картинки
        """
        return len(self.pictures) > 0

    def update(self, **kwargs):
        """
        Обновляет значение полей предложения
        """
        for field, value in list(kwargs.items()):
            if field not in self.field_coerce:
                raise TypeError('Unexpected offer field (%s)' % field)
            try:
                self.cached_data[field] = self.coerce(field, value)
            except ValueError as e:
                raise InvalidOffer('Invalid value for "%s" field: %s' % (field, e.message), data={}, value=value)

    def coerce(self, field, value):
        return getattr(self, 'to_%s' % self.field_coerce[field])(value)

    def to_string(self, value):
        return value

    def to_number(self, value):
        if not value:
            return 0
        if isinstance(value, (float, int)):
            return int(value)
        if not value.replace(',', '').replace('-', '').replace('.', '').isdigit():
            raise ValueError('must consist only digits')
        return int(value)

    def to_boolean(self, value):
        if isinstance(value, bool):
            return value
        if not value:
            return False
        if value.lower() in ['0', 'no', 'none', 'false', 'n/a', 'нет']:
            return False
        return True

    def data(self):
        return self.cached_data


class FeedPicture(LoggerMixin):
    def __init__(self, feed, offer_id, url, is_main, lot=None):
        self.feed = feed
        self.url = url
        self.url = absolute_url(url, based_on=feed.xml_url)
        self.offer_id = offer_id
        self.is_main = is_main
        self.lot = lot

    def __unicode__(self):
        return self.url

    def reload_imade(self, lot):
        image = Image.fetch(url=self.url,
                            lot=lot,
                            is_main=self.is_main
                            )
        try:
            if image.image is not None:
                image.image_resize()
        except AttributeError:
            pass

    def add_image(self):

        lot, create = Lot.objects.get_or_create(offer_id=self.offer_id,
                                                seller=self.feed.seller)
        image, create = Image.objects.update_or_create(downloaded_from=self.url,
                                                       lot=lot,
                                                       defaults={'is_main': self.is_main, }
                                                       )
        return image.pk


class FeedParam(LoggerMixin):
    def __init__(self, feed, name, unit=None, value=None):
        self.feed = feed
        self.name = name
        self.unit = unit
        self.value = value

    def set_value(self, value):
        self.value = value

    def __unicode__(self):
        return '{name}, {unit}: {value}'.format(name=self.name, unit=self.unit, value=self.value)
