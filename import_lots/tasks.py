# -*- coding: utf-8 -*-
import logging
import os
from xml.sax import make_parser

from billiard.exceptions import SoftTimeLimitExceeded
from celery.schedules import crontab
from celery.task import PeriodicTask, periodic_task
from celery.task import task
from django.utils import timezone

from cpashop import settings
from import_lots.exceptions import FeedImportError
from import_lots.managers import Downloader, Handler
from import_lots.utils.sax_handler import FeedContentHandler
from market.models import Feed, Lot, Image, ENABLED

logger = logging.getLogger(__name__)


class RunImportFeeds(PeriodicTask):
    # run_every = timedelta(hours=23)
    background = settings.CELERY_START
    if background:
        run_every = crontab(hour=23, minute=51)
    else:
        run_every = None

    def run(self, **kwargs):
        for feed in Feed.objects.filter(run_auto=True):
            if self.background:
                download_feed.delay(feed.pk, background=self.background)
            else:
                download_feed(feed.pk, background=self.background)


@periodic_task(run_every=crontab(hour=23, minute=51))
def run_import_feeds(background=True):
    """
    Дергаем фиды только платных юзеров, больше терпеть мочи нет
    """
    for feed in Feed.objects.filter(run_auto=True):
        if background:
            download_feed.delay(feed.pk, background=background)
        else:
            download_feed(feed_id=feed.pk, background=background)


def start_test_parse():
    feed = Feed.objects.get(pk=1)

    parser = make_parser()
    parser.setContentHandler(FeedContentHandler(feed=feed))
    filename = os.path.join(settings.TMP_ROOT, 'test.xml')
    parser.parse(filename)


@task
def start_parse(feed_id):
    feed = Feed.objects.get(pk=feed_id)
    downloader = Downloader(feed=feed)
    downloader.run()
    parser = make_parser()
    parser.setContentHandler(FeedContentHandler(feed=feed))
    filename = os.path.join(settings.TMP_ROOT, '%s.yml' % str(feed.pk))
    parser.parse(filename)


@task
def images_load(lot_id, pictures):
    logger.debug('Task images_load. lot_id %s >pictures: %s', lot_id, pictures)
    lot = Lot.objects.get(pk=lot_id)
    for picture_id in pictures:
        image = Image.objects.get(pk=picture_id)
        image.lot = lot
        image.save()
        image.load()

    if Image.objects.filter(lot=lot).count() > 0:
        logger.info('There are pictures. Up the product %s - %s', lot.pk, lot.name)
        lot.up()


@task
def download_feed(feed_id, background=True):
    """
    Первый этап: скачивание.

    """
    feed = Feed.objects.get(pk=feed_id)
    downloader = Downloader(feed=feed)
    try:
        downloader.run()
    except (FeedImportError, SoftTimeLimitExceeded) as e:
        logger.info('Ошибка сачивания фида id=%s' % feed.pk)
        finalize_import(feed_id, yml_filename=downloader.get_filename(), background=background)
        return
    if background:
        parse_feed.delay(feed_id, yml_filename=downloader.get_filename(), background=background)
    else:
        parse_feed(feed_id, yml_filename=downloader.get_filename(), background=background)


@task
def parse_feed(feed_id, yml_filename, background=True):
    """
    Выполняет разбор прайслиста в формате YML
    """
    feed = Feed.objects.get(pk=feed_id)
    logger.info('[Feed=%s] Начинаем парсить фид' % feed.pk)

    now = timezone.now()
    try:
        handler = Handler(feed=feed, filename=yml_filename)
        handler.run()
    except (FeedImportError, SoftTimeLimitExceeded) as e:
        logger.error('[Feed=%s] Ошибка парсинга фида e=%s' % (feed_id, e))
    finally:
        if background:
            down_lot.delay(feed_id, now, background=background)
            finalize_import.delay(feed_id, yml_filename, background=background)
        else:
            down_lot(feed_id, now, background=background)
            finalize_import(feed_id, yml_filename, background=background)
        return


@task
def down_lot(feed_id, now, background=True):
    feed = Feed.objects.get(pk=feed_id)
    logger.info(
        '[Feed=%s] Снимаем с показа товары не обновленные с последнего импорта seller = %s' % (feed_id, feed.seller))
    lots = Lot.objects.filter(date_changed__lt=now,
                              seller=feed.seller,
                              status=ENABLED)
    for lot in lots:
        logger.info('[Feed=%s] Сняли с показа лот №%s - %s' % (feed_id, lot.pk, feed.seller.name))
        lot.down()


@task
def finalize_import(feed_id, yml_filename, background=True):
    """
    Задача, завершающая всякие формальности после окончания импорта предложений.

        * Выставляет статус "импорт завершён"
        * Удаляет временные файлы

    Что-то ещё?
    """
    feed = Feed.objects.get(pk=feed_id)
    delete_tmp_file(yml_filename)
    logger.info('[Feed=%s] Закончили парсить фид, удаляем временные файлы' % feed.pk)
    # Чтобы обновилось поле date_changed
    feed.save()


@task
def delete_tmp_file(filename):
    """
    Удаляет временный файл скачанного прайс-листа в формате YML
    """
    try:
        os.unlink(filename)
    except OSError as e:
        logger.info('Unable delete %s: %s' % (filename, e))
