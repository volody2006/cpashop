# coding: utf-8


class ForceStop(Exception):
    """
    Исключение, бросаемое при принудительной остановке процедуры импорта
    """
    pass


class InvalidOffer(Exception):
    """
    Исключение, бросаемое в случае неподходящего оффера (например, отсутствующий id)
    """

    def __init__(self, message, data=None, value=None, key=None):
        Exception.__init__(self, message)
        self.data = data
        self.key = key
        self.value = value or self.data.get(self.key)


class InvalidCategory(Exception):
    pass


class FeedImportError(Exception):
    """
    Исключение, бросаемое в случае ошибки импорта фида
    """


class TooLargeYml(Exception):
    """
    Исключение, бросаемое при попытке загрузить YML-файл, превышающий максимально
    допустимый размер (в байтах)
    """


class NoUpdateRequired(Exception):
    """
    В скачанном прайслисте не произошло никаких изменений со времени последнего импорта
    """


class StopOfferIteration(Exception):
    """
    Перебор предложений принудительно завершён, вероятнее всего, из-за превышения
    максимально допустимого количества оных)
    """


class AlreadyCleaned(Exception):
    """
    Попытка обработать оффер, который уже был очищен
    """


class GhostOfferHappened(Exception):
    """
    Попытка заполнить оффер, данные для которого стёрты
    """
