# coding: utf-8

import hashlib
import logging
import os
import shutil
import subprocess
import uuid
from urllib.error import URLError
from xml.sax import make_parser

from django.conf import settings

from import_lots.exceptions import FeedImportError
from import_lots.utils.sax_handler import FeedContentHandler

logger = logging.getLogger(__name__)


class Downloader:
    """
    Робот. отвечающий за скачивание файла
    """
    chunk_size = 16 * 1024
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.22 (KHTML, like Gecko) ' + \
                 'Chrome/25.0.1364.172 Safari/537.22'

    def __init__(self, feed, test_run=False):
        self.test_run = test_run
        self.gzipped = False
        self.fp = None
        self.feed = feed
        self.checksum = hashlib.md5()
        self.had_changed = False
        self.filename = self.generate_tmp_filename()
        self.test_run = test_run

    def run(self):
        """
        Точка входа
        """
        # self.check_modified_since()
        logger.info('[Feed=%s] download YML: %s', self.feed.pk, self.feed.xml_url)

        self.save_anyway()

    def feed_had_changed(self):
        """
        Возвращает True, если по сравнению с прошлой версией скачанного прайслиста
        произошли какие-то изменения (не совпадает контрольная сумма)
        """
        return self.had_changed

    def get_filename(self):
        """
        Возвращает имя временного файла, под которым сохраняется скачиваемый.
        """
        return self.filename

    def generate_tmp_filename(self):
        """
        Генерирует имя файла, под которым будет сохранён YML-прайс.
        """
        if self.test_run:
            return os.path.join(settings.TMP_ROOT, '%s.yml' % 'test_file')
        return os.path.join(settings.TMP_ROOT, '%s.yml' % str(self.feed.pk))

    def save_anyway(self):
        """
        Выполняет безусловное скачивание YML-файла
        """
        import requests
        try:
            src = requests.get(url=self.feed.xml_url, stream=True,
                               headers={'User-Agent': self.user_agent},
                               # auth=self.get_auth()
                               )
            if src.status_code == 404:
                logger.info('[Feed=%s] Cannot download YML: 404 Not Found %s', self.feed.pk, self.feed.xml_url)
                raise FeedImportError('Cannot download YML: 404 Not Found %s' % self.feed.xml_url)

            self.filesize = int(src.headers.get('content-length') or 0)
            self.gzipped = 'gzip' in (src.headers.get('content-encoding') or '')
            with open(self.filename, "wb") as file:
                file.write(src.content)
                file.close()
            logger.info('[Feed=%s] Закончили скачивать файл %s', self.feed.pk, self.get_filename())

        except (requests.exceptions.ConnectionError, requests.exceptions.MissingSchema) as e:
            logger.info('HTTP error: %s' % str(e.args[0]))
            raise FeedImportError('Unable connect with YML host: %s' % e)
        except URLError as e:
            logger.info('url error [%e]' % e)
            raise FeedImportError('Cannot download YML: remote resource not found')


class Handler:
    """
    Обработка YML-файла средствами SAX-парсера
    """

    def __init__(self, feed, filename):
        self.feed = feed
        self.filename = filename
        # self.redis = get_redis()
        self.parser = None
        self.content_handler = None
        self.setup_parser()

    def setup_parser(self):
        """
        Создаёт объект SAX-парсера
        """
        self.parser = make_parser()
        self.parser.setContentHandler(FeedContentHandler(feed=self.feed))
        # self.parser.setFeature(handler.feature_external_ges, False)

    def get_content_handler(self):
        return self.content_handler

    def run(self):
        """
        Точка входа
        """
        logger.info('Starts parse feed from local file [%s]' % self.filename)
        # self.patch_encoding()
        try:
            self.parser.parse(self.filename)
        except ValueError:
            old_file = os.path.join(settings.BASE_DIR, 'import_lots', 'shops.dtd')
            new_file = os.path.join(settings.TMP_ROOT, 'shops.dtd')
            shutil.copy(old_file, new_file)
            logger.warning('Нет файла %s, копируем его из %s' % (new_file, old_file))
            self.parser.parse(self.filename)
        self.content_handler = self.parser.getContentHandler()

    def patch_encoding(self):
        logger.debug('Check if encoding patch need')

        # Проверяем, нужно ли исправлять кодировку
        with open(self.filename, 'r') as fp:
            first_bytes = str(fp.read(512))
            try:
                # Правильные пацаны используют UTF-8
                if 'utf8' not in first_bytes.decode('utf-8').lower():
                    return
            except UnicodeDecodeError:
                # Неправильные - какую-нибудь гадость типа WINDOWS-1251
                # Мы даже не будем пытаться ничего сделать
                return

        # Таки нужно
        tmp_filename = '/tmp/mv_%s' % uuid.uuid4()

        # По определению, subprocess.call не передаёт управление дальше до тех пор, пока запущенная
        # программа не закончит работу
        command = "cat {src} | sed -e 's/UTF8/utf-8/I' > {dst} && mv {dst} {src}".format(dst=tmp_filename,
                                                                                         src=self.filename)

        logger.debug('Patching encoding from UTF8 to UTF-8 (tmpfile=%s): %s' % (tmp_filename, command))
        try:
            subprocess.call(command, shell=True)
        except OSError:
            logger.warning('Error during encoding patch (OSERROR)')
