# -*- coding: utf-8 -*-
import json
import os
import shutil

from django.conf import settings
from django.test import TestCase
from mixer.backend.django import mixer

from import_lots.exceptions import FeedImportError
from import_lots.managers import Downloader, Handler
from market import models


# from cpashop.test_settings import HTTPBIN_SERVER


class DownloadFeedTest(TestCase):
    def setUp(self):
        self.url_feed = '%s/xml' % settings.HTTPBIN_SERVER
        self.url_404 = '%s/status/404' % settings.HTTPBIN_SERVER
        self.url_gzip = '%s/gzip' % settings.HTTPBIN_SERVER

        self.feed = mixer.blend(models.Feed,
                                xml_url=self.url_feed,
                                run_auto=True,
                                get_param_save=False)
        self.feed_404 = mixer.blend(models.Feed,
                                    xml_url=self.url_404,
                                    run_auto=True,
                                    get_param_save=False)
        self.feed_gzip = mixer.blend(models.Feed,
                                     xml_url=self.url_gzip,
                                     run_auto=True,
                                     get_param_save=False)

    def tearDown(self):
        try:
            os.unlink(self.filename)
        except FileNotFoundError:
            pass

    def test_downloader_feed(self):
        self.downloader = Downloader(feed=self.feed, test_run=True)
        self.downloader.run()
        self.filename = self.downloader.filename
        self.assertTrue(os.path.isfile(self.downloader.generate_tmp_filename()))
        self.assertEqual(self.downloader.generate_tmp_filename(), self.downloader.filename)

    def test_downloader_404(self):
        self.downloader_404 = Downloader(feed=self.feed_404, test_run=True)
        self.filename = self.downloader_404.filename
        with self.assertRaises(FeedImportError):
            self.downloader_404.run()

    def test_downloader_gzip_file(self):
        self.downloader_gzip = Downloader(feed=self.feed_gzip, test_run=True)
        self.downloader_gzip.run()
        self.filename = self.downloader_gzip.filename
        self.assertTrue(os.path.isfile(self.downloader_gzip.generate_tmp_filename()))
        with open(self.filename, "r") as file:
            js = file.read()
            dict = json.loads(js)
            self.assertTrue(dict['gzipped'])
            file.close()


class ParseFeedTest(TestCase):
    def setUp(self):
        file_name = 'test.xml'
        self.valid_count = 2
        self.url_feed = '%s/xml' % settings.HTTPBIN_SERVER
        # Откуда берем тестовый файл
        self.test_file = os.path.join(settings.BASE_DIR, 'test', 'import_test_file', file_name)
        # self.test_file = os.path.join(settings.BASE_DIR, 'test', 'import_test_file', 'test_abmitad.xml')
        # Копируем файл в рабочую папку
        self.filename = os.path.join(settings.TMP_ROOT, '%s.yml' % 'test_parse_file')
        shutil.copyfile(self.test_file, self.filename)
        self.feed = mixer.blend(models.Feed,
                                xml_url=self.url_feed,
                                run_auto=True,
                                get_param_save=False)

    def tearDown(self):
        try:
            os.unlink(self.filename)
        except FileNotFoundError:
            pass

    def test_parse_feed(self):
        handler = Handler(feed=self.feed, filename=self.filename)
        handler.run()
        # lots = models.Lot.objects.filter(staturl=models.ENABLED)
        lots = models.Lot.objects.all()
        self.assertEqual(lots.count(), self.valid_count)
        lots = models.Lot.objects.filter(status=models.ENABLED)
        self.assertEqual(lots.count(), self.valid_count)
        lot_1 = lots.get(offer_id=1)
        self.assertEqual(lot_1.name, 'Xlash Fiber')
        self.assertEqual(lot_1.price, 690)
        self.assertEqual(lot_1.url, 'http%3A%2F%2Falmea.ru%2Fshop%2Flashes-and-hair%2Flashes%2Flashes_267.html')
