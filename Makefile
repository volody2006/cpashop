clean:
	find . -name "*.pyc" -exec rm -rf {} \;

run_test:
	coverage run --omit='~/.virtualenvs/*'  manage.py test -v=3 --settings=cpashop.test_settings
	coverage html -i

run_httpbin:
	gunicorn -b 127.0.0.1:8008 httpbin:app;

start_docker:
	sh ./run_docker.sh;

stop_docker:
	docker stop $$(docker ps -a -q);

rm_docker:
	docker rm $$(docker ps -a -q);


mail_console:
	python -m smtpd -n -c DebuggingServer localhost:1025;



