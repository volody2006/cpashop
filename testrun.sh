#!/usr/bin/env bash

# Перед запуском в виртуальном окружение подними сервер для тестов
# gunicorn -b 127.0.0.1:8008 httpbin:app
#DATABASE_URL=sqlite://:memory:
coverage run --omit='~/.virtualenvs/*'  manage.py test -v=3 --settings=cpashop.test_settings --failfast market goto_url import_lots
coverage html -i