# -*- coding: utf-8 -*-
import logging

from django.contrib.sites.models import Site
from django.db import models
from base.models import CommonAbstractModel

logger = logging.getLogger(__name__)


#

class ItemView(CommonAbstractModel):
    """
    Единичный просмотр
    """
    DEFAULT_SUBSTANCE = 0
    ITEM_LOT_VIEW = 1
    ITEM_CATALOG_VIEW = 2
    ITEM_ARHIV_LOT_VIEW = 3
    ITEM_CLICK_GOTO = 4

    SUBST_OBJECT_VIEW = DEFAULT_SUBSTANCE
    SUBST_PHONE_VIEW = 1
    SUBST_ADDRESS_VIEW = 2
    SUBST_SITE_VIEW = 3
    SUBST_USER_META = 4
    # TODO УДАЛИТЬ ПЯТЕРКИ ИЗ РЕДИСА (SUBST_SHOP_CUSTOMER = 5, 8, 9)
    SUBST_SHOP_CUSTOMER = 7

    ip = models.IPAddressField()
    user = models.ForeignKey(User, blank=True, null=True)
    url = models.URLField(blank=True, max_length=1024)
    referrer = models.URLField(blank=True, max_length=1024)
    substance = models.PositiveIntegerField(default=DEFAULT_SUBSTANCE,
                                            choices=(
                                                (SUBST_OBJECT_VIEW, u'просмотр объекта БД'),
                                                (SUBST_PHONE_VIEW, u'просмотр телефона'),
                                                (SUBST_ADDRESS_VIEW, u'просмотр адресов магазина'),
                                                (SUBST_SITE_VIEW, u'переход на сайт'),
                                            ))
    department = models.ForeignKey(Department, default=get_current_dep_id)

    @property
    def is_customer_view(self):
        """
        Определяет является ли текущий просмотр просмотром от потенциального покупателя
        """

        # просмотры с офисного ip - все служебные
        if self.ip in settings.INTERNAL_IPS:
            return False

        # просмотры всяких извратных юзеров - тоже служебные
        u = self.user
        if u and (u.is_superuser or u.is_staff or u.is_agent or u.is_moderator):
            return False

        # если смотрит владелец сущности - считаем просмотр служебным тоже
        obj = self.content_object
        creator = obj.get_creator
        if self.user_id == creator.id:
            return False

        # если сущность - товар (лот), то требуем чтобы он был опубликован
        # неопубликованный товар купить нельзя - и считать такой просмотр полноценным неправильно
        from market.models import Lot, ENABLED
        if isinstance(obj, Lot) and obj.status != ENABLED:
            return False

        return True

    def __unicode__(self):
        return 'ItemView subst=%(substance)s dep_id=%(department_id)s ctid=%(content_type_id)s oid=%(object_id)s created=%(date_created)s' % self.__dict__

    class Meta(object):
        verbose_name = u'просмотр'
        verbose_name_plural = u'Просмотры'
