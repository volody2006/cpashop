# -*- coding: utf-8 -*-

import logging

from celery.task import task

from utils.cleand_dir import CleanDirectory

logger = logging.getLogger(__name__)


@task
def run_creand_dir():
    logger.info('Чистим картинки от мусора...')
    c = CleanDirectory()
    c.start()
