# -*- coding: utf-8 -*-

import logging
import os

from cpashop import settings
from market.models import Image

# Очищаем директорию от неиспользуемых файлов (картинок)

logger = logging.getLogger(__name__)


class CleanDirectory:
    def __init__(self):
        self.base_dir = settings.BASE_DIR
        self.media_path = os.path.join(self.base_dir, 'media')
        self.seller_path = os.path.join(self.media_path, 'seller')

    def start(self, path_dir=None):
        if path_dir is None:
            path_dir = self.seller_path
        for name in os.listdir(path_dir):
            path = os.path.join(path_dir, name)
            if os.path.isfile(path):
                # print (path, name)
                image_path = path.replace(self.seller_path, 'seller')
                try:
                    Image.objects.get(image=image_path)
                except Image.DoesNotExist:
                    logger.info('Файл %s не найден в базе. Удаляем...', path)
                    os.remove(path)
                    # print (image, name)
            else:
                self.start(path)

# path = '/home/user/program/MyProject/cpashop/media/seller/1/16/05/16055bcf-b53d-473e-8827-647d4bc32efd.jpg'
# path = 'seller/1/16/05/16055bcf-b53d-473e-8827-647d4bc32efd.jpg'
