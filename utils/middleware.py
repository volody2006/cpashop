# from .shortcuts import get_current_site
from template_selection.managers import custom_get_current_site


class CurrentSiteMiddleware(object):
    """
    Middleware that sets `site` attribute to request object.
    """

    def process_request(self, request):
        # print(request.META['HTTP_HOST'])
        request.site = custom_get_current_site(request)
