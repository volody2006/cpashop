# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.models import User
from django.utils import six
from django.utils.crypto import salted_hmac
from django.utils.encoding import force_text, force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

logger = logging.getLogger(__name__)


class UserTokenGenerator:
    key_salt = "django.contrib.auth.tokens.PasswordResetTokenGenerator"

    def make_token(self, user):
        """
        Возвращает токен для данного юзера
        """
        return self._make_token(user)

    def check_token(self, user, token):

        if not (user and token):
            return False
        token = token.replace('"', '')
        if self.make_token(user) == token:
            return True
        return False

    def _make_token(self, user):
        timestamp = 2100
        hash = salted_hmac(
            self.key_salt,
            self._make_hash_value(user, timestamp),
        ).hexdigest()[::2]
        return '%s-%s' % (self.get_uid(user), hash)

    def _make_hash_value(self, user, timestamp):
        date_joined = '' if user.date_joined is None else user.date_joined.replace(microsecond=0, tzinfo=None)
        return (six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(date_joined))

    def get_user(self, uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring on Python 3
            uid = int(force_text(urlsafe_base64_decode(uidb64)))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def get_uid(self, user):
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        return uid.decode()

    def parse_token(self, token):
        try:
            token = token.replace('"', '')
            uid, hasht = token.split("-")
            if not uid or not hasht:
                return None, None
        except (ValueError,) as e:
            print(e)
            uid, hasht = None, None
        return uid, hasht

    def valide_token(self, token):
        try:
            token = token.replace('"', '')
            uid, hasht = self.parse_token(token)

            if not uid or not hasht:
                print('not uid or not hasht')
                return False

            if not self.get_user(uid):
                print('not user')
                return False

            # print(self.make_token(self.get_user(uid)), str(token))
            if str(self.make_token(self.get_user(uid))) == str(token):
                return True
        except AttributeError:
            pass
        return False
