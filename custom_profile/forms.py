# -*- coding: utf-8 -*-
import re

from django import forms
# from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from custom_profile.models import UserProfile


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['admitad_identifier', 'admitad_secret_key', ]


class RegistrationForm(forms.ModelForm):
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
        'charset': _("May contain only Russian and Latin letters, numbers and symbols « »,«_» и «-»"),
        'password_length': _("Password 6 or more characters."),
    }

    username = forms.CharField(
        label=_('Username'),
        required=True,
        max_length=25,
    )

    email = forms.EmailField(
        label='Email',
        required=True,
    )
    password = forms.CharField(
        label=_("Password"),
        required=True,
        widget=forms.PasswordInput(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    def __init__(self, *args, **kwrds):
        super(RegistrationForm, self).__init__(*args, **kwrds)
        # if settings.CAPCHA:
        #     self.fields['captcha'] = supercaptcha.CaptchaField(label=u'Код с картинки')

    def clean_username(self):
        user = self.cleaned_data['username']
        if user and re.compile(r'^[a-zA-Z0-9а-яА-ЯёЁ][-a-zA-Z0-9а-яА-ЯёЁ_ ]*[a-zA-Z0-9а-яА-ЯёЁ]$').search(user):
            if User.objects.filter(username__iexact=user).count() > 0:
                raise forms.ValidationError(self.error_messages['duplicate_username'])
        else:
            raise forms.ValidationError(self.error_messages['charset'])
        return user

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email__iexact=email).count() > 0:
            raise forms.ValidationError(self.error_messages['duplicate_email'])
        return email

    def clean_password(self):
        password = self.cleaned_data['password']

        if password and not re.compile(r'^[^ \t\n\r\f\v]{6,}$').search(password):
            raise forms.ValidationError(self.error_messages['password_length'])

        return password

    def clean_password2(self):
        """Check that the two password entries match.
        :return str password2: cleaned password2
        :raise forms.ValidationError: password2 != password1
        """
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password and password2 and password != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

    def save(self, commit=True, is_webmaster=False):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
            if is_webmaster:
                profile = UserProfile.objects.get(user=user)
                profile.is_webmaster = is_webmaster
                profile.save()
        return user
