# -*- coding: utf-8 -*-
import logging
from urllib.parse import urlencode, quote_plus

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import login, PasswordChangeDoneView, PasswordChangeView, \
    PasswordResetView, LoginView, LogoutView, PasswordResetDoneView, PasswordResetCompleteView, PasswordResetConfirmView
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http.response import HttpResponseRedirect
# from django.template import loader
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.views.generic import TemplateView
from django.views.generic.edit import FormView, UpdateView

from custom_profile.forms import RegistrationForm, UserProfileForm
from custom_profile.models import UserProfile
from custom_profile.tasks import task_send_email
from custom_profile.utils import generate_username
from market.mixpanel.utils import GetTemplateNamesMixin
from template_selection.managers import custom_get_current_site

logger = logging.getLogger(__name__)


class UserRegisterLending():
    pass


class UserProfileTemplateView(LoginRequiredMixin, GetTemplateNamesMixin, TemplateView):
    template = 'user_profile/user_profile_detail.html'

    # model = UserProfile

    # queryset = Lot.objects.all()

    def get_context_data(self, **kwargs):
        context = super(UserProfileTemplateView, self).get_context_data(**kwargs)

        # context['related_more_lots'] = self.get_related()[8:16]
        # context['gfu'] = self.object.get_full_url(custom_get_current_site(self.request).mysite)

        return context


class UserProfileEditUpdateView(LoginRequiredMixin, GetTemplateNamesMixin, UpdateView):
    model = UserProfile
    template = 'user_profile/user_profile_update.html'
    form_class = UserProfileForm

    def get_object(self):
        return self.request.user.userprofile

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(UserProfileEditUpdateView, self).get_form_kwargs()
        return kwargs

    def form_valid(self, form):
        # form.save()
        self.object = form.save()
        return super(UserProfileEditUpdateView, self).form_valid(form)
        # return HttpResponseRedirect(self.get_success_url())


class CustomLoginView(GetTemplateNamesMixin, LoginView):
    template = "registration/login.html"

    def get(self, request, *args, **kwargs):
        get_param = self.request.GET

        return self.render_to_response(self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(CustomLoginView, self).get_context_data(**kwargs)
        current_site = custom_get_current_site(self.request)
        context.update({
            self.redirect_field_name: self.get_redirect_url(),
            'site': current_site,
            'site_name': current_site.name,
        })
        context.update(self.request.GET)
        if self.extra_context is not None:
            context.update(self.extra_context)
        return context


class CustomLogoutView(GetTemplateNamesMixin, LogoutView):
    next_page = '/'
    template = "registration/logout.html"


class RegisterFormView(GetTemplateNamesMixin, FormView):
    form_class = RegistrationForm

    # Ссылка, на которую будет перенаправляться пользователь в случае успешной регистрации.
    success_url = reverse_lazy('main_view')
    template = "registration/register.html"

    def register_user_lp(self):
        if self.request.GET.get('email'):
            email = self.request.GET.get('email')
            try:
                validate_email(email)
            except ValidationError as e:
                logger.info('На регистрацию отправлен не правильный email %s' % email)
                return False
            name = self.request.GET.get('name') if self.request.GET.get('name') else None
            self.email = email
            if User.objects.filter(email__iexact=email).count() > 0:
                logger.info('Пользователь есть! %s' % self.email)
                self.resetting_password()
                return True
            else:
                username = generate_username(email)
                password = User.objects.make_random_password()
                if name:
                    try:
                        name = name.replace('  ', ' ').strip()
                        first_name, last_name = name.split(' ', 1)
                        first_name, last_name = first_name[:30], last_name[:30]
                    except ValueError:
                        first_name, last_name = name[:30], ''
                else:
                    first_name = last_name = ''
                user = User.objects.create_user(username=username, email=email, password=password,
                                                first_name=first_name, last_name=last_name)
                userprofile = user.userprofile
                userprofile.referer_url = self.request.COOKIES.get('referer_url', '')
                userprofile.reg_type = userprofile.LENDINGS
                userprofile.save()

                context = {
                    'email': email,
                    'domain': custom_get_current_site(self.request).domain,
                    'site_name': custom_get_current_site(self.request).name,
                    'user_full_name': user.get_full_name() if user.get_full_name() else user.username,
                    'username': user.username,
                    'password': password,
                    'protocol': 'https' if self.request.is_secure() else 'http',
                }
                if settings.CELERY_START:
                    task_send_email.delay(
                        subject_template_name='dashboard/registration/password_lending_subject.txt',
                        email_template_name='dashboard/registration/password_register_email.html',
                        context=context, from_email=None,
                        to_email=email, html_email_template_name=None)
                else:
                    task_send_email(
                        subject_template_name='dashboard/registration/password_lending_subject.txt',
                        email_template_name='dashboard/registration/password_register_email.html',
                        context=context, from_email=None,
                        to_email=email, html_email_template_name=None)
                    return True
            return False

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """
        if self.register_user_lp():
            url_login = reverse_lazy('login')
            get_param = self.request.GET
            result = urlencode(get_param, quote_via=quote_plus)

            return HttpResponseRedirect(url_login + '?' + result)

        return self.render_to_response(self.get_context_data())

    def get_users(self, email):
        """Given an email, return matching user(s) who should receive a reset.

        This allows subclasses to more easily customize the default policies
        that prevent inactive users and users with unusable passwords from
        resetting their password.
        """
        active_users = User._default_manager.filter(**{
            '%s__iexact' % User.get_email_field_name(): email,
            'is_active': True,
        })
        return (u for u in active_users if u.has_usable_password())

    def resetting_password(self, domain_override=None,
                           subject_template_name='dashboard/registration/password_lending_subject.txt',
                           email_template_name='dashboard/registration/password_reset_email_lending.html',
                           token_generator=default_token_generator,
                           from_email=None, html_email_template_name=None,
                           extra_email_context=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        email = self.email
        for user in self.get_users(email):
            if not domain_override:
                current_site = custom_get_current_site(self.request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            context = {
                'email': email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user_full_name': user.get_full_name() if user.get_full_name() else user.username,
                'username': user.username,
                'token': token_generator.make_token(user),
                'protocol': 'https' if self.request.is_secure() else 'http',
            }
            if extra_email_context is not None:
                context.update(extra_email_context)

            if settings.CELERY_START:
                task_send_email.delay(
                    subject_template_name, email_template_name, context, from_email,
                    email, html_email_template_name=html_email_template_name,
                )
            else:
                task_send_email(
                    subject_template_name, email_template_name, context, from_email,
                    email, html_email_template_name=html_email_template_name,
                )

    def form_valid(self, form):
        # Регистрация для обычных пользователей
        form.save(is_webmaster=False)
        user = authenticate(username=form.cleaned_data['username'],
                            password=form.cleaned_data['password'])
        userprofile = user.userprofile
        userprofile.referer_url = self.request.COOKIES.get('referer_url', '')
        userprofile.reg_type = userprofile.NATIVE
        userprofile.save()
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())


class CustomPasswordResetDoneView(GetTemplateNamesMixin, PasswordResetDoneView):
    template = 'registration/password_reset_done.html'


class CustomPasswordResetCompleteView(GetTemplateNamesMixin, PasswordResetCompleteView):
    template = 'registration/password_reset_complete.html'


class CustomPasswordResetConfirmView(GetTemplateNamesMixin, PasswordResetConfirmView):
    template = 'registration/password_reset_confirm.html'


class CustomPasswordChangeDoneView(GetTemplateNamesMixin, PasswordChangeDoneView):
    template = 'registration/password_change_done.html'


class CustomPasswordChangeView(GetTemplateNamesMixin, PasswordChangeView):
    success_url = reverse_lazy('password_change_done')
    template = 'registration/password_change_form.html'


class CustomPasswordResetView(GetTemplateNamesMixin, PasswordResetView):
    template = 'registration/password_reset_form.html'
