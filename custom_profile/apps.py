# -*- coding: utf-8 -*-

from django.apps import AppConfig


class CustomProfileConfig(AppConfig):
    name = 'custom_profile'
