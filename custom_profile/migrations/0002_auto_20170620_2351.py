# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-20 23:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_profile', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='admitad_identifier',
            field=models.CharField(blank=True, default='', help_text='Admitad API: Идентификатор', max_length=31, null=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='admitad_secret_key',
            field=models.CharField(blank=True, default='', help_text='Admitad API: Секретный ключ', max_length=31, null=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='last_check_adm_api',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='verification_code',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
