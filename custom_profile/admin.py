# -*- coding: utf-8 -*-
from django.contrib import admin

# Register your models here.
from custom_profile.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'id', 'referer_url'
                    # 'verification_code'
                    )
    list_filter = ['reg_type',]
    # raw_id_fields = ['user']
    # search_fields = ['', ]
    # list_display = ('id',
    #                 )


admin.site.register(UserProfile, UserProfileAdmin)
