# -*- coding: utf-8 -*-
import hashlib
import logging

from django.contrib.auth.models import User

from custom_profile.models import create_user_profile, UserProfile

logger = logging.getLogger(__name__)


def run_profile():
    # Служебная функция
    for user in User.objects.all():
        print(user)
        create_profile(user)


def create_profile(user):
    # user = User.objects.get(pk=user_id)
    if UserProfile.objects.filter(user=user).exists():
        # if user.userprofile:
        logger.info('Профайл пользователя существует %s', user)
        check_profile(user.userprofile)
        return
    create_user_profile(sender=True, instance=user, created=True)


def check_profile(profile):
    if not profile.verification_code:
        profile.get_verification_code()


def get_md5_hexdigest(email):
    """
    Returns an md5 hash for a given email.
    The length is 30 so that it fits into Django's ``User.username`` field.
    """
    if isinstance(email, str):  # for py3
        email = email.encode('utf-8')
    return hashlib.md5(email).hexdigest()[0:30]


def generate_username(email):
    """
    Generates a unique username for the given email.
    The username will be an md5 hash of the given email. If the username exists
    we just append `a` to the email until we get a unique md5 hash.
    """
    try:
        return User.objects.get(email=email.lower()).username
    except User.DoesNotExist:
        pass

    # username = get_md5_hexdigest(email)
    username = email.split('@', 1)[0][:25]
    c = User.objects.filter(username=username).count()
    if c > 0:
        username += str(c + 1)

    found_unique_username = False
    while not found_unique_username:
        try:
            User.objects.get(username=username)
            email = '{0}a'.format(email.lower())
            username = get_md5_hexdigest(email)
        except User.DoesNotExist:
            found_unique_username = True
    return username
