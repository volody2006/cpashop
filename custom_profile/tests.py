# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.test import TestCase
# Create your tests here.
from django.urls import reverse

from custom_profile.models import UserProfile
from custom_profile.views import RegisterFormView

USER_NAME = 'StalinIs'
USER_PASSWORD = 'ItisPassword'
USER_EMAIL = 'stalin@stalingrad.us'


class UserRegistrationTest(TestCase):
    def setUp(self):
        self.url_register = reverse('register')

    def tearDown(self):
        self.client.logout()

    def test_registration(self):
        self.client.get(self.url_register)
        data = {
            'username': USER_NAME,
            'password': USER_PASSWORD,
            'password2': USER_PASSWORD,
            'email': USER_EMAIL,
            # 'is_webmaster': '1'
        }

        #
        response = self.client.post(self.url_register, data, )
        self.assertEqual(response.resolver_match.func.__name__, RegisterFormView.as_view().__name__)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('main_view'))
        user = User.objects.get(username=USER_NAME)
        profile = UserProfile.objects.get(user=user)
        self.assertFalse(profile.is_webmaster)
        self.client.logout()
        self.assertTrue(self.client.login(username=USER_NAME, password=USER_PASSWORD))
