# -*- coding: utf-8 -*-
from django.conf.urls import url

from custom_profile import views

urlpatterns = [
    url(r'^profile/$', views.UserProfileTemplateView.as_view(), name='profile'),
    url(r'^profile/edit$', views.UserProfileEditUpdateView.as_view(), name='profile_update'),

    url(r'^login/', views.CustomLoginView.as_view(), name='login'),
    url(r'^logout/', views.CustomLogoutView.as_view(), name='logout'),
    url(r'^register/', views.RegisterFormView.as_view(), name='register'),
    url(r'^password/$', views.CustomPasswordChangeView.as_view(), name='password_change'),
    url(r'^password/done/$', views.CustomPasswordChangeDoneView.as_view(), name='password_change_done'),
    url(r'^password/reset/$', views.CustomPasswordResetView.as_view(), name='password_reset'),
    url(r'^password/password/reset/done/$', views.CustomPasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^password/reset/complete/$', views.CustomPasswordResetCompleteView.as_view(), name='password_reset_complete'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        views.CustomPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    # url(r'^password/reset/confirm/(?P<uidb64>.+)/(?P<token>.+)/$',
    #     views.CustomPasswordResetConfirmView.as_view(), name='password_reset_confirm'),

]
