# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.urls import reverse
from django.utils import timezone

from custom_profile.managers import UserTokenGenerator
from market.models import Template, MySite

logger = logging.getLogger(__name__)

user_token_gen = UserTokenGenerator()


def get_absolute_url():
    return reverse('profile')


class UserProfile(models.Model):
    NATIVE = 1
    LENDINGS = 2

    REG_TYPE_CHOICES = (
        (NATIVE, 'Регистрация на сайте'),
        (LENDINGS, 'Регистрация через лендинг'),
    )

    # This field is required.
    user = models.OneToOneField(User)
    verification_code = models.CharField(max_length=200,
                                         null=True)
    # editable=False, unique=True)
    is_webmaster = models.BooleanField(default=False)

    # default_templates = models.ForeignKey(Template, default=Template.objects.first(),
    #                                       help_text='Шаблон по умолчанию для всех сайтов')
    # Язык по умолчанию
    # avatar
    admitad_identifier = models.CharField(default='', max_length=31, null=True, blank=True,
                                          help_text='Admitad API: Идентификатор')
    admitad_secret_key = models.CharField(default='', max_length=31, null=True, blank=True,
                                          help_text='Admitad API: Секретный ключ')
    last_check_adm_api = models.DateTimeField(null=True, blank=True)

    reg_type = models.PositiveSmallIntegerField(choices=REG_TYPE_CHOICES,
                                           default=NATIVE, db_index=True,
                                           verbose_name='Тип регистрации', editable=False)
    # reg_url = models.URLField(max_length=2000, null=True, blank=True, help_text='Страница регистрации', editable=False)
    referer_url = models.TextField(blank=True, verbose_name=u'Реф урл с которого пользователь пришел')

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('profile')

    def update_last_check_adm_api(self):
        self.last_check_adm_api = timezone.now()
        self.save()

    def get_token_gen(self):
        code = user_token_gen.make_token(self.user)
        return code

    def get_verification_code(self):
        # if self.verification_code:
        #     return self.verification_code
        self.verification_code = self.get_token_gen()
        self.save()
        return self.get_token_gen()

    def get_templates(self):
        return Template.objects.filter(user=self.user)

    def get_site(self):
        return MySite.objects.filter(user=self.user)


#
# class UserSite(models.Model):
#     user = models.ForeignKey(User)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile = UserProfile.objects.create(user=instance)
        profile.get_verification_code()
        logger.info('Профайл пользователя создан! %s', profile)


post_save.connect(create_user_profile, sender=User)
