# -*- coding: utf-8 -*-

from django.conf.urls import url

# from crm.views import *
from crm import views
from my_admitad_api.views import AdmCampaignDetailView, AdmCampaignListView

urlpatterns = [
    # url(r'^login/', CustomLoginView.as_view(), name='login_webmaster'),
    # url(r'^logout/', CustomLogoutView.as_view(), name='logout_webmaster'),
    # url(r'^register/', WebmasterRegisterFormView.as_view(), name='register_webmaster'),
    url(r'^$', views.MainCRMView.as_view(), name='main_crm'),
    # url(r'^$', MainCRMView.as_view(), name='test'),
    # url(r'^$', CrmMainPageView.as_view(), name='crm_main_view'),
    # url(r'^dashboard/$', CrmDashboardPageView.as_view(), name='crm_dashboard_view'),
    url(r'mysite/$', views.MySiteListView.as_view(), name='mysite_list'),
    # url(r'mysite/add/$', MySiteCreate.as_view(), name='mysite_add'),
    url(r'mysite/(?P<pk>[0-9]+)/$', views.MySiteView.as_view(), name='mysite'),
    url(r'mysite/(?P<pk>[0-9]+)/edit$', views.MySiteUpdate.as_view(), name='mysite_update'),
    url(r'mysite/(?P<pk>[0-9]+)/delete/$', views.MySiteDeleteView.as_view(), name='mysite_delete'),
    url(r'mysite/(?P<pk>[0-9]+)/add/$', views.AddSellerView.as_view(), name='add_seller'),
    url(r'mysite/(?P<pk>[0-9]+)/validate/admitad$', views.validate_mysite_admitad, name='mysite_validate_admitad'),

    url(r'template/$', views.MyTemplateListView.as_view(), name='template_list'),
    url(r'template/(?P<pk>[0-9]+)/$', views.MyTemplateView.as_view(), name='template'),
    url(r'template/(?P<pk>[0-9]+)/edit$', views.MyTemplateUpdate.as_view(), name='template_update'),
    url(r'template/(?P<pk>[0-9]+)/delete/$', views.MyTemplateDelete.as_view(), name='template_delete'),

    url(r'deeplink/add/$', views.MyDeeplinkCreateView.as_view(), name='deeplink_create'),
    url(r'deeplink/$', views.MyDeeplinkListView.as_view(), name='deeplink_list'),
    url(r'deeplink/(?P<pk>[0-9]+)/edit$', views.MyDeeplinkUpdateView.as_view(), name='deeplink_update'),
    url(r'deeplink/(?P<pk>[0-9]+)/delete/$', views.MyDeeplinkDeleteView.as_view(), name='deeplink_delete'),

    url(r'seller/$', AdmCampaignListView.as_view(), name='admcampaign_list'),
    url(r'seller/(?P<pk>[0-9]+)/$', AdmCampaignDetailView.as_view(), name='admcampaign_detail'),

    url(r'ajax/seller_delete/(?P<pk>[0-9]+)/$', views.seller_delete, name="seller_delete"),

    # url(r'test/', views.TemplateView.as_view(template_name='example/test.html'), name='test'),
    url(r'test/', views.TestView.as_view(template_name='example/test.html'), name='test'),

]
