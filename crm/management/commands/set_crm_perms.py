# coding: utf-8
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand

from custom_profile.models import UserProfile


# from catalog.models import Category
# from market.models import Lot
# from reviews.models import Review
# from users.models import User
# from ads.models import Service
# from balance.models import Transaction
# from yitems.models import YItem
# from departments.models import Department
# from ads.models import TariffZoneItem
# from crm.models import CrmMonitoringCommands


def create_permission(model, codename, name):
    perm, created = Permission.objects.get_or_create(content_type=ContentType.objects.get_for_model(model),
                                                     codename=codename,
                                                     defaults={'name': 'CRM: %s' % name})

    if not created and not perm.name.startswith('CRM'):
        perm.name = 'CRM: %s' % perm.name
        perm.save()


class Command(BaseCommand):
    """
    выставляем права необходимые для работы с црм
    """

    help = 'set users perms for crm'

    def handle(self, *args, **kwargs):
        pass

        create_permission(UserProfile, 'webmaster', 'Вебмастер')
        #
        # create_permission(Category, 'show_tree', u'Просмотр дерева категорий')
        #
        # create_permission(User, 'show_user_list', u'Просмотр списка пользователей')
        #
        # create_permission(Service, 'show_servise_list', u'Просмотр завершающихся тарифов')
        #
        # create_permission(Transaction, 'show_transactions', u'Просмотр транзакций')
        #
        # create_permission(YItem, 'show_ybdlist', u'Просмотр данных Яндекс БД')
        #
        # create_permission(TariffZoneItem, 'can_turn_on_tariffs', u'Может включать тарифы магазинов')
        #
        # create_permission(TariffZoneItem, 'turn_on_tariff_all_shops', u'Может включать тариф во всех городах')
        #
        # for d in Department.objects.all():
        #     create_permission(Department, 'turn_on_tariff_in_%s' % d.city_en,
        # u'Может включать тариф в %s' % d.city_form6)
        #
        # create_permission(CrmMonitoringCommands, 'have_monitoring_access', u'Есть доступ к мониторингу')
        #
        # create_permission(Lot, 'promo_products', u'Настройка промо продуктов')
        #
        # create_permission(Review, 'show_reviews', u'Отзывов просмотр')
        # create_permission(Lot, 'show_deals', u'Сделок просмотр')
