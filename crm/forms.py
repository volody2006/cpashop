# -*- coding: utf-8 -*-
from django import forms
# from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from market.models import MySite, Template, Deeplink


class MarketModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class UserMixin:
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(UserMixin, self).__init__(*args, **kwargs)


class SomeForm(forms.Form):
    def __init__(self, user, *args, **kwargs):  # переопределяем __init__ формы
        super(SomeForm, self).__init__(*args, **kwargs)  # выполняем __init__ из ModelForm
        if user.is_authenticated:
            self.fields['template'].queryset = Template.objects.filter(user=user)

    doname = forms.CharField()
    template = forms.ModelChoiceField(queryset=None)
    message = forms.CharField(widget=forms.Textarea)


class _MySiteForm(forms.Form):
    doname = forms.CharField()
    template = MarketModelChoiceField(queryset=None)
    message = forms.CharField(widget=forms.Textarea)

    def __init__(self, user, *args, **kwargs):
        super(_MySiteForm, self).__init__(*args, **kwargs)
        self.fields['template'].queryset = Template.objects.filter(user=user)
        # self.fields['source'].queryset = models.Account.active.all()
        # self.fields['destination'].queryset = models.Account.active.filter(primary_user=user)


class MySiteForm(UserMixin, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MySiteForm, self).__init__(*args, **kwargs)
        self.fields['template'].queryset = Template.objects.filter(Q(user=self.user) | Q(user=None),
                                                                   hidden=False,
                                                                   crm_template=False)

    def clean_description(self):
        data = self.cleaned_data['description']
        if len(data) < 100:
            raise forms.ValidationError(_("Минимальная длина сообщения должна быть 100 символов!"))

        if len(data) > 2000:
            raise forms.ValidationError(_("Максимальная длина сообщения должна быть 2000 символов!"))
        return data

    class Meta:
        model = MySite
        fields = ['verify_admitad', 'verify_yandex', 'web_property_id', 'verify_bing', 'template', 'description',
                  'query']


class MyTemplateForm(UserMixin, forms.ModelForm):
    class Meta:
        model = Template
        fields = ['name', 'folder', 'paginate_by', 'number_of_categories', 'number_of_subcategories']


class DeeplinkForm(UserMixin, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DeeplinkForm, self).__init__(*args, **kwargs)
        if self.user.is_superuser:
            self.fields['site'].queryset = MySite.objects.all()
        else:
            self.fields['site'].queryset = MySite.objects.filter(user=self.user)
            # self.fields['site'].queryset = MySite.objects.filter(user=self.user)

    class Meta:
        model = Deeplink
        fields = ['url', 'seller', 'site', ]


class MySiteSellerAddForm(UserMixin, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MySiteSellerAddForm, self).__init__(*args, **kwargs)

    class Meta:
        model = MySite
        fields = ['sellers', ]
