# -*- coding: utf-8 -*-
import logging

from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponseRedirect, Http404, JsonResponse
# Create your views here.
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView, RedirectView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic.list import ListView

from crm.forms import MySiteForm, MyTemplateForm, DeeplinkForm, MySiteSellerAddForm
from custom_profile.forms import RegistrationForm
from market.mixpanel.utils import GetTemplateNamesMixin, GetObjectUserMixin, NotCRMMixin
from market.models import MySite, Template, Seller, Deeplink
from my_admitad_api.models import AdmCampaignUser

logger = logging.getLogger(__name__)


class TestRedirectView(RedirectView):
    url = None


class TestView(NotCRMMixin, LoginRequiredMixin, GetObjectUserMixin, DetailView):
    template = 'example/test.html'
    model = MySite

    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)
        context['seller_list'] = Seller.objects.filter(site=self.object)
        context['sellers'] = Seller.objects.filter(adm_campaign__isnull=False)
        return context


class MainCRMView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, TemplateView):
    template = "index.html"


def is_staff(login_url=None, raise_exception=True):
    """
    Decorator for views that checks whether a user is in staff, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """

    def check_perms(user):
        # First check if the user has the permission (even anon users)
        if user.is_staff:
            return True
            # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
            # As the last resort, show the login form
        return False

    return user_passes_test(check_perms, login_url=login_url)


class MySiteView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, DetailView):
    template = 'mysite/mysite_detail.html'
    model = MySite

    def get_context_data(self, **kwargs):
        context = super(MySiteView, self).get_context_data(**kwargs)
        sellers = self.object.sellers.all()
        campaign = AdmCampaignUser.objects.filter(user=self.request.user,
                                                  website=self.object.adm_website
                                                  )
        context['seller_all'] = sellers
        context['campaigns'] = campaign
        return context


class MySiteListView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, ListView):
    template = 'mysite/mysite_list.html'
    model = MySite
    paginate_by = None

    def get_queryset(self):
        user = self.request.user
        # if user.is_superuser:
        #     return MySite.objects.all()
        return MySite.objects.filter(user=user)

    def get_context_data(self, **kwargs):
        context = super(MySiteListView, self).get_context_data(**kwargs)
        context['verification_code'] = self.request.user.userprofile.get_verification_code()
        return context


class MySiteUpdate(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, UpdateView):
    # permission_required = 'custom_profile.webmaster'
    model = MySite
    template = 'mysite/mysite_update1.html'
    form_class = MySiteForm

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(MySiteUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('mysite', args=[self.object.pk, ])


class MySiteDeleteView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin,
                       GetObjectUserMixin, DeleteView):
    # permission_required = 'custom_profile.webmaster'
    template = 'mysite/mysite_confirm_delete.html'
    model = MySite
    success_url = reverse_lazy('mysite_list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.is_crm:
            # Срм сайт нельзя удалить. )))
            return Http404
        success_url = self.get_success_url()
        self.object.user = None
        self.object.verify_admitad = None
        self.object.verify_yandex = None
        self.object.web_property_id = None
        self.object.save()
        return HttpResponseRedirect(success_url)


class CrmMainPageView(NotCRMMixin, GetTemplateNamesMixin, TemplateView):
    """
    главная
    """
    template = 'crm_admin/paper_dashboard/base.html'


class WebmasterRegisterFormView(NotCRMMixin, GetTemplateNamesMixin, FormView):
    '''
    Для веб мастеров отдельная форма регистрации. Только в этой вьюхе можно получить доступ к необходимым правам
    '''
    form_class = RegistrationForm

    # Ссылка, на которую будет перенаправляться пользователь в случае успешной регистрации.
    success_url = reverse_lazy('crm_dashboard_view')
    # success_url = '/'
    template = "crm_admin/register_webmaster.html"

    def form_valid(self, form):
        # Регистрация по этой вьюхе всегда делает веб мастеров.

        form.save(is_webmaster=True)
        user = authenticate(username=form.cleaned_data['username'],
                            password=form.cleaned_data['password'])
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())


class MyTemplateListView(NotCRMMixin, LoginRequiredMixin, PermissionDenied, GetTemplateNamesMixin, ListView):
    template = 'template/template_list.html'
    model = Template
    paginate_by = None

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Template.objects.all()
        return Template.objects.filter(user=self.request.user, hidden=False, crm_template=False)


class MyTemplateView(NotCRMMixin, LoginRequiredMixin, PermissionDenied, GetTemplateNamesMixin, View):
    template = 'template/template.html'
    model = Template

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Template.objects.all()
        return Template.objects.filter(user=self.request.user, hidden=False, crm_template=False)


class MyTemplateUpdate(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, UpdateView):
    model = Template
    template = 'template/template_update.html'
    form_class = MyTemplateForm

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """

        kwargs = super(MyTemplateUpdate, self).get_form_kwargs()

        kwargs.update({'user': self.request.user})
        return kwargs


class MyTemplateDelete(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, DeleteView):
    template = 'template/template_confirm_delete.html'
    model = Template
    success_url = reverse_lazy('template_list')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        # self.object.delete()
        return HttpResponseRedirect(success_url)


class MySiteDeleteSeller(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, DeleteView):
    template = 'template/template_confirm_delete.html'
    model = Template
    success_url = reverse_lazy('template_list')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)


class AjaxableResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return JsonResponse(data)
        else:
            return response


@login_required(login_url='/login/')
# @is_staff()
@csrf_exempt
def seller_delete(request, pk):
    class Form(forms.Form):
        seller = forms.IntegerField(required=True)

        def clean_seller(self):
            value = self.cleaned_data['seller']
            if not Seller.objects.filter(id=value).exists():
                raise forms.ValidationError('It seems seller does not exist')
            # Только владелиц и суперюзер могут удалять программы с сайта
            if not request.user == MySite.objects.get(pk=pk).user:
                if not request.user.is_superuser:
                    raise forms.ValidationError('It seems seller does not exist')
            return value

    form = Form(request.POST or None)
    if form.is_valid():
        #
        seller = Seller.objects.get(pk=form.cleaned_data['seller'])
        mysite = MySite.objects.get(pk=pk)
        mysite.sellers.remove(seller)
        json = {'status': 'ok'}
    else:
        # json = form.errors.as_json()
        json = {'error': 'Ошибка удаления записи!'}
    # print(json)
    return JsonResponse(json, safe=False)


class MyDeeplinkView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, DetailView):
    template = 'mysite/mysite_detail.html'
    model = Deeplink

    def get_context_data(self, **kwargs):
        context = super(MyDeeplinkView, self).get_context_data(**kwargs)
        context['seller_list'] = Seller.objects.filter(site=self.object)
        return context


class MyDeeplinkCreateView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, CreateView):
    model = Deeplink
    template = 'deeplink/deeplink_form.html'
    form_class = DeeplinkForm

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """

        kwargs = super(MyDeeplinkCreateView, self).get_form_kwargs()

        kwargs.update({'user': self.request.user})
        return kwargs


class MyDeeplinkListView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, ListView):
    template = 'deeplink/deeplink_list.html'
    model = Deeplink
    paginate_by = None

    def get_queryset(self):
        user = self.request.user
        # if user.is_superuser:
        #     return Deeplink.objects.all()
        return Deeplink.objects.filter(site__user=user)


class MyDeeplinkUpdateView(NotCRMMixin, LoginRequiredMixin, PermissionRequiredMixin, GetTemplateNamesMixin,
                           GetObjectUserMixin,
                           UpdateView):
    permission_required = 'custom_profile.webmaster'
    model = Deeplink
    template = 'deeplink/deeplink_form.html'
    form_class = DeeplinkForm

    def get_form_kwargs(self):
        kwargs = super(MyDeeplinkUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class MyDeeplinkDeleteView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, DeleteView):
    model = Deeplink
    success_url = reverse_lazy('deeplink_list')
    template = 'deeplink/deeplink_confirm_delete.html'


class AddSellerView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, GetObjectUserMixin, UpdateView):
    model = MySite
    form_class = MySiteSellerAddForm
    template = 'mysite/add_seller.html'

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(AddSellerView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, *args, **kwargs):

        from my_admitad_api.tasks import task_adm_add_seller
        if settings.CELERY_START:
            task_adm_add_seller.delay(self.object.pk)
        else:
            task_adm_add_seller(self.object.pk)
        return super(AddSellerView, self).form_valid(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('mysite', args=[self.object.pk, ])


class ValidateMySiteView(NotCRMMixin, LoginRequiredMixin, GetTemplateNamesMixin, TemplateView):
    success_url = None

    def validate(self, request, *args, **kwargs):
        """

        """
        # self.object = self.get_object()
        # self.object.delete()
        return HttpResponseRedirect(self.success_url)


@login_required(login_url='/login/')
def validate_mysite_admitad(request, pk):
    if not request.user.userprofile.admitad_identifier or not request.user.userprofile.admitad_secret_key:
        logger.info('Если нет апи ключей, надо добавить.')
        # Если нет апи ключей, надо добавить.
        return redirect(reverse_lazy('profile_update'))
    success_url = reverse_lazy('mysite', args=[pk, ])
    mysite = get_object_or_404(MySite, site_id=pk, user=request.user, manual_verification=False)
    if not settings.DEBUG:
        from my_admitad_api.tasks import adm_validate_site
        adm_validate_site(mysite.site_id)
    return redirect(success_url)
