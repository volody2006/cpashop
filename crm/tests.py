import unittest

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase
# Create your tests here.
from django.urls import reverse
from mixer.backend.django import mixer

from crm import views
from crm.views import WebmasterRegisterFormView
from custom_profile.forms import RegistrationForm
from custom_profile.models import UserProfile
from market.models import Template, MySite

USER_NAME = 'Stalin'
USER_PASSWORD = 'IamStalin'
USER_EMAIL = 'stalin@stalingrad.us'


@unittest.skip
class WebmasterRegistrationTest(TestCase):
    def setUp(self):
        self.url = reverse('register_webmaster')

    def tearDown(self):
        self.client.logout()

    def test_form_register(self):
        form_data = {'username': USER_NAME,
                     'password': USER_PASSWORD,
                     'password2': USER_PASSWORD,
                     'email': USER_EMAIL}
        form = RegistrationForm(data=form_data)
        self.assertTrue(form.is_valid())
        form.save(is_webmaster=True)
        user = User.objects.get(username=USER_NAME)
        self.assertEqual(user.email, USER_EMAIL)
        self.assertFalse(user.is_staff)
        self.assertTrue(user.is_active)

    def test_registration(self):
        data = {'username': USER_NAME,
                'password': USER_PASSWORD,
                'password2': USER_PASSWORD,
                'email': USER_EMAIL,
                # 'is_webmaster': '1'
                }
        self.client.get(self.url)

        #
        response = self.client.post(self.url, data, )
        self.assertEqual(response.resolver_match.func.__name__, WebmasterRegisterFormView.as_view().__name__)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('crm_dashboard_view'))
        user = User.objects.get(username=USER_NAME)
        profile = UserProfile.objects.get(user=user)
        self.assertTrue(profile.is_webmaster)
        self.client.logout()
        self.assertTrue(self.client.login(username=USER_NAME, password=USER_PASSWORD))


@unittest.skip
class CrmTest(TestCase):
    def setUp(self):
        self.url_register = reverse('register')
        tem = Template.objects.create(name=settings.ADMIN_FOLDER,
                                      folder=settings.ADMIN_FOLDER,
                                      hidden=True)
        mysite = mixer.blend(MySite, site=Site.objects.get(pk=1),
                             template=tem)


        # self.lots = mixer.cycle(5).blend(models.Lot, status=models.ENABLED, name=mixer.random)

    def tearDown(self):
        self.client.logout()

    def run_registration(self, tariff=None):
        """
        Помощник тестирования процесса регистрации

        Эмулирует HTTP-запросы к серверу через тестовый клиент Django и возвращает
        созданного при регистрации пользователя
        """

        url_dashboard = reverse('crm_dashboard_view')
        data = {'username': USER_NAME,
                'password': USER_PASSWORD,
                'password2': USER_PASSWORD,
                'email': USER_EMAIL,
                }
        self.client.get(self.url_register)
        self.client.post(path=self.url_register, data=data)
        self.client.logout()
        self.client.login(username=USER_NAME, password=USER_PASSWORD)
        resp = self.client.get(url_dashboard)
        user = resp.context['request'].user
        return user

    def test_view_mysite_list(self):
        self.run_registration()
        response = self.client.get(reverse('mysite_list'))
        self.assertEqual(response.resolver_match.func.__name__, views.MySiteListView.as_view().__name__)

    def test_mysite_list_not_login(self):
        response = self.client.get(reverse('mysite_list'))
        self.assertEqual(response.status_code, 302)
        redir_url = reverse('login') + '?next=' + reverse('mysite_list')
        self.assertEqual(response.url, redir_url)

    def test_dashboard_page(self):
        pass

    def test_webmaster_custom_site(self):
        # Вебмастер добавляет свои сайты
        pass
