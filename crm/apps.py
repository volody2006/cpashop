# -*- coding: utf-8 -*-
from django.apps import AppConfig


class CrmConfig(AppConfig):
    name = 'crm'
