# -*- coding: utf-8 -*-

import logging

from celery.schedules import crontab
from celery.task import task, periodic_task
from haystack.query import SearchQuerySet

from cpashop import settings
from market.models import Lot, Image, MySite, Category, ENABLED, Template
from menu.models import MenuItem

logger = logging.getLogger(__name__)


@periodic_task(run_every=crontab(hour=23, minute=1))
def generate_menu():
    for site in MySite.objects.all():
        generate_menu_site.delay(site.pk)


def get_template(site_id):
    try:
        mysite = MySite.objects.get(pk=site_id)
    except MySite.DoesNotExist:
        from django.contrib.sites.models import Site
        mysite = MySite.objects.create(site=Site.objects.get(pk=site_id),
                                       template=Template.objects.get(name=settings.DEFOLT_FOLDER))
    try:
        template = Template.objects.get(mysite=mysite)
    except Template.DoesNotExist:
        template = Template.objects.get(name=settings.DEFOLT_FOLDER)
    return template


def get_image_category(category):
    # Возвращаем картинку для категории. Если картинка не найдена в категории и детях, возвращаем заглушку
    if Image.objects.filter(lot__category=category, is_main=True, lot__status=ENABLED).exists():
        logger.debug('We got a picture in this category %s', category)
        return Image.objects.filter(lot__category=category, is_main=True, lot__status=ENABLED)[0]
    else:
        for sub_category in category.get_children():
            if Image.objects.filter(lot__category=sub_category, is_main=True, lot__status=ENABLED).exists():
                logger.debug('We got a picture in this category %s/%s' % (category, sub_category))
                return Image.objects.filter(lot__category=sub_category, is_main=True, lot__status=ENABLED)[0]
    logger.info('Picture NOT FOUND %s, return plug', category.name)
    return Image.objects.first()


def generation_of_main_menu_and_submenu(root_category, site_id):
    # generation_of_the_main_menu_with_submenu
    mysite = MySite.objects.get(pk=site_id)
    template = get_template(site_id)
    sub_categorys = Category.objects.filter(feed__seller__site=mysite,
                                            parent=root_category,
                                            ).exclude(name='')
    count_sub_categorys = 0
    generation_root_menu = False
    for sub_category in sub_categorys:
        count_lot = Lot.objects.filter(category=sub_category, status=ENABLED).count()
        # if count_lot >= template.paginate_by:
        if Lot.objects.filter(category=sub_category, status=ENABLED).exists():
            # В подкатегории есть товары! Она подходит для формирования в меню
            logger.debug('The sub %s lot %s !', sub_category, count_lot)

            root_menu, creade_root_menu = MenuItem.objects.update_or_create(menu=root_category,
                                                                            site=mysite)
            root_menu.image = get_image_category(root_category)
            root_menu.save()
            menu, creade_menu = MenuItem.objects.update_or_create(menu=sub_category,
                                                                  site=mysite,
                                                                  defaults={
                                                                      'parent': root_menu,
                                                                      'image': get_image_category(sub_category),
                                                                  })
            if creade_menu:
                count_sub_categorys += 1

            generation_root_menu = True
        if count_sub_categorys >= template.number_of_subcategories:
            logger.info('We create enough menu subcategories %s count_sub_categorys: %s' % (
                root_category, count_sub_categorys))
            break
    return generation_root_menu


def generation_of_main_menu_not_submenu(category, site_id):
    mysite = MySite.objects.get(pk=site_id)
    template = get_template(site_id)
    count_lot = Lot.objects.filter(category=category, status=ENABLED).count()

    if count_lot >= template.paginate_by:
        menu, creade_menu = MenuItem.objects.update_or_create(menu=category,
                                                              site=mysite,
                                                              defaults={
                                                                  'parent': None,
                                                                  'image': get_image_category(category),
                                                              })
        return True
    return False


def facet_menu(site_id):
    """
    Ищем все топ родительских категорий.
    Затем сужаем поиск в основных категориях

    Args:
        site_id:

    Returns:

    """

    mysite = MySite.objects.get(pk=site_id).site
    template = get_template(site_id)
    # Собираем фасеты по всем категориям
    root_category = SearchQuerySet().filter(
        status=ENABLED,
        sites=mysite.domain
    ).facet(
        'category_parent'
    ).facet_counts()['fields']['category_parent'][:template.number_of_categories]
    for facet in root_category:
        # print((facet[0], facet[1]))
        # sub_menu
        sqs = SearchQuerySet().filter(
            status=ENABLED,
            sites=mysite.domain
        ).facet(
            'category_parent'
        ).facet_counts()['fields']['category_parent'][:template.number_of_categories]
        logger.debug('SearchQuerySet %s, facet %s' % (sqs, facet))


@task
def generate_menu_site(site_id):
    # Генерируем меню.
    # site_id = 1
    mysite = MySite.objects.get(pk=site_id)
    template = get_template(site_id)

    # Удалили все меню, чтобы не делать дубли
    site_menu = MenuItem.objects.filter(site=mysite)
    site_menu.delete()
    logger.info('Удалили меню, чтобы не делать дубликаты mysite = %s', mysite)

    # Ищем все root категории для данного сайта. Исключаем, если у категории нет названия.
    root_categorys = Category.objects.filter(feed__seller__site=mysite,
                                             parent=None).exclude(name='')
    count_categorys = 0
    for root_category in root_categorys:
        if generation_of_main_menu_and_submenu(root_category, site_id):
            count_categorys += 1
        if count_categorys >= template.number_of_categories:
            logger.info('Создали достаточно рут меню count_categorys: %s' % count_categorys)
            break

    if count_categorys < template.number_of_categories:
        logger.info('Есть место для руд категорий, генерируем меню из подкатегорий.')
        exclude_category = Category.objects.filter(menu_items__in=MenuItem.objects.filter(site=mysite))
        for category in Category.objects.filter(feed__seller__site=mysite):
            if category not in exclude_category:
                if generation_of_main_menu_and_submenu(category, site_id):
                    count_categorys += 1
            if count_categorys >= template.number_of_categories:
                logger.info('Создали достаточно пунктов меню count_categorys: %s' % count_categorys)
                break
    # Если мы дошли до сюда, то на сайте явно недостаток товаров )))
    if count_categorys < template.number_of_categories:
        logger.error('Есть места для рут категорий, добавь магазины на сайт website %s', mysite)
