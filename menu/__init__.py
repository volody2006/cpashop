# -*- coding: utf-8 -*-

from haystack.query import SearchQuerySet


def creade_menu_facet():
    sqs = SearchQuerySet().facet('category_parent')
    if sqs.count() == 0:
        return
    for category_parent, count_category_parent in sqs.facet_counts()['fields']['category_parent']:
        pass
        # print category_parent, count_category_parent
        # print get_root_facet_menu('category', category_parent=category_parent, limit=10)
        # sqs_c = SearchQuerySet().filter(category_parent=category_parent).facet('category')
        # category_parent =  sqs_c.facet_counts()['fields']['category']
        # for category, count_category in category_parent:
        #     print (get_root_facet_menu('category', category_parent=category, limit=10))
        #     print ('-------', category, count_category)
        # href = "{{ request.get_full_path }}&amp;selected_facets=category_exact:{{ category_parent.0|urlencode }}"


def get_root_facet_menu(facet, category_parent, limit=10):
    sqs = SearchQuerySet().filter(is_published=True, category_parent=category_parent).facet(facet)
    if sqs.count() == 0:
        return []
    i = 1
    root_facet_menu = []
    for facet_name, count in sqs.facet_counts()['fields'][facet]:
        root_facet_menu.append([facet_name, count])
        i += 1
        if i > limit:
            break
    return root_facet_menu

# get_root_facet_menu('category', 10, category_parent=u'длинные')
