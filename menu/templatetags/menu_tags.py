# -*- coding: utf-8 -*-
import logging
import re

import bleach
from django import template
from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.core.cache import cache
from django.template.defaultfilters import linebreaksbr
from haystack.backends import SQ
from haystack.query import SearchQuerySet
from redis import ConnectionError

from market.models import MySite
from menu.tasks import get_template
from template_selection.managers import custom_get_current_site
from ..models import MenuItem

logger = logging.getLogger(__name__)

register = template.Library()


@register.simple_tag(takes_context=True)
def menu(context, parent=None):
    try:
        return MenuItem.objects.filter(
            site=MySite.objects.get(
                pk=custom_get_current_site(context["request"]).id
            ),
            parent=parent)
    except MySite.DoesNotExist:
        return []
    except Site.DoesNotExist:
        return []


@register.simple_tag(takes_context=True)
def root_menu_facet(context, paret=None):
    site = get_current_site(context["request"])
    return category_parent_list(site)


# TODO: Добавить кеш. нечего насиловать ПС
def category_parent_list(site):
    template = get_template(site.pk)
    limit = template.number_of_categories
    key_cache = 'category_parent_list_%s_%s' % (template.pk, site.pk)

    try:
        menu_list = cache.get(key_cache, None)
        if settings.DEBUG:
            menu_list = None
    except ConnectionError:
        menu_list = None

    if menu_list is None:
        logger.info('cache %s is None', key_cache)
        data = {}
        data.update(sites=site.domain)
        q_object = SQ(**data)

        if site.mysite.query is not None:
            q_object.add(site.mysite.parse_query(), SQ.AND)

        lots = SearchQuerySet().filter(q_object)
        sqs = lots.facet('category_parent')

        if sqs.count() == 0:
            return []

        i_limit = 1
        menu_list = []
        for category_parent, count_category_parent in sqs.facet_counts()['fields']['category_parent']:
            if i_limit > limit:
                break

            # Странный баг, если в еластике не указано название категории
            if category_parent == '':
                continue

            lot = sqs.filter(category_parent=category_parent)[0]
            menu_list.append([category_parent,
                              get_root_facet_menu('category', category_parent=category_parent,
                                                  site=site, limit=template.number_of_subcategories),
                              lot])
            i_limit += 1
        try:
            if settings.DEBUG is False:
                cache.set(key_cache, menu_list, 600)
        except ConnectionError:
            pass
    return menu_list


def get_root_facet_menu(facet, category_parent, site, limit=10):
    data = {}
    data.update(sites=site.domain, category_parent=category_parent)
    q_object = SQ(**data)
    if site.mysite.query is not None:
        q_object.add(site.mysite.parse_query(), SQ.AND)

    lots = SearchQuerySet().filter(q_object)
    # sqs = SearchQuerySet().filter(is_published=True, category_parent=category_parent, sites=site.domain).facet(facet)
    sqs = lots.facet(facet)
    if sqs.count() == 0:
        return []
    i = 1
    root_facet_menu = []
    for facet_name, count in sqs.facet_counts()['fields'][facet]:
        if i > limit:
            break
        lot = sqs.filter(category=facet_name)[0]
        root_facet_menu.append([facet_name, count, lot])
        i += 1

    return root_facet_menu


ALLOWED_TAGS = [
    'a',
    'abbr',
    'acronym',
    'b',
    'blockquote',
    'code',
    'em',
    'i',
    'li',
    'ol',
    'strong',
    'ul',
    'br'
]

ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title'],
    'abbr': ['title'],
    'acronym': ['title'],
}

ALLOWED_STYLES = ['font-weight', ]

PROTOCOLS = ['http', 'https', 'mailto', 'showinfo', 'fitting']


@register.filter
def cleaned_text(value):
    try:
        return bleach.clean(value,
                            tags=ALLOWED_TAGS,
                            attributes=ALLOWED_ATTRIBUTES,
                            styles=ALLOWED_STYLES,
                            strip=True,
                            )
    except:
        return value


@register.filter()
def smart_linebreaks(description):
    """
    В случае если подан текст без html тэгов, заменяем в нем смимволы
    переноса строки на <br />
    """

    matches = re.findall('<.*?>', description)
    # тэгов не найдено - нужно препарировать
    if len(matches) == 0:
        return linebreaksbr(description)

    return description
