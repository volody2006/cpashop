# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey
from slugify import slugify

from market.models import Category, Image, MySite


@python_2_unicode_compatible
class MenuItem(MPTTModel):
    class Meta:
        verbose_name = 'Пункт меню'
        verbose_name_plural = 'Пункты меню'
        ordering = ['tree_id', 'lft']

    menu = models.ForeignKey(Category, verbose_name='Меню', related_name='menu_items')
    parent = TreeForeignKey('self', blank=True, null=True, related_name='children', verbose_name='Родительский пункт')
    image = models.ForeignKey(Image, blank=True, null=True, related_name='image_menu',
                              verbose_name='Картинка категории, '
                                           'берется из товара присутствующим '
                                           'в этой категории')
    site = models.ForeignKey(MySite, blank=True, null=True, related_name='site_menu',
                             verbose_name='Сайт для отображения')

    def __str__(self):
        if self.menu.name is None or '':
            return '%s - Нет названия' % self.menu.pk

        return self.menu.name

    def get_absolute_url(self):
        return reverse('category-view', args=[self.menu.pk, ])


class Menu(MPTTModel):
    name = models.CharField(max_length=150,
                            verbose_name=_('name'))
    slug = models.SlugField(max_length=150,
                            verbose_name=_('Slug'))

    parent = TreeForeignKey('self', blank=True, null=True)
    description = models.TextField(verbose_name=_('Description'), blank=True, null=True)

    link = models.CharField(max_length=256, blank=True, null=True, verbose_name=_('url'),
                            help_text=_('URL or URI to the content, eg /about/ or http://foo.com/'))
    position = models.PositiveIntegerField(default=100)
    image = models.ForeignKey(Image, blank=True, null=True, related_name='menu_image',
                              verbose_name='Картинка категории, '
                                           'берется из товара присутствующим '
                                           'в этой категории')
    site = models.ForeignKey(MySite, blank=True, null=True, related_name='menu_site',
                             verbose_name='Сайт для отображения')

    class Meta(object):
        unique_together = ("slug", "site")

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super().save()

    def get_absolute_url(self):
        if self.link is not None:
            return self.link
        return reverse('category-view', args=[self.id, ])

    def get_url(self):
        return self.get_absolute_url()
